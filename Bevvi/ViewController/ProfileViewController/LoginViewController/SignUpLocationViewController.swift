//
//  SignUpLocationViewController.swift
//  Bevvi
//
//  Created by Hetal Govani on 07/11/17.
//  Copyright © 2017 solulab. All rights reserved.
//

import UIKit

class SignUpLocationViewController: UIViewController, UITextFieldDelegate,SCPopDatePickerDelegate
{
   @IBOutlet var viewHeight : NSLayoutConstraint!
   var dictSignUp : Dictionary<String,AnyObject>!

   let datePicker = SCPopDatePicker()
   @IBOutlet var txtMon1 : customBottomLineTextField!
   @IBOutlet var txtMon2 : customBottomLineTextField!
   
   @IBOutlet var txtDate1 : customBottomLineTextField!
   @IBOutlet var txtDate2 : customBottomLineTextField!
   
   @IBOutlet var txtYear1 : customBottomLineTextField!
   @IBOutlet var txtYear2 : customBottomLineTextField!
   @IBOutlet var txtYear3 : customBottomLineTextField!
   @IBOutlet var txtYear4 : customBottomLineTextField!
   var selectedDate : Date!
   var ageRange : String!
   lazy var errorView: ErrorView = {
      let Errview = UINib(nibName: "ErrorView", bundle: nil).instantiate(withOwner: self, options: nil)[0] as! ErrorView
      Errview.translatesAutoresizingMaskIntoConstraints = false
      Errview.retryButton.addTarget(self, action: #selector(btnRetryPress(sender:)), for: .touchUpInside)
      return Errview
   }()
   func presentError(_ error: customError) {
      self.errorView.error = error
      
      DispatchQueue.main.async {
         self.view.addSubview(self.errorView)
         self.installErrorViewConstraints()
      }
   }
   
   func installErrorViewConstraints() {
      self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[view]|", options: [], metrics: nil, views: [ "view": self.errorView ]))
      self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[view]|", options: [], metrics: nil, views: [ "view": self.errorView ]))
   }
   @IBAction func btnRetryPress(sender:UIButton)
   {
      errorView.removeFromSuperview()
      if appDel.reachability.connection != .none
      {
//         normalLogin()
      }
      else
      {
         self.presentError(.NetworkError)
      }
   }
    override func viewDidLoad()
    {
      super.viewDidLoad()
      self.navigationController?.navigationBar.isHidden = true
      
      let button1 = UIButton(type: UIButtonType.custom)
      button1.setImage(#imageLiteral(resourceName: "Back_red"), for: UIControlState.normal)
      button1.addTarget(self, action:#selector(btnBackPress(sender:)), for: UIControlEvents.touchUpInside)
      button1.frame=CGRect.init(x: 20, y: 20, width: 30, height: 30)
      self.view.addSubview(button1)
      viewHeight.constant = self.view.frame.height - 114
      
      print(dictSignUp)
    }
   @IBAction func showSambagDatePickerViewController(_ sender: UIButton)
   {
      self.datePicker.tapToDismiss = true
      self.datePicker.datePickerType = SCDatePickerType.date
      self.datePicker.showBlur = true
      self.datePicker.datePickerStartDate = Date()
      self.datePicker.btnFontColour = UIColor.white
      self.datePicker.btnColour = redColor
      self.datePicker.showCornerRadius = true
      self.datePicker.delegate = self
      self.datePicker.show(attachToView: self.view)
   }
    @IBAction func btnBackPress(sender:UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
   func validate() -> Bool
   {
      if selectedDate != nil
      {
         let calendar = Calendar.current
         
         let ageComponents = calendar.dateComponents([.year], from: selectedDate, to: Date())
         let age = ageComponents.year!
         
         if age >= 21
         {
            return true
         }
         else
         {
            UtilityClass.showAlert("You must be over 21 years of age to use Bevvi.")
            return false
         }
      }
      else
      {
         UtilityClass.showAlert("Please select birth date.")
         return false
      }
   }
    @IBAction func btnSignUpPress(sender:UIButton)
    {
      if validate()
      {
         if appDel.reachability.connection != .none
         {
            let para1:NSMutableDictionary = NSMutableDictionary()
            para1.setObject("\(dictSignUp["email"]!)", forKey: "email" as NSCopying)
            var fullNameArr = "\(dictSignUp["email"]!)".components(separatedBy: "@")
            let firstName: String = fullNameArr[0]
            para1.setObject(firstName, forKey: "displayName" as NSCopying)
            para1.setObject(firstName, forKey: "username" as NSCopying)
            para1.setObject("\(dictSignUp["password"]!)", forKey: "password" as NSCopying)
            para1.setObject(ageRange, forKey: "ageRange" as NSCopying)
            para1.setObject("\(selectedDate!)", forKey: "birthday" as NSCopying)
            appDel.showHUD()
            if let theJSONData1 = try?  JSONSerialization.data(withJSONObject: para1,options: .prettyPrinted),
               let JSONString1 = String(data: theJSONData1,
                                        encoding: String.Encoding.ascii)
            {
               print("JSON string = \n\(JSONString1)")
               
               self.createAccount(JSONText: JSONString1,FBDict: para1 as! JSONDictionary)
            }
         }
         else
         {
            self.presentError(.NetworkError)
         }
      }
   }
   func scPopDatePickerDidSelectDate(_ date: Date)
   {
      print(date)
      selectedDate = date
      let calendar = Calendar.current
      let components = calendar.dateComponents([.year, .month, .day], from: date)
      
      let year =  components.year
      let month = components.month
      let day = components.day
      
      print(year!)
      print(String(format: "%02d", month!))
      let strDay : String! = String(format: "%02d", day!)
      let strMonth : String! = String(format: "%02d", month!)
      let strYear : String! = String(year!)
      
      txtMon1.text = String(strMonth.first!)
      txtMon2.text = String(strMonth.last!)
      
      txtDate1.text = String(strDay.first!)
      txtDate2.text = String(strDay.last!)
      
      let arr = Array(strYear)
      txtYear1.text = String(arr[0])
      txtYear2.text = String(arr[1])
      txtYear3.text = String(arr[2])
      txtYear4.text = String(arr[3])
      
      let now = Date()
      let ageComponents = calendar.dateComponents([.year], from: date, to: now)
      let age = ageComponents.year!
      ageRange = String(age)
   }
   func createAccount(JSONText: String,FBDict:JSONDictionary)
   {
      let createAcccount = StoreListing()
      createAcccount.jsonArray = JSONText
      ApiManager.shared.createAccount(listing: createAcccount, view: self.view) { (arr : JSONDictionary) in
         print(arr)
         UserDefaults.standard.set("\(arr["code"]!)", forKey: "inviteCode")
         UserDefaults.standard.synchronize()
         appDel.hideHUD()
         for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: ProfileViewController.self)
            {
               self.navigationController!.popToViewController(controller, animated: true)
               UtilityClass.showAlert("Account created successfully.Please login.")
               
               break
            }
         }
//         NotificationCenter.default.post(name: NSNotification.Name(rawValue: "MoveToFirstView"), object: nil)
//         self.navigationController?.popToRootViewController(animated: false)
      }
   }
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
