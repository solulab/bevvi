//
//  SignUpViewController.swift
//  Bevvi
//
//  Created by Hetal Govani on 07/11/17.
//  Copyright © 2017 solulab. All rights reserved.
//

import UIKit
import FacebookLogin
import FacebookCore

class SignUpViewController: UIViewController,EnterEmailDelegate
{
   @IBOutlet var viewHeight : NSLayoutConstraint!
   @IBOutlet var lblAlert : UILabel!
   
//    let date = Date()
   var FBdict : JSONDictionary!
   var strEmail : String!
   @IBOutlet var txtEmail : customTextField!
   @IBOutlet var txtPassword : customTextField!
   @IBOutlet var viewAlert : UIView!
   lazy var errorView: ErrorView = {
      let Errview = UINib(nibName: "ErrorView", bundle: nil).instantiate(withOwner: self, options: nil)[0] as! ErrorView
      Errview.translatesAutoresizingMaskIntoConstraints = false
      Errview.retryButton.addTarget(self, action: #selector(btnRetryPress(sender:)), for: .touchUpInside)
      return Errview
   }()
   func presentError(_ error: customError) {
      self.errorView.error = error
      
      DispatchQueue.main.async {
         self.view.addSubview(self.errorView)
         self.installErrorViewConstraints()
      }
   }
   
   func installErrorViewConstraints() {
      self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[view]|", options: [], metrics: nil, views: [ "view": self.errorView ]))
      self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[view]|", options: [], metrics: nil, views: [ "view": self.errorView ]))
   }
   @IBAction func btnRetryPress(sender:UIButton)
   {
      errorView.removeFromSuperview()
      if appDel.reachability.connection != .none
      {
         //         normalLogin()
      }
      else
      {
         self.presentError(.NetworkError)
      }
   }
    override func viewDidLoad()
    {
        super.viewDidLoad()
//        self.title = "Settings"
        self.navigationController?.navigationBar.isHidden = true
        
        let button1 = UIButton(type: UIButtonType.custom)
        button1.setImage(#imageLiteral(resourceName: "Back_red"), for: UIControlState.normal)
        button1.addTarget(self, action:#selector(btnBackPress(sender:)), for: UIControlEvents.touchUpInside)
        button1.frame=CGRect.init(x: 20, y: 20, width: 30, height: 30)
        self.view.addSubview(button1)
        //        let barButton1 = UIBarButtonItem(customView: button1)
        //        self.navigationItem.leftBarButtonItem = barButton1
        viewHeight.constant = self.view.frame.height - 114
        viewAlert.isHidden = true
    }
    
   
    @IBAction func btnSignUpPress(sender:UIButton)
    {
      viewAlert.isHidden = true
      if validate()
      {
         if appDel.reachability.connection != .none
         {
            let dict = ["email":"\(txtEmail.text!)","password":"\(txtPassword.text!)"] as [String : AnyObject]
            let viewObj = self.storyboard?.instantiateViewController(withIdentifier: "SignUpLocationViewController") as! SignUpLocationViewController
            viewObj.dictSignUp = dict
            self.navigationController?.pushViewController(viewObj, animated: true)
         }
         else
         {
            self.presentError(.NetworkError)
         }
      }
    }
    func validate() -> Bool
    {
        if txtEmail.text?.count != 0
        {
            if UtilityClass.emailvalidate(txtEmail.text!)
            {
                if txtPassword.text?.count != 0
                {
                     return true
                }
                else
                {
                  viewAlert.isHidden = false
                  lblAlert.text = "Please enter password."
                  return false
                }
            }
            else
            {
               viewAlert.isHidden = false
               lblAlert.text = "Please enter valid email address."
                return false
            }
        }
        else
        {
            viewAlert.isHidden = false
            lblAlert.text = "Please enter email address."
            return false
        }
        
    }
    @IBAction func btnBackPress(sender:UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
   func getUserData()
   {
      let para:NSMutableDictionary = NSMutableDictionary()
      
      let includeDict:NSMutableArray = ["paymentIds"]
      para.setObject(includeDict, forKey: "include" as NSCopying)
      
      if let theJSONData = try?  JSONSerialization.data(
         withJSONObject: para,
         options: .prettyPrinted
         ),
         let theJSONText = String(data: theJSONData,
                                  encoding: String.Encoding.ascii)
      {
         print("JSON string = \n\(theJSONText)")
         
         let storeDetail = StoreListing()
         storeDetail.jsonArray = theJSONText
         ApiManager.shared.getUserDataAfterlogin(accountId: "\(UserDefaults.standard.value(forKey: "accountId")!)", listing: storeDetail, view: self.view, completion: { (arr:JSONDictionary) in
            appDel.hideHUD()
            
            print(arr)
            UserDefaults.standard.set("\(arr["code"]!)", forKey: "inviteCode")
            UserDefaults.standard.set("\(arr["email"]!)", forKey: "email")
            
            if let arrPayment = arr["paymentIds"] as? [JSONDictionary]
            {
               UserDefaults.standard.set("\(arrPayment[0]["token"]!)", forKey: "payment_token")
               UserDefaults.standard.synchronize()
               appDel.startSTPSession()
            }
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "MoveToFirstView"), object: nil)
            self.navigationController?.popToRootViewController(animated: false)
         })
      }
   }
   func addLocalCartToServer()
   {
      CartNotLogIn.sharedInstance.getAllCartProductsNotLoggedIn { (cart:[cartDetail]) in
         
         if cart.count>0
         {
            var arr : Array<Dictionary<String,AnyObject>> = Array()
            for Product in cart
            {
               let dict = ["accountId":"\(UserDefaults.standard.value(forKey: "accountId")!)","quantity":"\(Product.quantity!)","productId":"\(Product.productId!)","offerId":"\(Product.id!)"]
               arr.append(dict as [String : AnyObject])
            }
            if let theJSONData1 = try?  JSONSerialization.data(withJSONObject: arr,options: .prettyPrinted),
               let JSONString1 = String(data: theJSONData1,
                                        encoding: String.Encoding.ascii)
            {
               print("JSON string = \n\(JSONString1)")
               
               let listing = StoreListing()
               listing.jsonArray = JSONString1
               appDel.showHUD()
               ApiManager.shared.sendCartToServer(listing: listing, view: self.view, completion: { (arr:[JSONDictionary]) in
                  
                  print(arr)
                  CartNotLogIn.sharedInstance.deleteAllRecords(completion: {
                     self.getUserData()
                  })
                  
               })
            }
         }
         else
         {
            self.getUserData()
         }
      }
   }
   
   @IBAction func btnFacebookTapped(_ sender: Any)
   {
      if appDel.reachability.connection != .none
      {
         let fbLoginManagerTemp : LoginManager = LoginManager()
         fbLoginManagerTemp.logOut()
         let fbLoginManager : LoginManager = LoginManager()
         fbLoginManager.logIn(readPermissions: [ReadPermission.publicProfile,ReadPermission.email,ReadPermission.userBirthday,ReadPermission.userHometown], viewController: self) { (result) in
            self.getFBUserData()
         }
      }
      else
      {
         self.presentError(.NetworkError)
      }
   }
   
   func getFBUserData()
   {
      let params = ["fields":"name,email,first_name,last_name,gender,picture.type(large),age_range,hometown"]
      let graphRequest = GraphRequest(graphPath: "me", parameters: params)
      
      graphRequest.start
         { (urlResponse, requestResult) in
            switch requestResult
            {
            case .failed(let error):
               print(error)
            case .success(let graphResponse):
               print(requestResult)
               
               if let dict = graphResponse.dictionaryValue as JSONDictionary?
               {
                  self.FBdict = dict
                  if let dict1 = dict["picture"] as? JSONDictionary
                  {
                     if let data = dict1["data"] as? JSONDictionary
                     {
                        if let url = data["url"] as? String
                        {
                           print(url)
                           
                           //                           let url = URL(string: url)
                           
                           let para:NSMutableDictionary = NSMutableDictionary()
                           let dictWhere:NSDictionary = NSDictionary(object: "\(dict["id"]!)", forKey: "facebookId" as NSCopying)
                           para.setObject(dictWhere, forKey: "where" as NSCopying)
                           para.setObject("account", forKey: "include" as NSCopying)
                           
                           if let theJSONData = try?  JSONSerialization.data(
                              withJSONObject: para,
                              options: .prettyPrinted
                              ),
                              let theJSONText = String(data: theJSONData,encoding: String.Encoding.ascii)
                           {
                              print("JSON string = \n\(theJSONText)")
                              
                              let storelist = StoreListing()
                              storelist.jsonArray = theJSONText
                              
                              appDel.showHUD()
                              ApiManager.shared.checkFBID(listing: storelist, view: self.view) { (arr:[JSONDictionary]) in
                                 
                                 print(arr)
                                 
                                 if arr.count == 0
                                 {
                                    let ageRangeDict = dict["age_range"]! as! Dictionary<String,AnyObject>
                                    if let val = dict["email"]
                                    {
                                       let para1:NSMutableDictionary = NSMutableDictionary()
                                       para1.setObject("\(dict["email"]!)", forKey: "email" as NSCopying)
                                       para1.setObject("bevviapp", forKey: "password" as NSCopying)
                                       para1.setObject("\(dict["first_name"]!)", forKey: "firstName" as NSCopying)
                                       para1.setObject("\(dict["last_name"]!)", forKey: "lastName" as NSCopying)
                                       para1.setObject("\(dict["name"]!)", forKey: "displayName" as NSCopying)
                                       para1.setObject("\(dict["gender"]!)", forKey: "gender" as NSCopying)
                                       para1.setObject("\(ageRangeDict["min"]!)", forKey: "ageRange" as NSCopying)
                                       para1.setObject("", forKey: "birthday" as NSCopying)
                                       let photoDict:NSMutableDictionary = NSMutableDictionary()
                                       photoDict.setObject("\(url)", forKey: "path" as NSCopying)
                                       para1.setObject(photoDict, forKey: "photo" as NSCopying)
                                       
                                       if let theJSONData1 = try?  JSONSerialization.data(withJSONObject: para1,options: .prettyPrinted),
                                          let JSONString1 = String(data: theJSONData1,
                                                                   encoding: String.Encoding.ascii)
                                       {
                                          print("JSON string = \n\(JSONString1)")
                                          
                                          self.createFBAccount(JSONText: JSONString1,FBDict: dict)
                                       }
                                    }
                                    else
                                    {
                                       let ratingVC = EnterEmailViewController(nibName: "EnterEmailViewController", bundle: nil)
                                       ratingVC.strMessage = "Didn't get your email from facebook. Please enter in box."
                                       ratingVC.delegate=self
                                       self.presentPopupViewController(ratingVC, animationType: MJPopupViewAnimationFade)
                                    }
                                    
                                 }
                                 else
                                 {
                                    let dictAcc = arr[0]["account"] as! JSONDictionary
                                    UserDefaults.standard.set("\(dictAcc["code"]!)", forKey: "inviteCode")
                                    UserDefaults.standard.synchronize()
                                    
                                    
                                    // {"email":"urjasheth91@gmail.com","password":"bevviapp"}
                                    
                                    let param:NSMutableDictionary = NSMutableDictionary()
                                    
                                    param.setObject("\(dictAcc["email"]!)", forKey: "email" as NSCopying)
                                    param.setObject("bevviapp", forKey: "password" as NSCopying)
                                    
                                    if let theJSONData = try?  JSONSerialization.data(withJSONObject: param,options: .prettyPrinted),
                                       let JSONString = String(data: theJSONData,encoding: String.Encoding.ascii)
                                    {
                                       print("JSON string = \n\(JSONString)")
                                       self.login(JSONText: JSONString)
                                    }
                                    
                                 }
                              }
                           }
                        }
                     }
                  }
               }
               break
            }
      }
   }
   func SubmitButtonClicked(_ EnterEmailViewController: EnterEmailViewController)
   {
      self.dismissPopupViewControllerWithanimationType(MJPopupViewAnimationFade)
      let para1:NSMutableDictionary = NSMutableDictionary()
      
      if let dict1 = FBdict["picture"] as? JSONDictionary
      {
         if let data = dict1["data"] as? JSONDictionary
         {
            if let url = data["url"] as? String
            {
               let photoDict:NSMutableDictionary = NSMutableDictionary()
               photoDict.setObject("\(url)", forKey: "path" as NSCopying)
               para1.setObject(photoDict, forKey: "photo" as NSCopying)
            }
         }
      }
      let ageRangeDict = FBdict["age_range"]! as! Dictionary<String,AnyObject>
      
      para1.setObject("\(EnterEmailViewController.strEmail!)", forKey: "email" as NSCopying)
      strEmail = "\(EnterEmailViewController.strEmail!)"
      para1.setObject("bevviapp", forKey: "password" as NSCopying)
      para1.setObject("\(FBdict["first_name"]!)", forKey: "firstName" as NSCopying)
      para1.setObject("\(FBdict["last_name"]!)", forKey: "lastName" as NSCopying)
      para1.setObject("\(FBdict["name"]!)", forKey: "displayName" as NSCopying)
      para1.setObject("\(FBdict["gender"]!)", forKey: "gender" as NSCopying)
      para1.setObject("\(ageRangeDict["min"]!)", forKey: "ageRange" as NSCopying)
      para1.setObject("", forKey: "birthday" as NSCopying)
      
      
      if let theJSONData1 = try?  JSONSerialization.data(withJSONObject: para1,options: .prettyPrinted),
         let JSONString1 = String(data: theJSONData1,
                                  encoding: String.Encoding.ascii)
      {
         print("JSON string = \n\(JSONString1)")
         
         self.createFBAccount(JSONText: JSONString1,FBDict: FBdict)
      }
   }
   func createFBAccount(JSONText: String,FBDict:JSONDictionary)
   {
      let createAcccount = StoreListing()
      createAcccount.jsonArray = JSONText
      
      ApiManager.shared.createAccount(listing: createAcccount, view: self.view) { (arr : JSONDictionary) in
         
         print("\(arr["id"]!)")
         
         let para2:NSMutableDictionary = NSMutableDictionary()
         para2.setObject("\(FBDict["id"]!)", forKey: "facebookId" as NSCopying)
         para2.setObject("\(arr["id"]!)", forKey: "accountId" as NSCopying)
         UserDefaults.standard.set("\(arr["id"]!)", forKey: "accountId")
         UserDefaults.standard.set("\(arr["code"]!)", forKey: "inviteCode")

         UserDefaults.standard.synchronize()
         
         if let theJSONData1 = try?  JSONSerialization.data(withJSONObject: para2,options: .prettyPrinted),
            let JSONString1 = String(data: theJSONData1,
                                     encoding: String.Encoding.ascii)
         {
            print("JSON string = \n\(JSONString1)")
            
            self.createSocialCredentials(JSONText: JSONString1)
         }
      }
   }
   
   func createSocialCredentials(JSONText: String)
   {
      let socialCredentials = StoreListing()
      socialCredentials.jsonArray = JSONText
      
      ApiManager.shared.socialCredentials(listing: socialCredentials, view: self.view, completion: { (dict:JSONDictionary) in
         
         print(dict)
         
         let para2:NSMutableDictionary = NSMutableDictionary()
         // {"email":"urjasheth91@gmail.com","password":"bevviapp"}
         if let email = self.FBdict["email"]
         {
            para2.setObject("\(email)", forKey: "email" as NSCopying)
            
         }
         else
         {
            para2.setObject(self.strEmail, forKey: "email" as NSCopying)
            
         }
         para2.setObject("bevviapp", forKey: "password" as NSCopying)
         
         if let theJSONData1 = try?  JSONSerialization.data(withJSONObject: para2,options: .prettyPrinted),
            let JSONString1 = String(data: theJSONData1,
                                     encoding: String.Encoding.ascii)
         {
            print("JSON string = \n\(JSONString1)")
            self.login(JSONText: JSONString1)
         }
      })
   }
   func login(JSONText: String)
   {
      let socialCredentials = StoreListing()
      socialCredentials.jsonArray = JSONText
      
      ApiManager.shared.loginWithFB(listing: socialCredentials, view: self.view, completion: { (dict:JSONDictionary) in
         
         print(dict)
         UserDefaults.standard.set("\(dict["userId"]!)", forKey: "accountId")
         UserDefaults.standard.set("\(dict["id"]!)", forKey: "access_token")
         UserDefaults.standard.set(true, forKey: "isLogin")
         UserDefaults.standard.synchronize()

         self.deleteDevices(accountID: "\(UserDefaults.standard.value(forKey: "accountId")!)", accessToken: "\(dict["id"]!)")
      })
   }
   func deleteDevices(accountID: String,accessToken:String)
   {
      let socialCredentials = StoreListing()
      //      socialCredentials.jsonArray = jsonText
      ApiManager.shared.deleteDevices(accountID: accountID, accessToken: accessToken,listing: socialCredentials,view: self.view, completion: { () in
         let para2:NSMutableDictionary = NSMutableDictionary()
         // {"email":"urjasheth91@gmail.com","password":"bevviapp"}
         para2.setObject("\(UIDevice.current.identifierForVendor!.uuidString)", forKey: "uuid" as NSCopying)
         
         if let theJSONData1 = try?  JSONSerialization.data(withJSONObject: para2,options: .prettyPrinted),
            let JSONString1 = String(data: theJSONData1,
                                     encoding: String.Encoding.ascii)
         {
            print("JSON string = \n\(JSONString1)")
            self.AddDevices(accountId: "\(UserDefaults.standard.value(forKey: "accountId")!)", JSONText: JSONString1)
         }
      })
   }
   func AddDevices(accountId: String,JSONText: String)
   {
      let socialCredentials = StoreListing()
      socialCredentials.jsonArray = JSONText
      
      ApiManager.shared.addDevices(accountID: accountId, accessToken: UserDefaults.standard.value(forKey: "access_token")! as! String, listing: socialCredentials, view: self.view, completion: { (dict:JSONDictionary) in
         appDel.hideHUD()
         self.addLocalCartToServer()
         print(dict)
      })
   }
   
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}
