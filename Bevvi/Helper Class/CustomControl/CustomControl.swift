//
//  CustomControl.swift
//
//  Created by Ankit on 3/30/16.
//  Copyright © 2016 Ankit. All rights reserved.
//

import Foundation
import UIKit


extension UIView {
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
}


import UIKit

@IBDesignable
class customView: UIView {
    
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    @IBInspectable var borderColor: UIColor? {
        didSet {
            layer.borderColor = borderColor?.cgColor
        }
    }
    
    
    override func draw(_ rect: CGRect)
    {
        // Drawing code
        
        //      let lineView = UIView(frame: CGRect.init(x: 0, y: self.frame.size.height - 1, width: self.frame.size.width, height: 1))
        //      lineView.backgroundColor=UIColor(red: 204.0/255.0, green: 204.0/255.0, blue: 204.0/255.0, alpha: 1)
        //      self.addSubview(lineView)
    }
}
import UIKit

@IBDesignable
class CustomLabel: UILabel {
    
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    @IBInspectable var borderColor: UIColor? {
        didSet {
            layer.borderColor = borderColor?.cgColor
        }
    }
    
    
    override func draw(_ rect: CGRect)
    {
        // Drawing code
        
        //      let lineView = UIView(frame: CGRect.init(x: 0, y: self.frame.size.height - 1, width: self.frame.size.width, height: 1))
        //      lineView.backgroundColor=UIColor(red: 204.0/255.0, green: 204.0/255.0, blue: 204.0/255.0, alpha: 1)
        //      self.addSubview(lineView)
    }
}
