//
//  ForgotPasswordViewController.swift
//  Bevvi
//
//  Created by Hetal Govani on 08/11/17.
//  Copyright © 2017 solulab. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: UIViewController {

   @IBOutlet var viewHeight : NSLayoutConstraint!
   @IBOutlet var txtEmail : customTextField!
   @IBOutlet var lblError : UILabel!
   lazy var errorView: ErrorView = {
      let Errview = UINib(nibName: "ErrorView", bundle: nil).instantiate(withOwner: self, options: nil)[0] as! ErrorView
      Errview.translatesAutoresizingMaskIntoConstraints = false
      Errview.retryButton.addTarget(self, action: #selector(btnRetryPress(sender:)), for: .touchUpInside)
      return Errview
   }()
   func presentError(_ error: customError) {
      self.errorView.error = error
      
      DispatchQueue.main.async {
         self.view.addSubview(self.errorView)
         self.installErrorViewConstraints()
      }
   }
   
   func installErrorViewConstraints() {
      self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[view]|", options: [], metrics: nil, views: [ "view": self.errorView ]))
      self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[view]|", options: [], metrics: nil, views: [ "view": self.errorView ]))
   }
   @IBAction func btnRetryPress(sender:UIButton)
   {
      errorView.removeFromSuperview()
      if appDel.reachability.connection != .none
      {
         forgotPasswordAPCall()
      }
      else
      {
         self.presentError(.NetworkError)
      }
   }
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        
        let button1 = UIButton(type: UIButtonType.custom)
        button1.setImage(#imageLiteral(resourceName: "Back_red"), for: UIControlState.normal)
        button1.addTarget(self, action:#selector(btnBackPress(sender:)), for: UIControlEvents.touchUpInside)
        button1.frame=CGRect.init(x: 20, y: 20, width: 30, height: 30)
        self.view.addSubview(button1)
        //        let barButton1 = UIBarButtonItem(customView: button1)
        //        self.navigationItem.leftBarButtonItem = barButton1
        viewHeight.constant = self.view.frame.height - 114
         lblError.isHidden = true

    }
    @IBAction func btnBackPress(sender:UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnSendPress(sender:UIButton)
    {
      if txtEmail.text?.count != 0
      {
         lblError.isHidden = true
         if appDel.reachability.connection != .none
         {
            forgotPasswordAPCall()
         }
         else
         {
            self.presentError(.NetworkError)
         }
      }
      else
      {
         lblError.isHidden = false
         lblError.text = "Enter the email address linked to your account."
      }
    }
   func forgotPasswordAPCall()
   {
      let storelist = StoreListing()
      appDel.showHUD()
      ApiManager.shared.sendEmail(type: 0, email: txtEmail.text!, listing: storelist, view: self.view) { (arr:JSONDictionary) in
         
         appDel.hideHUD()
         print(arr)
         
         let viewObj = self.storyboard?.instantiateViewController(withIdentifier: "SentEmailViewController") as! SentEmailViewController
         self.navigationController?.pushViewController(viewObj, animated: true)
      }
   }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}
