//
//  CustomTabBarViewController.swift
//  AN Client
//
//  Created by Hetal Govani on 02/08/17.
//  Copyright © 2017 SoluLab. All rights reserved.
//

import UIKit

protocol CustomTabBarControllerDelegate
{
    func customTabBarControllerDelegate_CenterButtonTapped(tabBarController:CustomTabBarViewController, button:UIButton, buttonState:Bool)
}

class CustomTabBarViewController: UITabBarController, UITabBarControllerDelegate
{
    var customTabBarControllerDelegate:CustomTabBarControllerDelegate?;
    var centerButton:UIButton!;
    var centerButtonTappedOnce:Bool = false;
    
    override func viewDidLayoutSubviews()
    {
        super.viewDidLayoutSubviews();
//        self.bringcenterButtonToFront();
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        selectedMenuItem = 1
        
//        self.navigationController?.navigationBar.isHidden = true
//        UIApplication.shared.isStatusBarHidden = true
        
        self.delegate = self;
        self.tabBar.backgroundColor = blackColor
        //        let numberOfItems = CGFloat((self.tabBar.items!.count))
        
//        let tabBarItemSize = CGSize(width: (self.tabBar.frame.width) / 5, height: (self.tabBar.frame.height))
        self.tabBar.tintColor = redColor
//        self.tabBar.selectionIndicatorImage = UIImage().imageWithColor(color: UIColor.red, size: tabBarItemSize)

        let dashboardVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        
        let dashboardItem = self.tabBar.items![0] as UITabBarItem
        dashboardItem.imageInsets = UIEdgeInsets(top: 9, left: 0, bottom: -9, right: 0)
        
        let dashboardItemImage = UIImage(named:"tab1")!.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        let dashboardItemImageSelected = UIImage(named: "tab1_1")!.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        dashboardItem.image = dashboardItemImage
        dashboardItem.selectedImage = dashboardItemImageSelected.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
//        self.tabBar.frame.size.width = self.view.frame.width + 4
//        self.tabBar.frame.origin.x = -2

        let nav1 = UINavigationController(rootViewController: dashboardVC)
        
        let view2Obj = self.storyboard?.instantiateViewController(withIdentifier: "CartViewController") as! CartViewController
        let view2Item = self.tabBar.items![1] as UITabBarItem
        view2Item.imageInsets = UIEdgeInsets(top: 9, left: 0, bottom: -9, right: 0)
        let view2ItemImage = UIImage(named:"tab2")!.withRenderingMode(UIImageRenderingMode.alwaysOriginal)

        let view2ItemImageSelected = UIImage(named: "tab2_1")!.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        view2Item.image = view2ItemImage

        view2Item.selectedImage = view2ItemImageSelected
        let nav2 = UINavigationController(rootViewController: view2Obj)
        
        let view4Obj = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        let view4Item = self.tabBar.items![2] as UITabBarItem
        view4Item.imageInsets = UIEdgeInsets(top: 9, left: 0, bottom: -9, right: 0)
        let view4ItemImage = UIImage(named:"tab3")!.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        let view4ItemImageSelected = UIImage(named: "tab3_1")!.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        view4Item.image = view4ItemImage
        view4Item.selectedImage = view4ItemImageSelected
        let nav4 = UINavigationController(rootViewController: view4Obj)
        
        viewControllers = [nav1, nav2, nav4]
    }
    
    // MARK: - TabbarDelegate Methods
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController)
    {
        switch viewController
        {
        case is HomeViewController: break
        case is CartViewController: break
        case is ProfileViewController:
//            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ReloadView"), object: nil)
            break
        default: break
        }
    }
}

extension UIImage
{
    func imageWithColor(color: UIColor, size: CGSize) -> UIImage
    {
        let rect: CGRect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        color.setFill()
        UIRectFill(rect)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
}
