//
//  Constant.h
//  MedsyncRX
//
//  Created by Ankit on 3/31/16.
//  Copyright © 2016 Ankit. All rights reserved.
//

#ifndef Constant_h
#define Constant_h
#define APP_NAME "Bevvi"

#pragma mark - _____________________ Alert Validation _____________________
// Validation Alerts

#define k_Enter_Email_Address "Please enter email address."
#define k_Enter_Valid_Email_Address "Entered email address was invalid."
#define k_Enter_Password "Please enter password."
#define k_Enter_FullName "Please enter name."
#define k_Enter_Password_does_not_Match "Password does not match."
#define k_Enter_Confirm_Pass "Please enter confirm password."
#define k_Enter_MobileNo "Please enter 10 digit Mobile no."
#define k_Enter_DOB "Please select Date of Birth."


#endif /* Constant_h */
