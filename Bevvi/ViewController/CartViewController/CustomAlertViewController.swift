//
//  CustomAlertViewController.swift
//  Bevvi
//
//  Created by Hetal Govani on 22/11/17.
//  Copyright © 2017 solulab. All rights reserved.
//

import UIKit

protocol customAlertDelegate
{
    func btnSubmitPress(viewController:CustomAlertViewController)
}
class CustomAlertViewController: UIViewController
{
    var delegate : customAlertDelegate?
    @IBOutlet var lblTitle : UILabel!
    @IBOutlet var lblMessage : UILabel!
    @IBOutlet var btnSubmit : UIButton!
   var strMessage : String!
    override func viewDidLoad()
    {
        super.viewDidLoad()
         lblMessage.text = strMessage
    }
    @IBAction func btnSubmitPress(sender:UIButton)
    {
        delegate?.btnSubmitPress(viewController: self)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
