//
//  AppDelegate.swift
//  Bevvi
//
//  Created by Hetal Govani on 24/10/17.
//  Copyright © 2017 solulab. All rights reserved.

import UIKit
import CoreData
import Fabric
import Crashlytics
import IQKeyboardManagerSwift
import GoogleMaps
import GooglePlaces
import UserNotifications
import FacebookLogin
import FacebookCore
import Reachability
import MBProgressHUD
var currentLocation: CLLocation?
import Stripe
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate
{
   var window: UIWindow?
   let reachability = Reachability()!
   var customerContext: STPCustomerContext!
   var paymentContext: STPPaymentContext!
   
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool
    {
      if (UserDefaults.standard.object(forKey: "isLogin") == nil) {
         UserDefaults.standard.set(false, forKey: "isLogin")
         UserDefaults.standard.synchronize()
      }
      if (UserDefaults.standard.object(forKey: "isRateDisplayed") == nil) {
         UserDefaults.standard.set(false, forKey: "isRateDisplayed")
         UserDefaults.standard.synchronize()
      }
      if (UserDefaults.standard.object(forKey: "maxRadious") == nil) {
         UserDefaults.standard.set(1, forKey: "maxRadious")
         UserDefaults.standard.synchronize()
      }
      if (UserDefaults.standard.object(forKey: "walk") == nil) {
         UserDefaults.standard.set(0, forKey: "walk")
         UserDefaults.standard.synchronize()
      }
      GMSServices.provideAPIKey(GOOGLE_API_KEY)
      GMSPlacesClient.provideAPIKey(GOOGLE_API_KEY)
      IQKeyboardManager.sharedManager().enable = true
      
      NotificationCenter.default.addObserver(self, selector: #selector(self.reachabilityChanged),name: Notification.Name.reachabilityChanged,object: reachability)
      do{
         try reachability.startNotifier()
      }catch{
         print("could not start reachability notifier")
      }
      registerForPushNotifications(application: application)

      UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).defaultTextAttributes = [NSAttributedStringKey.foregroundColor.rawValue: UIColor.white]
//      UITextField.appearance().tintColor = UIColor.white
      let navigationBarAppearace = UINavigationBar.appearance()
      
      application.statusBarStyle = UIStatusBarStyle.lightContent
      navigationBarAppearace.backgroundColor = blackColor
      
      navigationBarAppearace.tintColor = kBARBUTTONCOLOR
      navigationBarAppearace.barTintColor = kNAVIGATIONCOLOR
      
      // change navigation item title color
      navigationBarAppearace.titleTextAttributes = [NSAttributedStringKey.foregroundColor:kBARBUTTONCOLOR,NSAttributedStringKey.font:UIFont(name: "Avenir-Medium", size: 15)!]
      self.window = UIWindow(frame: UIScreen.main.bounds)
      
      if (UserDefaults.standard.object(forKey: "isFirstTime") == nil) {
         UserDefaults.standard.set(false, forKey: "isFirstTime")
         UserDefaults.standard.synchronize()
      }
      let isFirstTime = UserDefaults.standard.bool(forKey: "isFirstTime")
      if isFirstTime == false
      {
         let enableLocationViewObj = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "EnableLocationViewController") as! EnableLocationViewController
         self.window?.rootViewController = enableLocationViewObj

      }
      else
      {
         let tabbarController = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CustomTabBarViewController") as! CustomTabBarViewController
         self.window?.rootViewController = tabbarController
      }
      
      self.window?.makeKeyAndVisible()
      
      Fabric.with([Crashlytics.self])
      
      return FacebookCore.SDKApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)

    }
   func startSTPSession() {
      let config = STPPaymentConfiguration.shared()
      config.publishableKey = StripePublishableKey
      
      customerContext = STPCustomerContext(keyProvider: MyAPIClient.sharedClient)
      paymentContext = STPPaymentContext(customerContext: customerContext,
                                         configuration: config,
                                         theme: STPTheme.init())
   }
    func registerForPushNotifications(application: UIApplication)
    {
        if #available(iOS 10.0, *)
        {
            UNUserNotificationCenter.current().delegate = self
            UNUserNotificationCenter.current().requestAuthorization(options: [.badge, .sound, .alert], completionHandler: {(granted, error) in
                if (granted)
                {
                    DispatchQueue.main.async
                    {
                        UIApplication.shared.registerForRemoteNotifications()
                    }
                }
                else{
                    //Do stuff if unsuccessful...
                }
            })
        }
            
        else{ //If user is not on iOS 10 use the old methods we've been using
            let notificationSettings = UIUserNotificationSettings(
                types: [.badge, .sound, .alert], categories: nil)
            application.registerUserNotificationSettings(notificationSettings)
        }
    }
   @objc func reachabilityChanged(note: NSNotification) {
      
      let reachability = note.object as! Reachability
      
      if reachability.connection != .none
      {
         if reachability.connection == .wifi {
            print("Reachable via WiFi")
         } else {
            print("Reachable via Cellular")
         }
      }
      else
      {
         print("Network not reachable")
      }
   }
    // Below Mehtod will print error if not able to update location.
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error")
    }
    
    func applicationWillResignActive(_ application: UIApplication)
    {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
   
    func showHUD()
    {
      hideHUD()
      MBProgressHUD.showAdded(to: (UIApplication.shared.keyWindow?.rootViewController?.view)!, animated: true)
//      animationView = LOTAnimationView(name: "preloader.json")
//      animationView.frame = CGRect.init(x: (UIScreen.main.bounds.width - 230)/2, y: (UIScreen.main.bounds.height - 230)/2, width: 230, height: 230)
//      UIApplication.shared.keyWindow?.rootViewController?.view.addSubview(animationView)
//      animationView.play()
    }

   func hideHUD()
   {
      MBProgressHUD.hide(for: (UIApplication.shared.keyWindow?.rootViewController?.view)!, animated: true)

//      if animationView != nil
//      {
//         animationView.stop()
//         animationView.removeFromSuperview()
//         UIApplication.shared.keyWindow?.rootViewController?.view.willRemoveSubview(animationView)
//      }
      
   }
   func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
      
      return FacebookCore.SDKApplicationDelegate.shared.application(app, open: url, options: options)
      
   }
    func applicationDidEnterBackground(_ application: UIApplication)
    {
        UserDefaults.standard.set(false, forKey: "isRateDisplayed")
        UserDefaults.standard.synchronize()
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication)
    {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication)
    {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication)
    {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        
        self.saveContext()
        
    }

    // MARK: - Core Data stack
   
   lazy var applicationDocumentsDirectory: NSURL = {
      // The directory the application uses to store the Core Data store file. This code uses a directory named "uk.co.plymouthsoftware.core_data" in the application's documents Application Support directory.
      let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
      return urls[urls.count-1] as NSURL
   }()
   
   lazy var managedObjectModel: NSManagedObjectModel = {
      // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
      let modelURL = Bundle.main.url(forResource: "Bevvi", withExtension: "momd")!
      return NSManagedObjectModel(contentsOf: modelURL)!
   }()
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "Bevvi")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
   lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
      // The persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
      // Create the coordinator and store
      let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
      let url = self.applicationDocumentsDirectory.appendingPathComponent("PROJECTNAME.sqlite")
      var failureReason = "There was an error creating or loading the application's saved data."
      do {
         try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: nil)
      } catch {
         // Report any error we got.
         var dict = [String: AnyObject]()
         dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data" as AnyObject?
         dict[NSLocalizedFailureReasonErrorKey] = failureReason as AnyObject?
         
         dict[NSUnderlyingErrorKey] = error as NSError
         let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
         // Replace this with code to handle the error appropriately.
         // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
         NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
         abort()
      }
      
      return coordinator
   }()
   lazy var managedObjectContext: NSManagedObjectContext = {
      // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
      let coordinator = self.persistentStoreCoordinator
      var managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
      managedObjectContext.persistentStoreCoordinator = coordinator
      return managedObjectContext
   }()
    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

}
