//
//  CustomTwoButtonAlertViewController.swift
//  Bevvi
//
//  Created by Hetal Govani on 29/12/17.
//  Copyright © 2017 solulab. All rights reserved.
//

import UIKit
protocol customTwoButtonAlertDelegate
{
   func btnSubmitPress(viewController:CustomTwoButtonAlertViewController)
   func btnDismissPress(viewController:CustomTwoButtonAlertViewController)

}
class CustomTwoButtonAlertViewController: UIViewController {

   var delegate : customTwoButtonAlertDelegate?
   @IBOutlet var lblTitle : UILabel!
   @IBOutlet var lblMessage : UILabel!
   @IBOutlet var btnSubmit : UIButton!
   @IBOutlet var btnDismiss : UIButton!
   var strMessage : String!
   var strTitle : String!
   override func viewDidLoad()
   {
      super.viewDidLoad()
      lblMessage.text = strMessage
      lblTitle.text = strTitle

   }
   @IBAction func btnSubmitPress(sender:UIButton)
   {
      delegate?.btnSubmitPress(viewController: self)
   }
   @IBAction func btnDismissPress(sender:UIButton)
   {
      delegate?.btnDismissPress(viewController: self)
   }
   override func didReceiveMemoryWarning() {
      super.didReceiveMemoryWarning()
   }

}
