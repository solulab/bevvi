//
//  STCustomer.swift
//  Bevvi
//
//  Created by Ankit on 1/11/18.
//  Copyright © 2018 solulab. All rights reserved.
//

import Foundation
import Stripe


class STPCustomerClass {
   
   static let shared = STPCustomerClass()
   
   var cardSources = [STPSource]()
   var selectedSource: STPSource?
   var defaultSource: STPSource?
   
   func getCustomer(completion:@escaping(_ error:Error?) -> ()) {
      
      appDel.customerContext.retrieveCustomer { (customer, error) in
      
         if error == nil {
            if customer!.sources.count > 0 {
               self.cardSources = customer!.sources as! [STPSource]
               if self.cardSources.count > 0 {
                  self.defaultSource = self.cardSources[0]
               }
            } else {
               self.cardSources = [STPSource]()
               self.selectedSource = nil
               self.defaultSource = nil
            }
            completion(nil)
         } else {
            completion(error)
         }
      }
   }
}


class STCustomer {
   
   var object: String?
   var has_more: Bool?
   var url: String?
   var data: [CustomerData]?
   
   func toParseDict(dict: JSONDictionary) {
      
      print("STCustomer- \(dict)")
      
      if let object = dict["object"] as? String {
         self.object = object
      }
      
      if let has_more = dict["has_more"] as? Bool {
         self.has_more = has_more
      }
      
      if let url = dict["url"] as? String {
         self.url = url
      }
      
      if let data = dict["data"] as? [JSONDictionary] {
         
         var tempCustomerData = [CustomerData]()
         for customer in data {
            let customerObj = CustomerData()
            customerObj.toParseDict(dict: customer)
            tempCustomerData.append(customerObj)
         }
         self.data = tempCustomerData
      }
   }
}

class CustomerData {
   var id: String?
   var object: String?
   var account_balance: NSNumber?
   var created: NSNumber?
   var currency: String?
   var default_source: String?
   var delinquent: Bool?
   var description: String?
   var discount: String?
   var email: String?
   var livemode: Bool?
   var metadata: JSONDictionary?
   var shipping: String?
   var sources: CustomerSource?
   var subscriptions: CustomerSubscriptions?
   
   func toParseDict(dict: JSONDictionary) {
      
      
      
      if let id = dict["id"] as? String {
         self.id = id
      }
      
      if let object = dict["object"] as? String {
         self.object = object
      }
      
      if let account_balance = dict["account_balance"] as? NSNumber {
         self.account_balance = account_balance
      }
      
      if let created = dict["created"] as? NSNumber {
         self.created = created
      }
      
      if let currency = dict["currency"] as? String {
         self.currency = currency
      }
      
      if let default_source = dict["default_source"] as? String {
         self.default_source = default_source
      }
      
      if let delinquent = dict["delinquent"] as? Bool {
         self.delinquent = delinquent
      }
      
      if let description = dict["description"] as? String {
         self.description = description
      }
      
      if let discount = dict["discount"] as? String {
         self.discount = discount
      }
      
      if let email = dict["email"] as? String {
         self.email = email
      }
      
      if let livemode = dict["livemode"] as? Bool {
         self.livemode = livemode
      }
      
      if let metadata = dict["metadata"] as? JSONDictionary {
         self.metadata = metadata
      }
      
      if let shipping = dict["shipping"] as? String {
         self.shipping = shipping
      }
      
      if let customerObj = dict["sources"] as? JSONDictionary {
         let customerSource = CustomerSource()
         customerSource.toParseDict(dict: customerObj)
         self.sources = customerSource
      }
      
      if let subscription = dict["subscriptions"] as? JSONDictionary {
         let customersub = CustomerSubscriptions()
         customersub.toParseDict(dict: subscription)
         self.subscriptions = customersub
      }
   }
}

class CustomerSource {
   var object: String?
   var data: [STCard]?
   var has_more: Bool?
   var total_count: NSNumber?
   var url: String?
   
   func toParseDict(dict: JSONDictionary) {
      
      
      
      if let object = dict["object"] as? String {
         self.object = object
      }
      
      if let data = dict["data"] as? [JSONDictionary] {
         
         var cards = [STCard]()
         for card in data {
            
            if "\(card["object"]!)" == "card" {
               let cardObj = STCard()
               cardObj.toParseDict(dict: card)
               cards.append(cardObj)
            }
            
         }
         self.data = cards
      }
      
      if let has_more = dict["has_more"] as? Bool {
         self.has_more = has_more
      }
      
      if let total_count = dict["total_count"] as? NSNumber {
         self.total_count = total_count
      }
      
      if let url = dict["url"] as? String {
         self.url = url
      }
   }
}

class CustomerSubscriptions {
   var object: String?
   var data: [JSONDictionary]?
   var has_more: Bool?
   var total_count: NSNumber?
   var url: String?
   
   func toParseDict(dict: JSONDictionary) {
      
      if let object = dict["object"] as? String {
         self.object = object
      }
      
      if let data = dict["data"] as? [JSONDictionary] {
         self.data = data
      }
      
      if let has_more = dict["has_more"] as? Bool {
         self.has_more = has_more
      }
      
      if let total_count = dict["total_count"] as? NSNumber {
         self.total_count = total_count
      }
      
      if let url = dict["url"] as? String {
         self.url = url
      }
   }
   
}
