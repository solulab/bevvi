//
//  StoreListing.swift
//  Bevvi
//
//  Created by Deep Sheth on 05/12/17.
//  Copyright © 2017 solulab. All rights reserved.
//

import UIKit
import Foundation

class StoreListing {

   var jsonArray : String!
   var dict: JSONDictionary?

   func toJsonDict() -> JSONDictionary {
      
      var dict: JSONDictionary = [:]
      
      return dict
   }
   func populateWithJson(dict: JSONDictionary)
   {
      self.dict = dict
   }
}

class StoreDetails {
   
   var jsonArray : String!
   var dict: JSONDictionary?
   
   func toJsonDict() -> JSONDictionary {
      
      var dict: JSONDictionary = [:]
      
      return dict
   }
   func populateWithJson(dict: JSONDictionary)
   {
      self.dict = dict
   }
}
class cartDetail {
   
   var jsonArray : String!
   var dict: JSONDictionary?
   var uniqueId : String?
   var productId : String?
   var quantity : Int?
   var id : String?
   var isFromServer : String?
   var name : String?
   var varietal : String?
   var salePrice : String?
   var originalPrice : String?
   var address : String?
   var storeName : String?

   func toJsonDict() -> JSONDictionary {
      
      var tempdict: JSONDictionary = [:]
      if let uniqueId = uniqueId { tempdict["uniqueId"] = uniqueId as AnyObject? }
      if let productId = productId { tempdict["productId"] = productId as AnyObject? }
      if let quantity = quantity { tempdict["quantity"] = quantity as AnyObject? }
      if let id = id { tempdict["id"] = id as AnyObject? }
      if let name = name { tempdict["name"] = name as AnyObject? }
      if let varietal = varietal { tempdict["varietal"] = varietal as AnyObject? }
      if let salePrice = salePrice { tempdict["salePrice"] = salePrice as AnyObject? }
      if let originalPrice = originalPrice { tempdict["originalPrice"] = originalPrice as AnyObject? }
      if let dict = dict { tempdict["dict"] = dict as AnyObject? }
      if let address = address { tempdict["address"] = address as AnyObject? }
      if let storeName = storeName { tempdict["storeName"] = storeName as AnyObject? }

      return tempdict
   }
   func toPopulateWithJSON(dict:JSONDictionary)
   {
      
      if let uniqueId = dict["uniqueId"] as? String {
         self.uniqueId = uniqueId
      }
      if let productId = dict["productId"] as? String {
         self.productId = productId
      }
      if let quantity =  dict["quantity"] as? Int {
         self.quantity = quantity
      }
      if let id = dict["id"] as? String {
         self.id = id
      }
      if let name = dict["name"] as? String {
         self.name = name
      }
      if let varietal = dict["varietal"] as? String {
         self.varietal = varietal
      }
      if let salePrice = dict["salePrice"] as? String {
         self.salePrice = salePrice
      }
      if let originalPrice = dict["originalPrice"] as? String {
         self.originalPrice = originalPrice
      }
      if let dict = dict["dict"] as? JSONDictionary {
         self.dict = dict
      }
      if let storeName = dict["storeName"] as? String {
         self.storeName = storeName
      }
      if let address = dict["address"] as? String {
         self.address = address
      }
   }
   func populateWithJson(dict: JSONDictionary)
   {
      self.dict = dict
   }
}
