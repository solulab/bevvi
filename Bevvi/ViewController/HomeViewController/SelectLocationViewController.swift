//
//  SelectLocationViewController.swift
//  Bevvi
//
//  Created by Hetal Govani on 25/10/17.
//  Copyright © 2017 solulab. All rights reserved.
//

import UIKit
import CoreLocation
import GooglePlaces
import GoogleMaps

//@objc protocol setAddressDelegate {
//   func setAddress(address:String)
//}

class SelectLocationViewController: UIViewController,UITableViewDelegate,UITableViewDataSource, CLLocationManagerDelegate,UITextFieldDelegate,UISearchDisplayDelegate
{
   @IBOutlet var tblObj : UITableView!
   var locManager = CLLocationManager()
   let locationManager = CLLocationManager()
   var arrAddress : Array<Dictionary<String,AnyObject>> = Array()
   @IBOutlet var txtLocation: UITextField!
    override func viewDidLoad() {
      
      super.viewDidLoad()
      self.title = "Location"
      
      let button1 = UIButton(type: UIButtonType.custom)
      button1.setImage(#imageLiteral(resourceName: "close"), for: UIControlState.normal)
      button1.addTarget(self, action:#selector(btnBackPress(sender:)), for: UIControlEvents.touchUpInside)
      button1.frame=CGRect.init(x: 0, y: 0, width: 30, height: 30)
      let barButton1 = UIBarButtonItem(customView: button1)
      self.navigationItem.leftBarButtonItem = barButton1
      
      tblObj.tableFooterView = UIView()
      
      self.txtLocation.delegate = self
      getLocation()
    }
   override func viewWillAppear(_ animated: Bool) {
      super.viewWillAppear(true)
      if (UserDefaults.standard.object(forKey: "userLocation") == nil) {
         self.navigationItem.leftBarButtonItem?.isEnabled = false
      }
   }
   @IBAction func btnCurrentLocationSet(sender:UIButton)
   {
      setupLocationManager()
   }
   func setupLocationManager()
   {
      appDel.showHUD()

      locManager = CLLocationManager()
      locManager.delegate = self
      locManager.requestAlwaysAuthorization()
      locManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
      locManager.startUpdatingLocation()
   }
   // Below method will provide you current location.
   func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
   {
      appDel.showHUD()

      currentLocation = locations.last
      locManager.stopMonitoringSignificantLocationChanges()
      if let locationValue = manager.location?.coordinate
      {
         print("locations = \(locationValue)")
         let lat = NSNumber(value: locationValue.latitude)
         let lon = NSNumber(value: locationValue.longitude)
         let userLocation: NSDictionary = ["lat": lat, "long": lon]
         UserDefaults.standard.set(userLocation, forKey: "userLocation") //the error is here
         self.navigationItem.leftBarButtonItem?.isEnabled = true
         UserDefaults.standard.synchronize()
         
         locManager.stopUpdatingLocation()
         getAddressFromLatLon(pdblLatitude: lat, withLongitude: lon)

         NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadMap"), object: nil)
      }
   }
   // Below Mehtod will print error if not able to update location.
   func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
   {
      print("Error")
      if CLLocationManager.locationServicesEnabled()
      {
         switch CLLocationManager.authorizationStatus() {
         case .notDetermined, .restricted, .denied:
            appDel.hideHUD()
            print("No access")
            let alert = UIAlertController.init(title: APP_NAME, message: "Please enable location service for this app in ALLOW LOCATION ACCESS: Always, Go to Setting?", preferredStyle: .alert)
            alert.addAction(UIAlertAction.init(title: "cancel", style: UIAlertActionStyle.cancel, handler: nil))
            alert.addAction(UIAlertAction.init(title: "Open Setting", style: UIAlertActionStyle.default, handler: { (action) in
               guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                  return
               }
               if UIApplication.shared.canOpenURL(settingsUrl) {
                  UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                     print("Settings opened: \(success)") // Prints true
                  })
               }
            }))
            self.present(alert, animated: true, completion: nil)
         case .authorizedAlways, .authorizedWhenInUse:
            print("Access")
            appDel.hideHUD()
         }
      } else {
         print("No Access")
         appDel.hideHUD()
      }
   }
   func getAddressFromLatLon(pdblLatitude: NSNumber, withLongitude pdblLongitude: NSNumber) {
      var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
      let lat: Double = Double(pdblLatitude)
      //21.228124
      let lon: Double = Double(pdblLongitude)
      //72.833770
      let ceo: CLGeocoder = CLGeocoder()
      center.latitude = lat
      center.longitude = lon
      
      let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
      
      ceo.reverseGeocodeLocation(loc, completionHandler:
         {(placemarks, error) in
            if (error != nil)
            {
               print("reverse geodcode fail: \(error!.localizedDescription)")
            }
            let pm = placemarks! as [CLPlacemark]
            
            if pm.count > 0 {
               let pm = placemarks![0]
               var addressString : String = ""
               if pm.subLocality != nil {
                  addressString = addressString + pm.subLocality! + ", "
               }
               if pm.thoroughfare != nil {
                  addressString = addressString + pm.thoroughfare! + ", "
               }
               if pm.locality != nil {
                  addressString = addressString + pm.locality! + ", "
               }
               if pm.country != nil {
                  addressString = addressString + pm.country! + ", "
               }
               if pm.postalCode != nil {
                  addressString = addressString + pm.postalCode! + " "
               }
               
               print(addressString)
               UserDefaults.standard.set(addressString, forKey: "address")
               let userLocation: NSDictionary = ["lat": lat, "long": lon]
               UserDefaults.standard.set(userLocation, forKey: "userLocation") //the error is here
               self.navigationItem.leftBarButtonItem?.isEnabled = true

               UserDefaults.standard.synchronize()
               self.PostLocation(lat: NSNumber(value:lat), long: NSNumber(value:lon), address: addressString)

               self.btnBackPress(sender: UIButton())
            }
      })
      
   }
   func getLocation()
   {
      
      if UserDefaults.standard.object(forKey: "accountId") != nil
      {
         appDel.showHUD()
         var dictWhere = Dictionary<String,AnyObject>()
         
         dictWhere["where"] = ["accountId":"\(UserDefaults.standard.value(forKey: "accountId")!)"] as AnyObject
         
         
         if let theJSONData = try?  JSONSerialization.data(withJSONObject: dictWhere,options: .prettyPrinted),
            let theJSONText = String(data: theJSONData,encoding: String.Encoding.ascii)
         {
            print("JSON string = \n\(theJSONText)")
            let listing = StoreListing()
            listing.jsonArray = theJSONText
            
            ApiManager.shared.getLocation(listing: listing, view: self.view, completion: { (arr:[JSONDictionary]) in
               
               self.arrAddress = arr
               appDel.hideHUD()
               self.tblObj.reloadData()
            })
         }
      }
   }
   func PostLocation(lat:NSNumber,long:NSNumber,address:String)
   {
      if UserDefaults.standard.object(forKey: "accountId") != nil
      {
         
         appDel.showHUD()
         ApiManager.shared.Location(lat: lat, long: long, completion: { (dict:JSONDictionary) in
            
            print(dict)
            
            ApiManager.shared.pickupLocation(id : "\(dict["id"]!)",address: address, lat: lat, long: long, completion: { (dict:JSONDictionary) in
               
               print(dict)
               appDel.hideHUD()
            })
         })
         
      }
   }
   func textFieldDidBeginEditing(_ textField: UITextField)
   {
      let acController = GMSAutocompleteViewController()
      acController.delegate = self
//      acController.
      self.present(acController, animated: true, completion: nil)
   }

    // MARK: - Tableview delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrAddress.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
      let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! LocationCell
      let arr = "\(arrAddress[indexPath.row]["address"]!)".components(separatedBy: ",") as? [String]
      cell.lblTitle.text = "\(arr![0])"
      if arr!.count > 1
      {
         var arrString = arr as! [String]
         arrString.remove(at: 0)
//         var str : String! = ""
//         for i in 1 ..< arr!.count
//         {
//            str = "\(str!) \(arr![i])"
//         }
         cell.lblDesc.text = arrString.joined(separator: ",")
      }
      else
      {
         cell.lblDesc.text = "\(arr![0])"
      }
      cell.selectionStyle = .none
      return cell
    }
   func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
   {
      let dictGeo = arrAddress[indexPath.row]["geoLocation"] as! [String:AnyObject]
      let arrCoord = dictGeo["coordinates"] as! [NSNumber]
      let userLocation: NSDictionary = ["lat": arrCoord[1], "long": arrCoord[0]]
      UserDefaults.standard.set(userLocation, forKey: "userLocation") //the error is here
      self.navigationItem.leftBarButtonItem?.isEnabled = true
      
      UserDefaults.standard.set("\(arrAddress[indexPath.row]["address"]!)", forKey: "address")
      UserDefaults.standard.synchronize()
      
      self.btnBackPress(sender: UIButton())

      
   }
    @IBAction func btnBackPress(sender:UIButton)
    {
        let transition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionMoveIn
        transition.subtype = kCATransitionFromBottom
        self.navigationController?.view.layer.add(transition, forKey: nil)
        self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
extension SelectLocationViewController: GMSAutocompleteViewControllerDelegate
{
   func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace)
   {
      print("Place name: \(place.name)")
      print("Place address: \(place.formattedAddress ?? "null")")
      self.txtLocation.text = place.formattedAddress
      print("Place attributions: \(String(describing: place.attributions))")
      print(place.coordinate.latitude)
      print(place.coordinate.longitude)
      
      let userLocation: NSDictionary = ["lat": place.coordinate.latitude as NSNumber, "long": place.coordinate.longitude as NSNumber]
      UserDefaults.standard.set(userLocation, forKey: "userLocation") //the error is here
      self.navigationItem.leftBarButtonItem?.isEnabled = true

      UserDefaults.standard.set("\(place.formattedAddress!)", forKey: "address")
      UserDefaults.standard.synchronize()
      
      PostLocation(lat: NSNumber(value:place.coordinate.latitude), long: NSNumber(value:place.coordinate.longitude), address: "\(place.formattedAddress!)")
      self.dismiss(animated: true, completion: nil)

   }
   
   func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
      // TODO: handle the error.
      print("Error: \(error.localizedDescription)")
      self.dismiss(animated: true, completion: nil)
   }
   
   // User canceled the operation.
   func wasCancelled(_ viewController: GMSAutocompleteViewController) {
      print("Autocomplete was cancelled.")
      self.dismiss(animated: true, completion: nil)
   }
}

class LocationCell : UITableViewCell
{
    @IBOutlet var lblTitle : UILabel!
    @IBOutlet var lblDesc : UILabel!
}
