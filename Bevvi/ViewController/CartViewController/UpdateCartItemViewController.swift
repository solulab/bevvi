//
//  UpdateCartItemViewController.swift
//  Bevvi
//
//  Created by Hetal Govani on 06/11/17.
//  Copyright © 2017 solulab. All rights reserved.
//

import UIKit
protocol UpdateCartItemDelegate
{
    func UpdateButtonClicked(_ secondDetailViewController: UpdateCartItemViewController)
    func DismissButtonClicked(_ secondDetailViewController: UpdateCartItemViewController)

}
class UpdateCartItemViewController: UIViewController
{
   var delegate: UpdateCartItemDelegate?
   @IBOutlet var lblName : UILabel!
   @IBOutlet var lblType : UILabel!
   @IBOutlet var lblBottleLeft : UILabel!
   @IBOutlet var lblOfferExpires : UILabel!
   @IBOutlet var lblOriginalPrice : UILabel!
   @IBOutlet var lblOfferPrice : UILabel!
   @IBOutlet var lblSize : UILabel!
   @IBOutlet var lblALCContent : UILabel!
   @IBOutlet var lblRegion : UILabel!
   @IBOutlet var lblCountry : UILabel!
   @IBOutlet var lblYear : UILabel!
   @IBOutlet var lblVerietal : UILabel!
   @IBOutlet var lblDescription : UILabel!
   @IBOutlet var btnAddToCart : UIButton!
   @IBOutlet var lblQuantity : UILabel!
   var qty : Int!
   @IBOutlet var viewHeight : NSLayoutConstraint!
   @IBOutlet var viewAddToCart : UIView!
   @IBOutlet var rateView : FloatRatingView!
   var dictDetail : Dictionary<String,AnyObject>! = Dictionary()
   var productDict : Dictionary<String,AnyObject>! = Dictionary()
   var dictOffer : Dictionary<String,AnyObject>! = Dictionary()
   var cartDetail : cartDetail?
    override func viewDidLoad()
    {
      super.viewDidLoad()
      if (UserDefaults.standard.object(forKey: "accountId") == nil)
      {
         dictDetail = cartDetail?.dict
         print(dictDetail)
         
         let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: "$\(dictDetail["originalPrice"]!)")
         attributeString.addAttribute(NSAttributedStringKey.strikethroughStyle, value: 1, range: NSMakeRange(0, attributeString.length))
         attributeString.addAttribute(NSAttributedStringKey.strikethroughColor, value: redColor, range: NSMakeRange(0, attributeString.length))
         
         
         lblOfferPrice.text = "$\(dictDetail["salePrice"]!)"
         lblBottleLeft.text = "\(dictDetail["remQty"]!) out \(dictDetail["totalQty"]!) bottles left"
         lblOriginalPrice.attributedText = attributeString
         qty = Int("\(cartDetail!.quantity!)")
      }
      else
      {
         dictOffer = self.dictDetail["offer"] as! Dictionary<String,AnyObject>
         
         let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: "$\(dictOffer["originalPrice"]!)")
         attributeString.addAttribute(NSAttributedStringKey.strikethroughStyle, value: 1, range: NSMakeRange(0, attributeString.length))
         attributeString.addAttribute(NSAttributedStringKey.strikethroughColor, value: redColor, range: NSMakeRange(0, attributeString.length))

         lblOfferPrice.text = "$\(dictOffer["salePrice"]!)"
         lblBottleLeft.text = "\(dictOffer["remQty"]!) out \(dictOffer["totalQty"]!) bottles left"
         lblOriginalPrice.attributedText = attributeString
         qty = Int("\(dictDetail["quantity"]!)")

      }
      productDict = self.dictDetail["product"] as! Dictionary<String,AnyObject>
      
      lblName.text = "\(productDict["name"]!)"
      lblType.text = "\(productDict["category"]!)"
      rateView.rating = Float("\(productDict["rating"]!)")!
      
      if let val = productDict["size"] as? String
      {
         lblSize.text = "\(val)"
      }
      if let val = productDict["alcContent"] as? String
      {
         lblALCContent.text = "\(val)"
      }
      if let val = productDict["region"] as? String
      {
         lblRegion.text = "\(val)"
      }
      if let val = productDict["country"] as? String
      {
         lblCountry.text = "\(val)"
      }
      if let val = productDict["year"] as? String
      {
         lblYear.text = "\(val)"
      }
      if let val = productDict["varietal"] as? String
      {
         lblVerietal.text = "\(val)"
      }
      if let val = productDict["description"] as? String
      {
         lblDescription.text = "\(val)"
      }
      
      self.viewHeight.constant = estimatedHeightOfLabel(view: view, text: lblDescription.text!) + lblDescription.frame.origin.y + 50
      
      lblQuantity.text = "\(qty!)"
    }
   @IBAction func btnPlusPress(sender:UIButton)
   {
      qty = qty + 1

      if (UserDefaults.standard.object(forKey: "accountId") == nil)
      {
         if qty > Int("\(dictDetail["remQty"]!)")!{
            qty = Int("\(dictDetail["remQty"]!)")!
         }
      }
      else
      {
         if qty > Int("\(dictOffer["remQty"]!)")!{
            qty = Int("\(dictOffer["remQty"]!)")!
         }
      }
     
      lblQuantity.text = "\(qty!)"
   }
   @IBAction func btnMinusPress(sender:UIButton)
   {
      qty = qty - 1
      if qty <= 1
      {
         qty = 1
         lblQuantity.text = "\(qty!)"
      }
      else
      {
         lblQuantity.text = "\(qty!)"
      }
   }
    @IBAction func UpdateButtonClicked(sender:UIButton)
    {
      delegate?.UpdateButtonClicked(self)
    }
    @IBAction func btnDismissPress(sender:UIButton)
    {
        delegate?.DismissButtonClicked(self)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
