//
//  ShopDetailViewController.swift
//  Bevvi
//
//  Created by Hetal Govani on 25/10/17.
//  Copyright © 2017 solulab. All rights reserved.
//

import UIKit
import MapKit

class ShopDetailViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate, UITableViewDelegate, UITableViewDataSource,CLLocationManagerDelegate,MKMapViewDelegate
{
   @IBOutlet var tblObj : UITableView!
   @IBOutlet var collectionObj : UICollectionView!
//       @IBOutlet var mapView : GMSMapView!
   @IBOutlet weak var mapView: MKMapView!
   var strEstablishmentId : String!
   var offerId : String!
   
   private var locationManager: CLLocationManager!
   private var currentLocation: CLLocation?
   var arrCategories : Array<String>! = Array()
   var arrTableCategories : Array<String>! = Array()

   var arrList : Array<Dictionary<String,AnyObject>>! = Array()
   var arrStoreArray : Dictionary<String,AnyObject>! = Dictionary()
   var arrFilter : Array<Dictionary<String,AnyObject>>! = Array()

   var arrCoord : Array<NSNumber>! = Array()
   var strTitle : String!
   @IBOutlet var lblAddress : UILabel!
   @IBOutlet var lblDistance : UILabel!
   lazy var errorView: ErrorView = {
      let Errview = UINib(nibName: "ErrorView", bundle: nil).instantiate(withOwner: self, options: nil)[0] as! ErrorView
      Errview.translatesAutoresizingMaskIntoConstraints = false
      Errview.retryButton.addTarget(self, action: #selector(btnRetryPress(sender:)), for: .touchUpInside)
      return Errview
   }()
   func presentError(_ error: customError) {
      self.errorView.error = error
      
      DispatchQueue.main.async
         {
         self.view.addSubview(self.errorView)
         self.installErrorViewConstraints()
      }
   }
   
   func installErrorViewConstraints() {
      self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[view]|", options: [], metrics: nil, views: [ "view": self.errorView ]))
      self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[view]|", options: [], metrics: nil, views: [ "view": self.errorView ]))
   }
   @IBAction func btnRetryPress(sender:UIButton)
   {
      errorView.removeFromSuperview()
      if appDel.reachability.connection != .none
      {
         productListingAPICall()
      }
      else
      {
         self.presentError(.NetworkError)
      }
   }
   override func viewDidLoad()
   {
      super.viewDidLoad()
      
      self.title = strTitle
      
      let button1 = UIButton(type: UIButtonType.custom)
      button1.setImage(#imageLiteral(resourceName: "Back"), for: UIControlState.normal)
      button1.addTarget(self, action:#selector(btnBackPress(sender:)), for: UIControlEvents.touchUpInside)
      button1.frame=CGRect.init(x: 0, y: 0, width: 30, height: 30)
      let barButton1 = UIBarButtonItem(customView: button1)
      self.navigationItem.leftBarButtonItem = barButton1
      
      tblObj.tableFooterView = UIView()
      
      mapView.delegate = self
      
      locationManager = CLLocationManager()
      locationManager.delegate = self
      locationManager.desiredAccuracy = kCLLocationAccuracyBest
      
      // Check for Location Services
      DispatchQueue.main.async {
         self.setMapView()
      }
      
      self.arrCategories = ["ALL","WINE","LIQUOR","BEER","MIXER"]
      if appDel.reachability.connection != .none
      {
         productListingAPICall()
      }
      else
      {
         self.presentError(.NetworkError)
      }
   }
   
   func setMapView()
   {
      let shopLatitude = (arrCoord[1] as NSNumber).doubleValue
      let shopLongitude = (arrCoord[0] as NSNumber).doubleValue
      
      let anno = Annonation.init(coordinate: CLLocationCoordinate2D(latitude: shopLatitude as CLLocationDegrees, longitude: shopLongitude as CLLocationDegrees))
      
      self.mapView.addAnnotation(anno)
      
      let location = CLLocation.init(latitude: shopLatitude as CLLocationDegrees, longitude: shopLongitude as CLLocationDegrees)
      
      let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
      let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate, 1000, 1000)
      mapView.setRegion(coordinateRegion, animated: true)
      print(shopLatitude)
      print(shopLongitude)
      print(arrStoreArray)
      self.lblAddress.text = "\(arrStoreArray["address"]!)"
      let arrDist = arrStoreArray["distinfo"] as! Array<Dictionary<String,AnyObject>>
      let dictDuration = arrDist[0]["duration"] as! Dictionary<String,AnyObject>
      if Int("\(UserDefaults.standard.value(forKey: "walk")!)") == 0 {
         lblDistance.text = "\(dictDuration["text"]!) walk"
      }
      else
      {
         lblDistance.text = "\(dictDuration["text"]!) drive"
      }
   }
   
   func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
      let identifier = "pin"
      
      if annotation.isKind(of: MKUserLocation.classForCoder()) {
         return nil
      }
      var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
      if  annotationView == nil {
         annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: "pin")
         annotationView!.canShowCallout = true
         annotationView!.image = #imageLiteral(resourceName: "location")
      }
      else {
         annotationView!.annotation = annotation
      }
      return annotationView
      
   }
   
   func productListingAPICall()
   {
      print(strEstablishmentId!)
      //http://18.221.89.220:3000/api/offers?filter={"where":{"establishmentId":"5a24a29410785a5c1aa56cde"}, "include":["product"]}
      let para:NSMutableDictionary = NSMutableDictionary()
      
      let geoLocation:NSDictionary = NSDictionary(object: strEstablishmentId!, forKey: "establishmentId" as NSCopying)
      let includeDict:NSMutableArray = ["product"]
      para.setObject(geoLocation, forKey: "where" as NSCopying)
      para.setObject(includeDict, forKey: "include" as NSCopying)
      
      if let theJSONData = try?  JSONSerialization.data(
         withJSONObject: para,options: .prettyPrinted),
         let theJSONText = String(data: theJSONData,encoding: String.Encoding.ascii)
      {
         print("JSON string = \n\(theJSONText)")
         
         let storeDetail = StoreDetails()
         storeDetail.jsonArray = theJSONText
         appDel.showHUD()
         ApiManager.shared.getProductListing(listing: storeDetail, view: self.view, completion: { (arr:[JSONDictionary]) in
            
            self.arrList = arr
            self.arrFilter = arr
            
           
            let group = DispatchGroup()
            group.enter()
            
            DispatchQueue.main.async {
               
               self.tblObj.reloadData()

               group.leave()
            }
            
            // does not wait. But the code in notify() is run
            // after enter() and leave() calls are balanced
            
            group.notify(queue: .main)
            {
               
               self.collectionObj.selectItem(at: IndexPath.init(row: 0, section: 0), animated: true, scrollPosition: .top)
               self.collectionObj.delegate?.collectionView!(self.collectionObj, didSelectItemAt: IndexPath.init(row: 0, section: 0))
//               var setA = Set(arrayA)
//               var setB = Set(arrayB)
//               let arrTemp = Array(Set(self.arrCategories).subtract(Set(self.arrTableCategories)))
//               print(arrTemp)
//               for i in 0 ..< self.arrCategories.count
//               {
//                  if i != 0
//                  {
//                     let filter = self.arrList.filter
//                     { (dict) -> Bool in
//                        if let product = dict["product"] as? [String:Any]
//                        {
//                           print("\(self.arrCategories[i])".capitalized)
//                           return "\(String(describing: product["category"]!))" == "\(self.arrCategories[i])".capitalized
//                        }
//                        return false
//                     }
//                     print(filter)
//                     if filter.count == 0
//                     {
//                        let indexOfA = self.arrCategories.index(of:"\(self.arrCategories[i])")
//
//                        let cell = self.collectionObj.cellForItem(at: IndexPath.init(item: indexOfA!, section: 0)) as? ProductCell
//                        cell?.backgroundColor = UIColor.gray
//                        cell?.isUserInteractionEnabled = false
//                     }
//                  }
//               }
            }

            appDel.hideHUD()
         })
      }
   }
   func openMapForPlace(location: CLLocation)
   {
      let latitude: CLLocationDegrees = location.coordinate.latitude
      let longitude: CLLocationDegrees = location.coordinate.longitude
      
      let regionDistance:CLLocationDistance = 10000
      let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
      let regionSpan = MKCoordinateRegionMakeWithDistance(coordinates, regionDistance, regionDistance)
      let options = [
         MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
         MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
      ]
      let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
      let mapItem = MKMapItem(placemark: placemark)
      mapItem.name = "Place Name"
      mapItem.openInMaps(launchOptions: options)
   }
   
   // MARK - CLLocationManagerDelegate
   
   func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
   {
      defer
      {
         currentLocation = locations.last
      }
      if currentLocation == nil
      {
         // Zoom to user location
         if let userLocation = locations.last
         {
            let viewRegion = MKCoordinateRegionMakeWithDistance(userLocation.coordinate, 2000, 2000)
            mapView.setRegion(viewRegion, animated: false)
         }
      }
   }
   
   @IBAction func btnMapPress(sender:UIButton)
   {
      let alert = UIAlertController.init(title: APP_NAME, message: "Open Map?", preferredStyle: .alert)
      alert.addAction(UIAlertAction.init(title: "Open", style: UIAlertActionStyle.default, handler: { (action) in
         
         let shopLatitude = (self.arrCoord[1] as NSNumber).doubleValue
         let shopLongitude = (self.arrCoord[0] as NSNumber).doubleValue
         let homeLocation = CLLocation(latitude: shopLatitude, longitude: shopLongitude)
         self.mapView.showsUserLocation = true
         self.openMapForPlace(location: homeLocation)
      }))
      alert.addAction(UIAlertAction.init(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
      self.present(alert, animated: true, completion: nil)
   }
   
   @IBAction func btnBackPress(sender:UIButton)
   {
      self.navigationController?.popViewController(animated: true)
   }
   
   //MARK: - Tableview delegate method
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
   {
      return arrFilter.count
   }
   
   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
   {
      let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ProductDetailTableCell
      
      cell.selectionStyle = .none
      
      let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: "$\(arrList[indexPath.row]["originalPrice"]!)")
      attributeString.addAttribute(NSAttributedStringKey.strikethroughStyle, value: 1, range: NSMakeRange(0, attributeString.length))
      attributeString.addAttribute(NSAttributedStringKey.strikethroughColor, value: redColor, range: NSMakeRange(0, attributeString.length))
      
      let productDict = arrFilter[indexPath.row]["product"] as! Dictionary<String,AnyObject>
      
      cell.lblName.text = "\(productDict["name"]!)"
      cell.lblType.text = "\(productDict["category"]!)"
      if !arrTableCategories.contains("\(productDict["category"]!)")
      {
         arrTableCategories.append("\(productDict["category"]!)")
      }
      print(arrTableCategories)
      cell.rateView.rating = Float("\(productDict["rating"]!)")!
      cell.lblOriginalPrice.attributedText = attributeString
      cell.lblOfferPrice.text = "$\(arrFilter[indexPath.row]["salePrice"]!)"
      
      return cell
   }
   
   func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
   {
      let detailObj = self.storyboard?.instantiateViewController(withIdentifier: "ProductDetailViewController") as! ProductDetailViewController
      detailObj.dictDetail = arrFilter[indexPath.row]
      detailObj.address = "\(arrStoreArray["address"]!)"
      detailObj.shopName = strTitle
      self.navigationController?.pushViewController(detailObj, animated: true)
   }
   
   //MARK: - Collectionview delegate method
   func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
   {
      return arrCategories.count
   }
   
   func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
   {
      let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ProductCell
      cell.layer.cornerRadius = 5

      cell.lblName.text = "\(arrCategories[indexPath.row])"
      
//      if indexPath.row != 0
//      {
//         let filter = self.arrList.filter
//         { (dict) -> Bool in
//            if let product = dict["product"] as? [String:Any]
//            {
//               print("\(self.arrCategories[indexPath.row])".capitalized)
//               return "\(String(describing: product["category"]!))" == "\(self.arrCategories[indexPath.row])".capitalized
//            }
//            return false
//         }
//         print(filter)
//         if filter.count == 0
//         {
//            cell.backgroundColor = UIColor.gray
//            cell.isUserInteractionEnabled = false
//         }
//      }
      return cell
   }
   func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
   {
      if "\(arrCategories[indexPath.row])" == "ALL"
      {
         arrFilter = arrList
      }
      else
      {
         let filter = arrList.filter { (dict) -> Bool in
            if let product = dict["product"] as? [String:Any]
            {
               print("\(arrCategories[indexPath.row])".capitalized)
               return "\(String(describing: product["category"]!))" == "\(arrCategories[indexPath.row])".capitalized
            }
            return false
         }
         print(filter) // ["b": 42]
         arrFilter = filter
         
      }
      tblObj.reloadData()
      let cell = collectionView.cellForItem(at: indexPath) as? ProductCell
      cell?.backgroundColor = MaroonColor
   }
   func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
      let cell = collectionView.cellForItem(at: indexPath) as? ProductCell
      cell?.backgroundColor = redColor
   }
   override func didReceiveMemoryWarning()
   {
      super.didReceiveMemoryWarning()
   }
}

class ProductCell: UICollectionViewCell
{
    @IBOutlet var lblName : UILabel!
}

class ProductDetailTableCell: UITableViewCell
{
    @IBOutlet var lblName : UILabel!
    @IBOutlet var lblType : UILabel!
    @IBOutlet var lblOriginalPrice : UILabel!
    @IBOutlet var lblOfferPrice : UILabel!
    @IBOutlet var lblOfferExpires : UILabel!
   @IBOutlet var rateView : FloatRatingView!
}
class Annonation: NSObject, MKAnnotation {
   var title: String?
   var coordinate: CLLocationCoordinate2D
   var subtitle: String?
   var tag: NSNumber!
   init(coordinate: CLLocationCoordinate2D) {
      self.coordinate = coordinate
   }
}
extension String {
   func firstCharacterUpperCase() -> String {
      if let firstCharacter = self.first {
         return replacingCharacters(in: startIndex..<index(after: startIndex), with: String(firstCharacter).uppercased())
      }
      return ""
   }
}
