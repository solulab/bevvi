//
//  HomeViewController.swift
//  Bevvi
//
//  Created by Hetal Govani on 24/10/17.
//  Copyright © 2017 solulab. All rights reserved.
//

import UIKit
import CoreLocation
import MBProgressHUD
import Alamofire

class HomeViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,RateUsDelegate,EnterEmailDelegate
{
   @IBOutlet var tblObj : UITableView!
   var cnt : Int! = 0
   var arrList : Array<Dictionary<String,Any>> = Array()
   var radiusInMiles : CGFloat! = 5
   @IBOutlet var viewBringToCity : UIView!
   @IBOutlet var btnBringToCity : UIButton!
   lazy var errorView: ErrorView =
   {
      let Errview = UINib(nibName: "ErrorView", bundle: nil).instantiate(withOwner: self, options: nil)[0] as! ErrorView
      Errview.translatesAutoresizingMaskIntoConstraints = false
      Errview.retryButton.addTarget(self, action: #selector(btnRetryPress(sender:)), for: .touchUpInside)
      return Errview
   }()
   override func viewDidLoad()
   {
      super.viewDidLoad()
      
      tblObj.tableFooterView = UIView()
      UIApplication.shared.statusBarStyle = UIStatusBarStyle.lightContent

      NotificationCenter.default.addObserver(self, selector: #selector(reloadMap), name: NSNotification.Name(rawValue: "reloadMap"), object: nil)
      if UserDefaults.standard.object(forKey: "accountId") != nil
      {
         appDel.startSTPSession()
      }
   }
   func presentError(_ error: customError) {
      self.errorView.error = error
      
      DispatchQueue.main.async {
         self.view.addSubview(self.errorView)
         self.installErrorViewConstraints()
      }
   }
   
   func installErrorViewConstraints() {
      self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[view]|", options: [], metrics: nil, views: [ "view": self.errorView ]))
      self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[view]|", options: [], metrics: nil, views: [ "view": self.errorView ]))
   }
   override func viewWillAppear(_ animated: Bool) {
      super.viewWillAppear(true)
      if UserDefaults.standard.object(forKey: "walk") == nil
      {
         UserDefaults.standard.set(0, forKey: "walk")
         UserDefaults.standard.synchronize()
      }
      setupView()
      if let dict = UserDefaults.standard.value(forKey: "userLocation") as? NSDictionary
      {
         if appDel.reachability.connection != .none
         {
            errorView.removeFromSuperview()
            StoreListingCall()
         }
         else
         {
            self.presentError(.NetworkError)
         }
      }
      else
      {
         btnSelectLocation(sender: UIButton())
      }
      //      DispatchQueue.main.async {
      //      }
      
      /*   let isCartEmpty = UserDefaults.standard.object(forKey: "isCartEmpty") as! Bool
       
       if isCartEmpty == true
       {
       cnt = 0
       }
       else
       {
       cnt = 10
       tblObj.reloadData()
       let isRateDisplayed = UserDefaults.standard.object(forKey: "isRateDisplayed") as! Bool
       
       if isRateDisplayed == false
       {
       let ratingVC = RateUsViewController(nibName: "RateUsViewController", bundle: nil)
       ratingVC.delegate=self
       self.presentPopupViewController(ratingVC, animationType: MJPopupViewAnimationFade)
       UserDefaults.standard.set(true, forKey: "isRateDisplayed")
       UserDefaults.standard.synchronize()
       }
       }
       
       if cnt == 0
       {
       UtilityClass.showAlert("Sorry, we're not available in your area. We'll do our best to get you soon!")
       }*/
   }
   
   func setupView()
   {
      let titleview = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 200, height: 44))
      let label = UILabel.init(frame: CGRect.init(x: 0, y: 0, width: 180, height: 44))
      var address : String! = "Set Location"
      if let str = UserDefaults.standard.value(forKey: "address") as? String
      {
         let arr = "\(str)".components(separatedBy: ",")
         address = "\(arr[0])"
      }
      label.text = address
      label.font = UIFont(name: "Avenir-Medium", size: 15)
      label.sizeToFit()
      label.center.y = titleview.center.y
      label.textColor = UIColor.white
      titleview.addSubview(label)
      
      let imgView = UIImageView.init(frame: CGRect.init(x: label.frame.width + 5, y: 0, width: 13, height: 44))
      imgView.image = #imageLiteral(resourceName: "arrow")
      imgView.contentMode = .scaleAspectFit
      titleview.addSubview(imgView)
      
      titleview.frame = CGRect.init(x: 0, y: 0, width: label.frame.width + imgView.frame.width, height: 44)
      
      let btn = UIButton.init(frame: titleview.frame)
      btn.addTarget(self, action: #selector(btnSelectLocation(sender:)), for: .touchUpInside)
      titleview.addSubview(btn)
      
      self.navigationItem.titleView = titleview
      let left = UISwipeGestureRecognizer(target: self, action: #selector(swipeLeft))
      left.direction = .left
      self.view.addGestureRecognizer(left)
      
      viewBringToCity.isHidden = true
      
   }
   @objc func reloadMap() {
      setupView()
      errorView.removeFromSuperview()
      if appDel.reachability.connection != .none
      {
         StoreListingCall()
      }
      else
      {
         self.presentError(.NetworkError)
      }
   }
   @IBAction func btnRetryPress(sender:UIButton)
   {
      errorView.removeFromSuperview()
      if appDel.reachability.connection != .none
      {
         StoreListingCall()
      }
      else
      {
         self.presentError(.NetworkError)
      }
   }
   func StoreListingCall()
   {
      appDel.hideHUD()
      if let dict = UserDefaults.standard.value(forKey: "userLocation") as? NSDictionary
      {
         let latitude = CLLocationDegrees(truncating: dict.object(forKey: "lat") as! NSNumber)
         //
         let longitude = CLLocationDegrees(truncating: dict.object(forKey: "long") as! NSNumber)

            let storelist = StoreListing()
            appDel.showHUD()
//         -72.75585772715851
//         40.956148571075765
         let radious = 1609.34 * Double("\(UserDefaults.standard.value(forKey: "maxRadious")!)")! as! Double
         ApiManager.shared.getStoreListing(longitude: NSNumber(value:longitude), latitude: NSNumber(value:latitude), mindistance: 0, maxdistance: radious, limit: 25, transit: Int("\(UserDefaults.standard.value(forKey: "walk")!)")!, listing: storelist, view: self.view) { (arr:[JSONDictionary]) in
               self.arrList = arr
               if arr.count == 0
               {
                  self.viewBringToCity.isHidden = false
               }
               print(self.arrList)
               self.tblObj.reloadData()
               appDel.hideHUD()
            }
      }
   }
  
   func SubmitButtonClicked(_ secondDetailViewController: RateUsViewController)
   {
      self.dismissPopupViewControllerWithanimationType(MJPopupViewAnimationFade)
   }
   func DismissButtonClicked(_ secondDetailViewController: RateUsViewController) {
      self.dismissPopupViewControllerWithanimationType(MJPopupViewAnimationFade)
   }
   
   //MARK: - Tableview delegate method
   
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
   {
      return arrList.count
   }
   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! HomeViewCell
      
      cell.selectionStyle = .none
      cell.lblShopName.text = "\(arrList[indexPath.row]["name"]!)"
      cell.lblAddress.text = "\(arrList[indexPath.row]["address"]!)"
      
      let arrOffers = arrList[indexPath.row]["offers"]! as! Array<Dictionary<String,AnyObject>>
      
      cell.lblNoOfDeals.text = "\(arrOffers.count) deals"
      let arrDist = arrList[indexPath.row]["distinfo"] as! Array<Dictionary<String,AnyObject>>
      let dictDuration = arrDist[0]["duration"] as! Dictionary<String,AnyObject>
      if Int("\(UserDefaults.standard.value(forKey: "walk")!)") == 0 {
         cell.lblDistance.text = "\(dictDuration["text"]!) walk"
      }
      else
      {
         cell.lblDistance.text = "\(dictDuration["text"]!) drive"
      }
      if arrOffers.count <= 0
      {
         cell.lblShopName.isEnabled = false
         cell.lblAddress.isEnabled = false
         cell.lblNoOfDeals.isEnabled = false
         cell.lblDistance.isEnabled = false
         cell.imgView.alpha = 0.5
      }
      else
      {
         cell.lblShopName.isEnabled = true
         cell.lblAddress.isEnabled = true
         cell.lblNoOfDeals.isEnabled = true
         cell.lblDistance.isEnabled = true
         cell.imgView.alpha = 1.0
      }
      
      return cell
   }
   func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
   {
      if let arroffers = arrList[indexPath.row]["offers"] as? Array<Dictionary<String,AnyObject>>
      {
         let arrCoord = arrList[indexPath.row]["geoLocation"]! as! Dictionary<String,AnyObject>
         
         if arroffers.count > 0
         {
            let shopDetailObj = self.storyboard?.instantiateViewController(withIdentifier: "ShopDetailViewController") as! ShopDetailViewController
            shopDetailObj.strEstablishmentId = "\(arrList[indexPath.row]["id"]!)"
            shopDetailObj.arrStoreArray = arrList[indexPath.row] as Dictionary<String, AnyObject>

            shopDetailObj.arrCoord = arrCoord["coordinates"] as! Array<NSNumber>
            shopDetailObj.strTitle = "\(arrList[indexPath.row]["name"]!)"
            self.navigationController?.pushViewController(shopDetailObj, animated: true)
         }
      }
   }
   
   @objc func swipeLeft()
   {
      let total = self.tabBarController!.viewControllers!.count - 1
      tabBarController!.selectedIndex = min(total, tabBarController!.selectedIndex + 1)
   }
   func bringBevviyToCity(email:String)
   {
      let storelist = StoreListing()
      appDel.showHUD()
      ApiManager.shared.sendEmail(type: 0, email: email, listing: storelist, view: self.view) { (arr:JSONDictionary) in
         
         appDel.hideHUD()
         print(arr)
         self.btnBringToCity.backgroundColor = YellowColor
         self.btnBringToCity.setTitle("Thank you!", for: .normal)
      }
   }
   @IBAction func btnBringToCity(sender:UIButton)
   {
      if btnBringToCity.titleLabel?.text == "Thank you!"
      {
//         viewBringToCity.isHidden = true
//         btnSelectLocation(sender: UIButton())
      }
      else
      {
         if UserDefaults.standard.object(forKey: "accountId") != nil
         {
            bringBevviyToCity(email: "\(UserDefaults.standard.value(forKey: "email")!)")
         }
         else
         {
            let ratingVC = EnterEmailViewController(nibName: "EnterEmailViewController", bundle: nil)
            ratingVC.strMessage = "Please enter email in box to request bevvi for your area."
            ratingVC.delegate = self
            self.presentPopupViewController(ratingVC, animationType: MJPopupViewAnimationFade)
         }
      }
   }
   func SubmitButtonClicked(_ EnterEmailViewController: EnterEmailViewController)
   {
      self.dismissPopupViewControllerWithanimationType(MJPopupViewAnimationFade)
      bringBevviyToCity(email: EnterEmailViewController.strEmail)
   }
   @IBAction func btnSelectLocation(sender:UIButton)
   {
      let transition = CATransition()
      transition.duration = 0.5
      transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
      transition.type = kCATransitionMoveIn
      transition.subtype = kCATransitionFromTop
      self.navigationController?.view.layer.add(transition, forKey: nil)
      let selectLocationObj = self.storyboard?.instantiateViewController(withIdentifier: "SelectLocationViewController") as! SelectLocationViewController
      self.navigationController?.pushViewController(selectLocationObj, animated: false)
   }
   override func didReceiveMemoryWarning()
   {
      super.didReceiveMemoryWarning()
   }
}
class HomeViewCell: UITableViewCell {
    @IBOutlet var lblShopName : UILabel!
    @IBOutlet var lblNoOfDeals : UILabel!
    @IBOutlet var lblAddress : UILabel!
    @IBOutlet var lblDistance : UILabel!
    @IBOutlet var imgView : UIImageView!
}
