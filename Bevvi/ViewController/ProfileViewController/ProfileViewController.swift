//
//  ProfileViewController.swift
//  Bevvi
//
//  Created by Hetal Govani on 24/10/17.
//  Copyright © 2017 solulab. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController,UITableViewDelegate,UITableViewDataSource
{
   @IBOutlet var tblObj : UITableView!
   @IBOutlet var inviteViewHeight : NSLayoutConstraint!
   @IBOutlet var progressview : UIProgressView!
   @IBOutlet var loginView : UIView!
   var isLogin : Bool!
   @IBOutlet var txtInviteCode : UITextField!
   var arrPending : Array<Dictionary<String,AnyObject>>! = Array()
   var arrPast : Array<Dictionary<String,AnyObject>>! = Array()

   override func viewDidLoad()
   {
      super.viewDidLoad()
      self.title = "Profile"
      self.tabBarItem.title = ""
      
      let button = UIButton(type: UIButtonType.custom)
      button.setImage(#imageLiteral(resourceName: "settings"), for: UIControlState.normal)
      button.addTarget(self, action:#selector(btnSettingPress(sender:)), for: UIControlEvents.touchUpInside)
      button.frame=CGRect.init(x: 0, y: 0, width: 30, height: 30)
      let barButton = UIBarButtonItem(customView: button)
      self.navigationItem.rightBarButtonItem = barButton
      
      progressview.transform = progressview.transform.scaledBy(x: 1, y: 7)
      
      let right = UISwipeGestureRecognizer(target: self, action: #selector(swipeRight))
      right.direction = .right
      self.view.addGestureRecognizer(right)
   }
   
   override func viewWillAppear(_ animated: Bool)
   {
      super.viewWillAppear(true)
      self.navigationController?.navigationBar.isHidden = false
      NotificationCenter.default.addObserver(self, selector: #selector(gotoFirstTab), name: NSNotification.Name(rawValue: "MoveToFirstView"), object: nil)
      NotificationCenter.default.addObserver(self, selector: #selector(reloadView), name: NSNotification.Name(rawValue: "ReloadView"), object: nil)
      
      isLogin = UserDefaults.standard.object(forKey: "isLogin") as! Bool
      
      if isLogin == true
      {
         loginView.isHidden = true
         txtInviteCode.text = "\(UserDefaults.standard.value(forKey: "inviteCode")!)"
         pendingPickups()
         self.navigationItem.rightBarButtonItem?.isEnabled = true
      }
      else
      {
         loginView.isHidden = false
         self.navigationItem.rightBarButtonItem?.isEnabled = false
      }
      inviteViewHeight.constant = 150
   }
   
   @objc func reloadView()
   {
      isLogin = UserDefaults.standard.object(forKey: "isLogin") as! Bool
      
      if isLogin == true
      {
         loginView.isHidden = true
         
         self.navigationItem.rightBarButtonItem?.isEnabled = true
      }
      else
      {
         loginView.isHidden = false
         self.navigationItem.rightBarButtonItem?.isEnabled = false
      }
      NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "ReloadView"), object: nil)
      //        NotificationCenter.default.removeObserver(self)
   }
   func pendingPickups()
   {
      var paraMain : Dictionary<String,AnyObject>! = Dictionary()
      let dictWhere = ["accountId":"\(UserDefaults.standard.value(forKey: "accountId")!)","status":0] as [String : Any]
      let arrFlield : [String] = ["product"]
      let dictorderdetails = ["orderdetails":arrFlield] as [String : AnyObject]
      let arrInclude : Array<AnyObject> = ["establishment" as AnyObject,dictorderdetails as AnyObject]
      paraMain = ["where":dictWhere as AnyObject,"include":arrInclude as AnyObject]
      
      if let theJSONData = try?  JSONSerialization.data(
         withJSONObject: paraMain,
         options: .prettyPrinted
         ),
         let theJSONText = String(data: theJSONData,encoding: String.Encoding.ascii)
      {
         print("JSON string = \n\(theJSONText)")
         appDel.showHUD()
         let StoreList = StoreListing()
         StoreList.jsonArray = theJSONText
         
         ApiManager.shared.orders(listing: StoreList, view: self.view, completion: { (arr:[JSONDictionary]) in
            
            self.arrPending = arr
            self.pastOrders()
         })
      }
   }
   func pastOrders()
   {
      var paraMain : Dictionary<String,AnyObject>! = Dictionary()
      let dictWhere = ["accountId":"\(UserDefaults.standard.value(forKey: "accountId")!)","status":2] as [String : Any]
      let arrFlield : [String] = ["product"]
      let dictorderdetails = ["orderdetails":arrFlield] as [String : AnyObject]

      let arrInclude : Array<AnyObject> = ["establishment" as AnyObject,dictorderdetails as AnyObject]
      paraMain = ["where":dictWhere as AnyObject,"include":arrInclude as AnyObject]
      
      if let theJSONData = try?  JSONSerialization.data(
         withJSONObject: paraMain,options: .prettyPrinted),
         let theJSONText = String(data: theJSONData,
                                  encoding: String.Encoding.ascii) {
         print("JSON string = \n\(theJSONText)")
         let StoreList = StoreListing()
         StoreList.jsonArray = theJSONText
         
         ApiManager.shared.orders(listing: StoreList, view: self.view, completion: { (arr:[JSONDictionary]) in
            
            print(arr)
            self.arrPast = arr
            appDel.hideHUD()
           
            self.tblObj.reloadData()
         })
      }
   }
   @objc func swipeRight()
   {
      tabBarController!.selectedIndex = max(0, tabBarController!.selectedIndex - 1)
   }
   @objc func gotoFirstTab()
   {
      self.tabBarController?.selectedIndex = 0
      NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "MoveToFirstView"), object: nil)
   }
   @IBAction func btnCopyPress(sender:UIButton)
   {
      UIPasteboard.general.string = txtInviteCode.text!
      self.showToast(message: "Copied..!!")
   }
   @IBAction func btnInvitePress(sender:UIButton)
   {
      // text to share
//      Make your hour hap÷py with Bevvi!!!! You friend <name> has given you $5 off your first Bevvi order with promo code <code>. Order now: <link>
      let text = "Make your hour happy with Bevvi!!!! Your friend has given you $5 off on your first Bevvi order with promo code \(txtInviteCode.text!). Order now: https://itunes.apple.com/us/app/bevvi/id1309066556?ls=1&mt=8"
      
      let textToShares = [text]
      
      // set up activity view controller
      let activityViewController = UIActivityViewController(activityItems: textToShares, applicationActivities: nil)
      activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
      
      // exclude some activity types from the list (optional)
      activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
      
      // present the view controller
      self.present(activityViewController, animated: true, completion: nil)
   }
   @IBAction func btnClosePress(sender:UIButton)
   {
      inviteViewHeight.constant = 0
   }
   
   func numberOfSections(in tableView: UITableView) -> Int
   {
      return 2
   }
   
   func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?
   {
      if section == 0
      {
         if arrPending.count == 0
         {
            return ""
         }
         return "PENDING ACCEPTANCE"
      }
      if section == 1
      {
         if arrPast.count == 0
         {
            return ""
         }
         return "PAST ORDERS"
      }
      return ""
   }
   
   func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int)
   {
      guard let header = view as? UITableViewHeaderFooterView else { return }
      header.textLabel?.textColor = UIColor.black
      header.textLabel?.font = UIFont.init(name: "Avenir-Roman", size: 18)
      header.textLabel?.frame = header.frame
      //        header.textLabel?.textAlignment = .center
   }
   func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
   {
      if section == 0
      {
         if arrPending.count == 0
         {
            return 0.01
         }
         return 50
      }
      if section == 1
      {
         if arrPast.count == 0
         {
            return 0.01
         }
         return 40
      }
      return 0
   }
   
   func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
      return 0
   }
   
   func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
      if indexPath.section == 0 {
         return 100
      }
      else
      {
         if let arrOrderDetail = arrPast[indexPath.row]["orderdetails"] as? Array<Dictionary<String,AnyObject>>
         {
            if arrOrderDetail.count == 1
            {
               return 90
            }
            return 130
         }
            return 130
      }
   }
   
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      if section == 0 {
         return arrPending.count
      }
      else
      {
         return arrPast.count
      }
      
   }
   
   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
   {
      if indexPath.section == 0
      {
         let cell = tableView.dequeueReusableCell(withIdentifier: "cellPending", for: indexPath) as! PendingPickupCell
         cell.selectionStyle = .none

         let dictEstablishment = arrPending[indexPath.row]["establishment"] as! [String:AnyObject]
         cell.lblStoreName.text = "\(dictEstablishment["name"]!)"
         cell.lblAdress.text = "\(dictEstablishment["address"]!)"
//         2018-01-10T00:00:00.000Z,
         let RFC3339DateFormatter = DateFormatter()
         RFC3339DateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
         RFC3339DateFormatter.locale = Locale(identifier: "us")
         let dateFormatter = DateFormatter()
         dateFormatter.dateFormat = "MMM dd"

         let date: Date? = RFC3339DateFormatter.date(from: "\(arrPending[indexPath.row]["pickupTime"]!)")
         print(dateFormatter.string(from: date!))
         cell.lblPickup.text = "\(dateFormatter.string(from:date!))"

         return cell
      }
      else
      {
         let cell = tableView.dequeueReusableCell(withIdentifier: "cellPast", for: indexPath) as! PastOrdersCell
         cell.selectionStyle = .none
         let dictEstablishment = arrPast[indexPath.row]["establishment"] as! [String:AnyObject]
         cell.lblStoreName.text = "\(dictEstablishment["name"]!)"
         let RFC3339DateFormatter = DateFormatter()
         RFC3339DateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
         RFC3339DateFormatter.locale = Locale(identifier: "us")
         let dateFormatter = DateFormatter()
         dateFormatter.dateFormat = "MMM dd"
         
         let date: Date? = RFC3339DateFormatter.date(from: "\(arrPast[indexPath.row]["pickupDate"]!)")
         print(dateFormatter.string(from: date!))
         cell.lblDate.text = "\(dateFormatter.string(from: date!))"
         if let arrOrderDetail = arrPast[indexPath.row]["orderdetails"] as? Array<Dictionary<String,AnyObject>>
         {
            for i in 0 ..< arrOrderDetail.count
            {
               let dicProduct = arrOrderDetail[i]["product"] as! [String:AnyObject]
               if i == 0
               {
                  cell.lblRateDescLabel1.text = "\(dicProduct["description"]!)"
                  cell.lblRateTitleLabel1.text = "\(dicProduct["name"]!)"
                  cell.lblRateView1.rating = Float("\(dicProduct["rating"]!)")!
               }
               if i == 1
               {
                  cell.lblRateDescLabel2.text = "\(dicProduct["description"]!)"
                  cell.lblRateTitleLabel2.text = "\(dicProduct["name"]!)"
                  cell.lblRateView2.rating = Float("\(dicProduct["rating"]!)")!
               }
               else
               {
                  cell.lblRateDescLabel2.isHidden = true
                  cell.lblRateTitleLabel2.isHidden = true
                  cell.lblRateView2.isHidden = true
               }
            }
         }
         return cell
      }
   }
   
   func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
   {
      let viewObj = self.storyboard?.instantiateViewController(withIdentifier: "OrderSummaryViewController") as! OrderSummaryViewController
      if indexPath.section == 0
      {
         viewObj.isPickup = true
         viewObj.dictDetail = arrPending[indexPath.row]
      }
      else
      {
         viewObj.isPickup = false
         viewObj.dictDetail = arrPast[indexPath.row]
      }
      viewObj.isFromCheckOut = false
      self.navigationController?.pushViewController(viewObj, animated: true)
   }
   
   @IBAction func btnSettingPress(sender:UIButton)
   {
      let viewObj = self.storyboard?.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
      self.navigationController?.pushViewController(viewObj, animated: true)
   }
   
   @IBAction func btnLoginPress(sender:UIButton)
   {
      let viewObj = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
      self.navigationController?.pushViewController(viewObj, animated: true)
   }
   
   @IBAction func btnSignupPress(sender:UIButton)
   {
      let viewObj = self.storyboard?.instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
      self.navigationController?.pushViewController(viewObj, animated: true)
   }
   
   override func didReceiveMemoryWarning()
   {
      super.didReceiveMemoryWarning()
   }
}
class PastOrdersCell: UITableViewCell
{
    @IBOutlet var lblimgView : UIImageView!
    @IBOutlet var lblDate : UILabel!
    @IBOutlet var lblStoreName : UILabel!
    
    @IBOutlet var lblRateTitleLabel1 : UILabel!
    @IBOutlet var lblRateDescLabel1 : UILabel!
    @IBOutlet var lblRateView1 : FloatRatingView!

    @IBOutlet var lblRateTitleLabel2 : UILabel!
    @IBOutlet var lblRateDescLabel2 : UILabel!
    @IBOutlet var lblRateView2 : FloatRatingView!
}

class PendingPickupCell: UITableViewCell
{
    @IBOutlet var lblimgView : UIImageView!
    @IBOutlet var lblPickup : UILabel!
    @IBOutlet var lblStoreName : UILabel!
    @IBOutlet var lblAdress : UILabel!
}
