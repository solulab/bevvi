//
//  AddCardViewController.swift
//  Bevvi
//
//  Created by Hetal Govani on 10/11/17.
//  Copyright © 2017 solulab. All rights reserved.
//dd

import UIKit
import Stripe

protocol ReloadCardDelegate {
   func reloadCards()
}

class AddCardViewController: UIViewController, UITextFieldDelegate {
   
   @IBOutlet var viewHeight : NSLayoutConstraint!
   
   @IBOutlet var txtCardNumber : customTextField!
   @IBOutlet var txtCVVNumber : customTextField!
   @IBOutlet var txtDateNumber : customTextField!

   @IBOutlet var txtNameOnCard : customBottomLineTextField!
   var paymentTextField: STPPaymentCardTextField! = nil
   var submitButton: UIButton! = nil
   
   @IBOutlet var txtName: customBottomLineTextField!
   @IBOutlet var txtAddress: customBottomLineTextField!
   @IBOutlet var txtApt: customBottomLineTextField!
   @IBOutlet var txtCity: customBottomLineTextField!
   @IBOutlet var txtState: customBottomLineTextField!
   @IBOutlet var txtZip: customBottomLineTextField!
   
   var apiAdapter: STPBackendAPIAdapter!
   
   var delegate: ReloadCardDelegate!
   
   override func viewDidLoad()
   {
      super.viewDidLoad()
      
      self.title = "Add Payment"
      viewHeight.constant = self.view.frame.height - 114
      
      let button1 = UIButton(type: UIButtonType.custom)
      button1.setImage(#imageLiteral(resourceName: "Back"), for: UIControlState.normal)
      button1.addTarget(self, action:#selector(btnBackPress(sender:)), for: UIControlEvents.touchUpInside)
      button1.frame=CGRect.init(x: 0, y: 0, width: 30, height: 30)
      let barButton1 = UIBarButtonItem(customView: button1)
      self.navigationItem.leftBarButtonItem = barButton1
      txtDateNumber.delegate = self
   }
   
   @IBAction func btnBackPress(sender:UIButton)
   {
      self.navigationController?.popViewController(animated: true)
   }
   
   override func didReceiveMemoryWarning()
   {
      super.didReceiveMemoryWarning()
   }
   
   func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
      if textField == txtDateNumber {
         
         // check the chars length dd -->2 at the same time calculate the dd-MM --> 5
         if (txtDateNumber?.text?.characters.count == 2) {
            //Handle backspace being pressed
            if !(string == "") {
               // append the text
               txtDateNumber?.text = (txtDateNumber?.text)! + "/"
            }
         }
         // check the condition not exceed 9 chars
         return !(textField.text!.characters.count > 4 && (string.characters.count ) > range.length)
      }
      else {
         return true
      }
   }
   
   @IBAction func addPaymentCard() {
      
      let card = STPCardParams.init()
      card.number = txtCardNumber.text!
      card.name = self.txtName.text!
      
      let address = STPAddress.init()
      address.line1 = self.txtAddress.text!
      address.line2 = self.txtApt.text!
      address.city = self.txtCity.text!
      address.state = self.txtState.text!
      address.postalCode = self.txtZip.text!
      
      card.address = address
      card.cvc = self.txtCVVNumber.text!
      
      if let dateObj = self.txtDateNumber.text {
         let arrObj = dateObj.components(separatedBy: "/")
         if arrObj.count > 1{
            card.expMonth = UInt.init(exactly: Float.init(arrObj[0])!)!
            card.expYear = UInt.init(exactly: Float.init(arrObj[1])!)!
         }
         
      }
      
      
      appDel.showHUD()
      let cardSouce = STPSourceParams.cardParams(withCard: card)
      
      STPAPIClient.shared().createSource(with: cardSouce) { (source, error) in
         if error == nil {
            appDel.customerContext.attachSource(toCustomer: source!, completion: { (error) in
               
               if error == nil {
                  self.delegate.reloadCards()
                  self.navigationController?.popViewController(animated: true)
                  appDel.hideHUD()
               } else {
                  appDel.hideHUD()
                  UtilityClass.showAlert("something went wrong")
               }
            })
         } else {
            UtilityClass.showAlert("something went wrong")
            appDel.hideHUD()
         }
      }
   }
}
