//
//  EnterEmailViewController.swift
//  Bevvi
//
//  Created by solulab on 1/9/18.
//  Copyright © 2018 solulab. All rights reserved.
//

import UIKit

protocol EnterEmailDelegate
{
   func SubmitButtonClicked(_ EnterEmailViewController: EnterEmailViewController)
}
class EnterEmailViewController: UIViewController
{
   @IBOutlet var txtEmail : customTextField!
   @IBOutlet var lblAlert : UILabel!
   @IBOutlet var lblMessage : UILabel!
   var strMessage : String!
   var strEmail : String!
   var delegate: EnterEmailDelegate?

   override func viewDidLoad()
   {
      super.viewDidLoad()
      lblAlert.isHidden = true
      lblMessage.text = strMessage

   }
   
   @IBAction func btnDonePress(sender:UIButton)
   {
      lblAlert.isHidden = true
      if txtEmail.text?.count != 0
      {
         strEmail = txtEmail.text!

         delegate?.SubmitButtonClicked(self)
      }
      else
      {
         lblAlert.text = "Please enter email address."
         lblAlert.isHidden = false
      }
   }
   override func didReceiveMemoryWarning()
   {
      super.didReceiveMemoryWarning()
   }
}
