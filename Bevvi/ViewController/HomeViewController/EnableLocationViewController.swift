//
//  EnableLocationViewController.swift
//  Bevvi
//
//  Created by Hetal Govani on 01/01/18.
//  Copyright © 2018 solulab. All rights reserved.
//

import UIKit
import CoreLocation
import GooglePlaces
import GoogleMaps

class EnableLocationViewController: UIViewController,CLLocationManagerDelegate,UITextFieldDelegate,UISearchDisplayDelegate {

   var locManager = CLLocationManager()
   let locationManager = CLLocationManager()
   @IBOutlet var txtLocation: UITextField!
   
   @IBOutlet var viewLocaion : UIView!
   
   override func viewDidLoad()
   {
      super.viewDidLoad()
      viewLocaion.isHidden = true
      UIApplication.shared.statusBarStyle = UIStatusBarStyle.default
   }
   
   @IBAction func btnCurrentLocationSet(sender:UIButton)
   {
      setupLocationManager()
   }
   @IBAction func btnEnablePress(sender:UIButton)
   {
      self.setupLocationManager()
   }
   @IBAction func btnNotNowPress(sender:UIButton)
   {
//      self.setupLocationManager()
      viewLocaion.isHidden = false
      
   }
   @IBAction func btnBackPress(sender:UIButton)
   {
      //      self.setupLocationManager()
      viewLocaion.isHidden = true
   }
   func setupLocationManager()
   {
      appDel.showHUD()
      locManager = CLLocationManager()
      locManager.delegate = self
      locManager.requestAlwaysAuthorization()
      locManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
      locManager.startUpdatingLocation()
   }
   // Below method will provide you current location.
   func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
   {
      appDel.showHUD()
      currentLocation = locations.last
      locManager.stopMonitoringSignificantLocationChanges()
      if let locationValue = manager.location?.coordinate
      {
         print("locations = \(locationValue)")
         let lat = NSNumber(value: locationValue.latitude)
         let lon = NSNumber(value: locationValue.longitude)
         let userLocation: NSDictionary = ["lat": lat, "long": lon]
         UserDefaults.standard.set(userLocation, forKey: "userLocation") //the error is here
         UserDefaults.standard.synchronize()
         
         locManager.stopUpdatingLocation()
         getAddressFromLatLon(pdblLatitude:lat,withLongitude:lon)
         NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadMap"), object: nil)
      }
   }
   func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
      print("Error")
      if CLLocationManager.locationServicesEnabled()
      {
         switch CLLocationManager.authorizationStatus() {
         case .notDetermined, .restricted, .denied:
            appDel.hideHUD()
            print("No access")
            let alert = UIAlertController.init(title: APP_NAME, message: "Please enable location service for this app in ALLOW LOCATION ACCESS: Always, Go to Setting?", preferredStyle: .alert)
            alert.addAction(UIAlertAction.init(title: "cancel", style: UIAlertActionStyle.cancel, handler: nil))
            alert.addAction(UIAlertAction.init(title: "Open Setting", style: UIAlertActionStyle.default, handler: { (action) in
               guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                  return
               }
               if UIApplication.shared.canOpenURL(settingsUrl) {
                  UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                     print("Settings opened: \(success)") // Prints true
                  })
               }
            }))
            self.present(alert, animated: true, completion: nil)
         case .authorizedAlways, .authorizedWhenInUse:
            print("Access")
            appDel.hideHUD()
         }
      } else {
         print("No Access")
         appDel.hideHUD()
      }
   }
   func textFieldDidBeginEditing(_ textField: UITextField)
   {
      let acController = GMSAutocompleteViewController()
      acController.delegate = self
      //      acController.
      self.present(acController, animated: true, completion: nil)
   }
   func getAddressFromLatLon(pdblLatitude: NSNumber, withLongitude pdblLongitude: NSNumber) {
      var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
      let lat: Double = Double(pdblLatitude)
      //21.228124
      let lon: Double = Double(pdblLongitude)
      //72.833770
      let ceo: CLGeocoder = CLGeocoder()
      center.latitude = lat
      center.longitude = lon
      
      let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
      
      ceo.reverseGeocodeLocation(loc, completionHandler:
         {(placemarks, error) in
            if (error != nil)
            {
               print("reverse geodcode fail: \(error!.localizedDescription)")
               appDel.hideHUD()
               UtilityClass.showAlert("Not able to get location. Please try after some time.")
            }
            if let pm = placemarks as? [CLPlacemark]
            {
               if pm.count > 0 {
                  let pm = placemarks![0]

                  var addressString : String = ""
                  if pm.subLocality != nil {
                     addressString = addressString + pm.subLocality! + ", "
                  }
                  if pm.thoroughfare != nil {
                     addressString = addressString + pm.thoroughfare! + ", "
                  }
                  if pm.locality != nil {
                     addressString = addressString + pm.locality! + ", "
                  }
                  if pm.country != nil {
                     addressString = addressString + pm.country! + ", "
                  }
                  if pm.postalCode != nil {
                     addressString = addressString + pm.postalCode! + " "
                  }
                  
                  print(addressString)
                  UserDefaults.standard.set(addressString, forKey: "address")
                  UserDefaults.standard.set(true, forKey: "isFirstTime")
                  UserDefaults.standard.synchronize()
                  NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadMap"), object: nil)
                  appDel.hideHUD()

                  let tabbarController = self.storyboard?.instantiateViewController(withIdentifier: "CustomTabBarViewController") as! CustomTabBarViewController
                  
                  appDel.window?.rootViewController = tabbarController
               }
            }
      })
   }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
extension EnableLocationViewController: GMSAutocompleteViewControllerDelegate
{
   func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace)
   {
      self.dismiss(animated: true, completion: nil)

      print("Place name: \(place.name)")
      print("Place address: \(place.formattedAddress ?? "null")")
      self.txtLocation.text = place.formattedAddress
      print("Place attributions: \(String(describing: place.attributions))")
      print(place.coordinate.latitude)
      print(place.coordinate.longitude)
      
      let userLocation: NSDictionary = ["lat": place.coordinate.latitude as NSNumber, "long": place.coordinate.longitude as NSNumber]
      
      UserDefaults.standard.set(userLocation, forKey: "userLocation") //the error is here
      UserDefaults.standard.set("\(place.formattedAddress!)", forKey: "address")
      UserDefaults.standard.set(true, forKey: "isFirstTime")
      UserDefaults.standard.synchronize()
      
      let tabbarController = self.storyboard?.instantiateViewController(withIdentifier: "CustomTabBarViewController") as! CustomTabBarViewController
      appDel.window?.rootViewController = tabbarController
   }
   
   func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error)
   {
      // TODO: handle the error.
      print("Error: \(error.localizedDescription)")
      self.dismiss(animated: true, completion: nil)
   }
   
   // User canceled the operation.
   func wasCancelled(_ viewController: GMSAutocompleteViewController) {
      print("Autocomplete was cancelled.")
      self.dismiss(animated: true, completion: nil)
   }
}
