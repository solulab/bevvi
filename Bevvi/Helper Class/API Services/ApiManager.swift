//
//  ApiManager.swift
//  OyeDeals
//
//  Created by Ankit on 9/15/17.
//  Copyright © 2017 Solulab. All rights reserved.
//

import Foundation
import Alamofire
import MBProgressHUD
typealias JSONDictionary = [String:AnyObject]

public class ApiManager
{
    
   static let shared: ApiManager = ApiManager()
   
   
   //MARK:- check FB account is created or not
   func checkFBID(listing: StoreListing,view:UIView, completion:@escaping (_ arr: [JSONDictionary]) -> ())
   {
      Alamofire.request(kcrateAccountFB, method: .get, parameters: ["filter":listing.jsonArray]).responseJSON { (response:DataResponse<Any>) in
         
         print(response.result)
         
         if let json = response.result.value as? [JSONDictionary]
         {
            print(json)
            completion(json)
         }
         else if let json = response.result.value as? JSONDictionary
         {
            print(json)
            if let dict = json["error"] as? JSONDictionary
            {
               appDel.hideHUD()
               UtilityClass.showAlert("\(dict["message"]!)")
            }
         }
         else
         {
            appDel.hideHUD()
            UtilityClass.showAlert("Somwthing went wrong. Please try again later.")
         }
      }
   }
   
   //MARK:- creat account
   func createAccount(listing: StoreListing,view:UIView, completion:@escaping (_ arr: JSONDictionary) -> ()) {
      
      let urlString = kcreateAccount
      let json = listing.jsonArray
      
      let url = URL(string: urlString)!
      let jsonData = json?.data(using: .utf8, allowLossyConversion: false)!
      
      var request = URLRequest(url: url)
      request.httpMethod = HTTPMethod.post.rawValue
      request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
      request.httpBody = jsonData
      
      Alamofire.request(request).responseJSON { (response:DataResponse<Any>) in
         print(response.result)
         
         if let json = response.result.value as? JSONDictionary
         {
            print(json)
            if let dict = json["error"] as? JSONDictionary
            {
               if let detailsDict = dict["details"] as? JSONDictionary
               {
                  if let msgDict = detailsDict["messages"] as? JSONDictionary
                  {
                     if let arrEmail = msgDict["username"] as? Array<String>
                     {
                        appDel.hideHUD()
                        UtilityClass.showAlert("\(arrEmail[0])")
                     }
                     if let arrEmail = msgDict["email"] as? Array<String>
                     {
                        appDel.hideHUD()
                        UtilityClass.showAlert("\(arrEmail[0])")
                     }
                  }
               }
            }
            else
            {
               completion(json)
            }
         }
         else
         {
            appDel.hideHUD()
            UtilityClass.showAlert("Somwthing went wrong. Please try again later.")
         }
      }
   }
   
   //MARK:- creat social credentials
   func socialCredentials(listing: StoreListing,view:UIView, completion:@escaping (_ arr: JSONDictionary) -> ()) {
      
      let urlString = ksocialCredentials
      let json = listing.jsonArray
      
      let url = URL(string: urlString)!
      let jsonData = json?.data(using: .utf8, allowLossyConversion: false)!
      
      var request = URLRequest(url: url)
      request.httpMethod = HTTPMethod.post.rawValue
      request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
      request.httpBody = jsonData
      
      Alamofire.request(request).responseJSON { (response:DataResponse<Any>) in
         print(response.result)
         
         if let json = response.result.value as? JSONDictionary
         {
            print(json)
            completion(json)
         }
         else if let json = response.result.value as? JSONDictionary
         {
            print(json)
            if let dict = json["error"] as? JSONDictionary
            {
               appDel.hideHUD()
               UtilityClass.showAlert("\(dict["message"]!)")
            }
         }
         else
         {
            appDel.hideHUD()
            UtilityClass.showAlert("Somwthing went wrong. Please try again later.")
         }
      }
   }
   
   //MARK:- login with FB
   func loginWithFB(listing: StoreListing,view:UIView, completion:@escaping (_ arr: JSONDictionary) -> ()) {
      
      let urlString = klogin
      let json = listing.jsonArray
      
      let url = URL(string: urlString)!
      let jsonData = json?.data(using: .utf8, allowLossyConversion: false)!
      
      var request = URLRequest(url: url)
      request.httpMethod = HTTPMethod.post.rawValue
      request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
      request.httpBody = jsonData
      
      Alamofire.request(request).responseJSON { (response:DataResponse<Any>) in
         print(response.result)
         
         if let json = response.result.value as? JSONDictionary
         {
            print(json)
            if let dict = json["error"] as? JSONDictionary
            {
               appDel.hideHUD()
               UtilityClass.showAlert("\(dict["message"]!)")
            }
            else
            {
               completion(json)

            }
         }
         else
         {
            appDel.hideHUD()
            UtilityClass.showAlert("Somwthing went wrong. Please try again later.")
         }
      }
   }
   //MARK:- Delete Device
   func deleteDevices(accountID: String,accessToken:String,listing: StoreListing,view:UIView, completion:@escaping () -> ())
   {
      let urlString = "\(BASE_URL)accounts/\(accountID)/devices?access_token=\(accessToken)"
//      let json = listing.jsonArray
      
      let url = URL(string: urlString)!
//      let jsonData = json?.data(using: .utf8, allowLossyConversion: false)!
      
      var request = URLRequest(url: url)
      request.httpMethod = HTTPMethod.delete.rawValue
      request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
//      request.httpBody = jsonData
      
      Alamofire.request(request).responseString { (response) in
         if let str = response.result.value
         {
            if str == "" || str == "no content"
            {
               completion()
            }
         }
//         print(response.result.value)
      }
   }
   
   //MARK:- Add Device
   func addDevices(accountID: String,accessToken:String,listing: StoreListing,view:UIView, completion:@escaping (_ arr: JSONDictionary) -> ())
   {
      let urlString = "\(BASE_URL)accounts/\(accountID)/devices?access_token=\(accessToken)"
      let json = listing.jsonArray
      
      let url = URL(string: urlString)!
      let jsonData = json?.data(using: .utf8, allowLossyConversion: false)!
      
      var request = URLRequest(url: url)
      request.httpMethod = HTTPMethod.post.rawValue
      request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
      request.httpBody = jsonData
      
      Alamofire.request(request).responseJSON { (response:DataResponse<Any>) in
        
         print(response.result)
         
         if let json = response.result.value as? JSONDictionary
         {
            print(json)
            completion(json)
         }
         else if let json = response.result.value as? JSONDictionary
         {
            print(json)
            if let dict = json["error"] as? JSONDictionary
            {
               appDel.hideHUD()
               UtilityClass.showAlert("\(dict["message"]!)")
            }
         }
         else
         {
            appDel.hideHUD()
            UtilityClass.showAlert("Somwthing went wrong. Please try again later.")
         }
      }
   }
   
   //MARK:- Normal Login(Email and Pwd)
   func loginWithEmail(email:String,password:String,listing: StoreListing,view:UIView, completion:@escaping (_ arr: [JSONDictionary]) -> ()) {
      
      let urlString = "\(kvalidateAccount)?emailId=\(email)&password=\(password)"
      
      let url = URL(string: urlString)!
      
      var request = URLRequest(url: url)
      request.httpMethod = HTTPMethod.get.rawValue
      request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
      
      Alamofire.request(request).responseJSON { (response:DataResponse<Any>) in
         
         print(response.result)
         
         if let json = response.result.value as? [JSONDictionary]
         {
            print(json)
            completion(json)
         }
         else if let json = response.result.value as? JSONDictionary
         {
            print(json)
            if let dict = json["error"] as? JSONDictionary
            {
               appDel.hideHUD()
               UtilityClass.showAlert("\(dict["message"]!)")
            }
         }
         else
         {
            appDel.hideHUD()
            UtilityClass.showAlert("Somwthing went wrong. Please try again later.")
         }
      }
   }
   
   //MARK:- Forgot Password
   func sendEmail(type:Int,email:String,listing: StoreListing,view:UIView, completion:@escaping (_ arr: JSONDictionary) -> ()) {
      
      let urlString = "\(kforgotPassword)/{id}/sendEmail?type=\(type)&emailId=\(email)"
//      let allowedCharacterSet = (CharacterSet(charactersIn: "!*'();:@&=+$,/?%#[] {}").inverted)
      
      var escapedString = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
      print(escapedString!)
      let url = URL(string: escapedString!)!
      
      var request = URLRequest(url: url)
      request.httpMethod = HTTPMethod.get.rawValue
      request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
      
      Alamofire.request(request).responseJSON { (response:DataResponse<Any>) in
         
         print(response.result)
         
         if let json = response.result.value as? JSONDictionary
         {
            print(json)
            completion(json)
         }
         else if let json = response.result.value as? JSONDictionary
         {
            print(json)
            if let dict = json["error"] as? JSONDictionary
            {
               appDel.hideHUD()
               UtilityClass.showAlert("\(dict["message"]!)")
            }
         }
         else
         {
            appDel.hideHUD()
            UtilityClass.showAlert("Somwthing went wrong. Please try again later.")
         }
      }
   }
   
   //MARK:- Store listing
   func getStoreListing(longitude:NSNumber,latitude:NSNumber,mindistance:Double,maxdistance:Double,limit:NSNumber,transit : Int,listing: StoreListing,view:UIView, completion:@escaping (_ arr: [JSONDictionary]) -> ()) {
      
      let urlString = "\(kStoreListing)?longitude=\(longitude)&latitude=\(latitude)&mindistance=\(mindistance)&maxdistance=\(maxdistance)&transit=\(transit)&limit=\(limit)"
      
      let url = URL(string: urlString)!
      
      var request = URLRequest(url: url)
      request.httpMethod = HTTPMethod.get.rawValue
      request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
      
      Alamofire.request(request).responseJSON { (response:DataResponse<Any>) in
         print(response.result)
         
         if let json = response.result.value as? [JSONDictionary]
         {
            print(json)
            completion(json)
         }
         else if let json = response.result.value as? JSONDictionary
         {
            print(json)
            if let dict = json["error"] as? JSONDictionary
            {
               appDel.hideHUD()
               UtilityClass.showAlert("\(dict["message"]!)")
            }
         }
         else
         {
            appDel.hideHUD()
            UtilityClass.showAlert("Somwthing went wrong. Please try again later.")
         }
      }
   }
   //MARK:- get location
   func getLocation(listing: StoreListing,view:UIView, completion:@escaping (_ arr: [JSONDictionary]) -> ()) {
      
      Alamofire.request(kpickuplocations, method: .get, parameters: ["filter":listing.jsonArray]).responseJSON { (response:DataResponse<Any>) in
         
         print(response.result)
         
         if let json = response.result.value as? [JSONDictionary]
         {
            print(json)
            completion(json)
         }
         else if let json = response.result.value as? JSONDictionary
         {
            print(json)
            if let dict = json["error"] as? JSONDictionary
            {
               appDel.hideHUD()
               UtilityClass.showAlert("\(dict["message"]!)")
            }
         }
         else
         {
            appDel.hideHUD()
            UtilityClass.showAlert("Somwthing went wrong. Please try again later.")
         }
      }
   }
   //MARK:- location
   func Location(lat:NSNumber,long:NSNumber, completion:@escaping (_ arr: JSONDictionary) -> ()) {
      
      let urlString = kLocation
      var dictMain = Dictionary<String,AnyObject>()
      var dictGeo = Dictionary<String,AnyObject>()
      dictGeo["type"] = "Point" as AnyObject
      let arrLatLong = [long,lat]
      dictGeo["coordinates"] = arrLatLong as AnyObject
      
      dictMain["geoLocation"] = dictGeo as AnyObject
      let url = URL(string: urlString)!

      var request = URLRequest(url: url)

      if let theJSONData = try?  JSONSerialization.data(withJSONObject: dictMain,options: .prettyPrinted),
         let theJSONText = String(data: theJSONData,encoding: String.Encoding.ascii)
      {
         print("JSON string = \n\(theJSONText)")
         request.httpBody = theJSONText.data(using: String.Encoding.ascii)
      }
//      let escapedString = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
//      print(escapedString!)
      request.httpMethod = HTTPMethod.post.rawValue
      request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
      Alamofire.request(request).responseJSON { (response:DataResponse<Any>) in
         print(response.result)
         
         if let json = response.result.value as? JSONDictionary
         {
            print(json)
            completion(json)
         }
         else if let json = response.result.value as? JSONDictionary
         {
            print(json)
            if let dict = json["error"] as? JSONDictionary
            {
               appDel.hideHUD()
               UtilityClass.showAlert("\(dict["message"]!)")
            }
         }
         else
         {
            appDel.hideHUD()
            UtilityClass.showAlert("Somwthing went wrong. Please try again later.")
         }
      }
   }
   //MARK:- pick up location
   func pickupLocation(id:String,address:String,lat:NSNumber,long:NSNumber, completion:@escaping (_ arr: JSONDictionary) -> ()) {
      
      let urlString = kpickuplocations
      var dictMain = Dictionary<String,AnyObject>()
      var dictGeo = Dictionary<String,AnyObject>()
      dictGeo["type"] = "Point" as AnyObject
      let arrLatLong = [long,lat]
      dictGeo["coordinates"] = arrLatLong as AnyObject
      
      dictMain["geoLocation"] = dictGeo as AnyObject
      dictMain["address"] = address as AnyObject
      dictMain["locationId"] = id as AnyObject

      dictMain["accountId"] = "\(UserDefaults.standard.value(forKey: "accountId")!)" as AnyObject

      let url = URL(string: urlString)!
      
      var request = URLRequest(url: url)
      
      if let theJSONData = try?  JSONSerialization.data(withJSONObject: dictMain,options: .prettyPrinted),
         let theJSONText = String(data: theJSONData,encoding: String.Encoding.ascii)
      {
         print("JSON string = \n\(theJSONText)")
         request.httpBody = theJSONText.data(using: String.Encoding.ascii)
      }
      //      let escapedString = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
      //      print(escapedString!)
      request.httpMethod = HTTPMethod.post.rawValue
      request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
      Alamofire.request(request).responseJSON { (response:DataResponse<Any>) in
         print(response.result)
         
         if let json = response.result.value as? JSONDictionary
         {
            print(json)
            completion(json)
         }
         else if let json = response.result.value as? JSONDictionary
         {
            print(json)
            if let dict = json["error"] as? JSONDictionary
            {
               appDel.hideHUD()
               UtilityClass.showAlert("\(dict["message"]!)")
            }
         }
         else
         {
            appDel.hideHUD()
            UtilityClass.showAlert("Somwthing went wrong. Please try again later.")
         }
      }
   }
   //MARK:- Product listing
   func getProductListing(listing: StoreDetails,view:UIView, completion:@escaping (_ arr: [JSONDictionary]) -> ())
   {
      Alamofire.request(kProductListing, method: .get, parameters: ["filter":listing.jsonArray]).responseJSON { (response:DataResponse<Any>) in
         
         print(response.result)
         
         if let json = response.result.value as? [JSONDictionary]
         {
            print(json)
            completion(json)
         }
         else if let json = response.result.value as? JSONDictionary
         {
            print(json)
            if let dict = json["error"] as? JSONDictionary
            {
               appDel.hideHUD()
               UtilityClass.showAlert("\(dict["message"]!)")
            }
         }
         else
         {
            appDel.hideHUD()
            UtilityClass.showAlert("Somwthing went wrong. Please try again later.")
         }
      }
   }
   //MARK:- retrive Cart
   func retriveCart(listing: StoreDetails,view:UIView, completion:@escaping (_ arr: [JSONDictionary]) -> ())
   {
      Alamofire.request(kretriveCart, method: .get, parameters: ["filter":listing.jsonArray]).responseJSON { (response:DataResponse<Any>) in
         
         print(response.result)
         
         if let json = response.result.value as? [JSONDictionary]
         {
            print(json)
            completion(json)
         }
         else if let json = response.result.value as? JSONDictionary
         {
            print(json)
            if let dict = json["error"] as? JSONDictionary
            {
               appDel.hideHUD()
               UtilityClass.showAlert("\(dict["message"]!)")
            }
         }
         else
         {
            appDel.hideHUD()
            UtilityClass.showAlert("Somwthing went wrong. Please try again later.")
         }
      }
   }
   //MARK:- Add to cart
   func addToCart(accountId:String,productId:String,offerId:String,quantity:Int,listing: StoreListing,view:UIView, completion:@escaping (_ arr: JSONDictionary) -> ()) {
      
      let urlString = "\(kaddToCart)?accountId=\(accountId)&quantity=\(quantity)&productId=\(productId)&offerId=\(offerId)"
      
      let url = URL(string: urlString)!
      
      var request = URLRequest(url: url)
      request.httpMethod = HTTPMethod.get.rawValue
      request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
      
      Alamofire.request(request).responseJSON { (response:DataResponse<Any>) in
         print(response.result)
         
         if let json = response.result.value as? JSONDictionary
         {
            print(json)
            completion(json)
         }
         else if let json = response.result.value as? JSONDictionary
         {
            print(json)
            if let dict = json["error"] as? JSONDictionary
            {
               appDel.hideHUD()
               UtilityClass.showAlert("\(dict["message"]!)")
            }
         }
         else
         {
            appDel.hideHUD()
            UtilityClass.showAlert("Somwthing went wrong. Please try again later.")
         }
      }
   }
   
   //MARK:- Update Cart
   func updateCart(id:String,listing: StoreDetails,view:UIView, completion:@escaping (_ arr: JSONDictionary) -> ())
   {
      
      let urlString = "\(kretriveCart)/\(id)"
      let json = listing.jsonArray
      
      let url = URL(string: urlString)!
      let jsonData = json?.data(using: .utf8, allowLossyConversion: false)!
      
      var request = URLRequest(url: url)
      request.httpMethod = HTTPMethod.put.rawValue
      request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
      request.httpBody = jsonData
      
      Alamofire.request(request).responseJSON { (response:DataResponse<Any>) in
         
         print(response.result)
         
         if let json = response.result.value as? JSONDictionary
         {
            print(json)
            if let dict = json["error"] as? JSONDictionary
            {
               appDel.hideHUD()
               UtilityClass.showAlert("\(dict["message"]!)")
            }
            else
            {
               completion(json)
            }
         }
         else
         {
            appDel.hideHUD()
            UtilityClass.showAlert("Somwthing went wrong. Please try again later.")
         }
      }
   }
   
   //MARK:- Delete item from cart
   func deleteCart(id:String,view:UIView, completion:@escaping (_ arr: JSONDictionary) -> ())
   {
      let urlString = "\(kretriveCart)/\(id)"
      //      let json = listing.jsonArray
      
      let url = URL(string: urlString)!
      //      let jsonData = json?.data(using: .utf8, allowLossyConversion: false)!
      
      var request = URLRequest(url: url)
      request.httpMethod = HTTPMethod.delete.rawValue
      request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
      //      request.httpBody = jsonData
      
      Alamofire.request(request).responseJSON { (response:DataResponse<Any>) in
         print(response.result)
         
         if let json = response.result.value as? JSONDictionary
         {
            print(json)
            if let dict = json["error"] as? JSONDictionary
            {
               appDel.hideHUD()
               UtilityClass.showAlert("\(dict["message"]!)")
            }
            else
            {
               completion(json)
            }
         }
         else
         {
            appDel.hideHUD()
            UtilityClass.showAlert("Somwthing went wrong. Please try again later.")
         }
      }
         //         print(response.result.value)
   }
   
   //MARK:- Empty cart
   func emptyCart(accountId : String,listing: StoreListing,view:UIView, completion:@escaping (_ arr: JSONDictionary) -> ())
   {
      var urlString = ""
      let dict = ["accountId":"\(accountId)","state":0] as [String : Any]
      if let theJSONData = try?  JSONSerialization.data(withJSONObject: dict,options: .prettyPrinted),
         let theJSONText = String(data: theJSONData,encoding: String.Encoding.ascii)
      {
         print("JSON string = \n\(theJSONText)")
         urlString = "\(kemptyCart)?where=\(theJSONText)"
      }
      let escapedString = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
      print(escapedString!)
      let url = URL(string: escapedString!)!
      var request = URLRequest(url: url)
      request.httpMethod = HTTPMethod.post.rawValue
      request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
      request.httpBody = listing.jsonArray.data(using: String.Encoding.ascii)
      Alamofire.request(request).responseJSON { (response:DataResponse<Any>) in
         print(response.result)
         
         if let json = response.result.value as? JSONDictionary
         {
            print(json)
            completion(json)
         }
         else if let json = response.result.value as? JSONDictionary
         {
            print(json)
            if let dict = json["error"] as? JSONDictionary
            {
               appDel.hideHUD()
               UtilityClass.showAlert("\(dict["message"]!)")
            }
         }
         else
         {
            appDel.hideHUD()
            UtilityClass.showAlert("Somwthing went wrong. Please try again later.")
         }
      }
   }
   
   //MARK:- send cart to server
   func sendCartToServer(listing: StoreListing,view:UIView, completion:@escaping (_ arr: [JSONDictionary]) -> ()) {
      
      let urlString = "\(ksendCart)?items=\(listing.jsonArray!)"
      let escapedString = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
      print(escapedString!)
      let url = URL(string: escapedString!)!
      
      var request = URLRequest(url: url)
      request.httpMethod = HTTPMethod.get.rawValue
      request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
//      request.httpBody = jsonData
      
      Alamofire.request(request).responseJSON { (response:DataResponse<Any>) in
         print(response.result)
         
         if let json = response.result.value as? [JSONDictionary]
         {
            print(json)
            completion(json)
         }
         else if let json = response.result.value as? JSONDictionary
         {
            print(json)
            if let dict = json["error"] as? JSONDictionary
            {
               appDel.hideHUD()
               UtilityClass.showAlert("\(dict["message"]!)")
            }
         }
         else
         {
            appDel.hideHUD()
            UtilityClass.showAlert("Somwthing went wrong. Please try again later.")
         }
      }
   }
   //MARK:- get userData afterlogin
   func getUserDataAfterlogin(accountId : String,listing: StoreListing,view:UIView, completion:@escaping (_ arr: JSONDictionary) -> ())
   {
      var urlString : String = ""
      let arr = ["paymentIds"] as [String]
      let includeDict = ["include":arr] as JSONDictionary
      
      if let theJSONData = try?  JSONSerialization.data(
         withJSONObject: includeDict,
         options: .prettyPrinted
         ),
         let theJSONText = String(data: theJSONData,
                                  encoding: String.Encoding.ascii)
      {
         print("JSON string = \n\(theJSONText)")
         urlString = "\(kcreateAccount)/\(accountId)?filter=\(theJSONText)&access_token=\(UserDefaults.standard.value(forKey: "access_token")!)"
      }
      let escapedString = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
      print(escapedString!)
      let url = URL(string: escapedString!)!
      var request = URLRequest(url: url)
      request.httpMethod = HTTPMethod.get.rawValue
      request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
//      request.httpBody = listing.jsonArray.data(using: String.Encoding.ascii)
      Alamofire.request(request).responseJSON { (response:DataResponse<Any>) in
         print(response.result)
         
         if let json = response.result.value as? JSONDictionary
         {
            print(json)
            completion(json)
         }
         else if let json = response.result.value as? JSONDictionary
         {
            print(json)
            if let dict = json["error"] as? JSONDictionary
            {
               appDel.hideHUD()
               UtilityClass.showAlert("\(dict["message"]!)")
            }
         }
         else
         {
            appDel.hideHUD()
            UtilityClass.showAlert("Somwthing went wrong. Please try again later.")
         }
      }
   }
   //MARK:- send phone number
   func sendPhoneNumber(accountId : String,listing: StoreListing,view:UIView, completion:@escaping (_ arr: JSONDictionary) -> ())
   {
      let urlString = "\(kcreateAccount)/\(accountId)?access_token=\(UserDefaults.standard.value(forKey: "access_token")!)"
      let escapedString = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
      print(escapedString!)
      let url = URL(string: escapedString!)!
      var request = URLRequest(url: url)
      request.httpMethod = HTTPMethod.put.rawValue
      request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
      request.httpBody = listing.jsonArray.data(using: String.Encoding.ascii)
      Alamofire.request(request).responseJSON { (response:DataResponse<Any>) in
         print(response.result)
         
         if let json = response.result.value as? JSONDictionary
         {
            print(json)
            completion(json)
         }
         else if let json = response.result.value as? JSONDictionary
         {
            print(json)
            if let dict = json["error"] as? JSONDictionary
            {
               appDel.hideHUD()
               UtilityClass.showAlert("\(dict["message"]!)")
            }
         }
         else
         {
            appDel.hideHUD()
            UtilityClass.showAlert("Somwthing went wrong. Please try again later.")
         }
      }
   }
//   http://18.221.89.220:3000/api/bevviutils/getEstDistance?latitude=40.772370&longitude=-74.386692&establishmentId=5a24a28610785a5c1aa5333c&transit=1
   
   //MARK:- get distance
   func getEstDistance(latitude : String,longitude : String,establishmentId : String, completion:@escaping (_ arr: [JSONDictionary]) -> ())
   {
      let urlString = "\(kgetEstDistance)?latitude=\(latitude)&longitude=\(longitude)&establishmentId=\(establishmentId)&transit=\(UserDefaults.standard.value(forKey: "walk")!)"
      let escapedString = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
      print(escapedString!)
      let url = URL(string: escapedString!)!
      var request = URLRequest(url: url)
      request.httpMethod = HTTPMethod.get.rawValue
      request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
      Alamofire.request(request).responseJSON { (response:DataResponse<Any>) in
         print(response.result)
         
         if let json = response.result.value as? [JSONDictionary]
         {
            print(json)
            completion(json)
         }
         else if let json = response.result.value as? JSONDictionary
         {
            print(json)
            if let dict = json["error"] as? JSONDictionary
            {
               appDel.hideHUD()
               UtilityClass.showAlert("\(dict["message"]!)")
            }
         }
         else
         {
            appDel.hideHUD()
            UtilityClass.showAlert("Somwthing went wrong. Please try again later.")
         }
      }
   }
   //MARK:- pending pickup/past orders
   func orders(listing: StoreListing,view:UIView, completion:@escaping (_ arr: [JSONDictionary]) -> ()) {
      
      Alamofire.request(korders, method: .get, parameters: ["filter":listing.jsonArray]).responseJSON { (response:DataResponse<Any>) in
         
         print(response.result)
         
         if let json = response.result.value as? [JSONDictionary]
         {
            print(json)
            completion(json)
         }
         else if let json = response.result.value as? JSONDictionary
         {
            print(json)
            if let dict = json["error"] as? JSONDictionary
            {
               appDel.hideHUD()
               UtilityClass.showAlert("\(dict["message"]!)")
            }
         }
         else
         {
            appDel.hideHUD()
            UtilityClass.showAlert("Somwthing went wrong. Please try again later.")
         }
      }
   }
   //MARK:- post rating
   func postRating(listing:StoreListing, completion:@escaping (_ arr: JSONDictionary) -> ()) {
      
      let urlString = krating
      let url = URL(string: urlString)!
      
      var request = URLRequest(url: url)
     
      //      let escapedString = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
      //      print(escapedString!)
      request.httpMethod = HTTPMethod.post.rawValue
      request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
      request.httpBody = listing.jsonArray.data(using: String.Encoding.ascii)

      Alamofire.request(request).responseJSON { (response:DataResponse<Any>) in
         print(response.result)
         
         if let json = response.result.value as? JSONDictionary
         {
            print(json)
            completion(json)
         }
         else if let json = response.result.value as? JSONDictionary
         {
            print(json)
            if let dict = json["error"] as? JSONDictionary
            {
               appDel.hideHUD()
               UtilityClass.showAlert("\(dict["message"]!)")
            }
         }
         else
         {
            appDel.hideHUD()
            UtilityClass.showAlert("Somwthing went wrong. Please try again later.")
         }
      }
   }
   //MARK:- get settings
   func getSetting(listing: StoreListing,completion:@escaping (_ arr: [JSONDictionary]) -> ())
   {
      Alamofire.request(ksettings, method: .get, parameters: ["filter":listing.jsonArray]).responseJSON { (response:DataResponse<Any>) in
         
         print(response.request)
         
         if let json = response.result.value as? [JSONDictionary]
         {
            print(json)
            
            completion(json)
         }
         else if let json = response.result.value as? JSONDictionary
         {
            print(json)
            if let dict = json["error"] as? JSONDictionary
            {
               appDel.hideHUD()
               UtilityClass.showAlert("\(dict["message"]!)")
            }
         }
         else
         {
            appDel.hideHUD()
            UtilityClass.showAlert("Somwthing went wrong. Please try again later.")
         }
      }
   }
   //MARK:- post settings
   func postSetting(listing: StoreListing,completion:@escaping (_ arr: JSONDictionary) -> ())
   {
      let urlString = ksettings
      let json = listing.jsonArray
      let escapedString = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
      print(escapedString!)
      let url = URL(string: escapedString!)!
      let jsonData = json?.data(using: .utf8, allowLossyConversion: false)!
      
      var request = URLRequest(url: url)
      request.httpMethod = HTTPMethod.post.rawValue
      request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
      request.httpBody = jsonData
      
      Alamofire.request(request).responseJSON { (response:DataResponse<Any>) in
         print(response.result)
         
         if let json = response.result.value as? JSONDictionary
         {
            print(json)
            completion(json)
         }
         else if let json = response.result.value as? JSONDictionary
         {
            print(json)
            if let dict = json["error"] as? JSONDictionary
            {
               appDel.hideHUD()
               UtilityClass.showAlert("\(dict["message"]!)")
            }
         }
         else
         {
            appDel.hideHUD()
            UtilityClass.showAlert("Somwthing went wrong. Please try again later.")
         }
      }
   }
   //MARK:- logout
   func logout(accessToken:String,completion:@escaping () -> ())
   {
      let urlString = "\(klogout)?access_token=\(accessToken)"
//      let json = listing.jsonArray
      let escapedString = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
      print(escapedString!)
      let url = URL(string: escapedString!)!
//      let jsonData = json?.data(using: .utf8, allowLossyConversion: false)!
      
      var request = URLRequest(url: url)
      request.httpMethod = HTTPMethod.post.rawValue
      request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
//      request.httpBody = jsonData
      
      Alamofire.request(request).responseString
      { (response) in
         
         completion()
      }
   }
   
}

import UIKit
class GlobalFunction: NSObject
{
    
    static var instance: GlobalFunction!
    class func sharedInstance() -> GlobalFunction
    {
        self.instance = (self.instance ?? GlobalFunction())
        return self.instance
    }
    class func convertStringToDictionary(text: String) -> [String:AnyObject]? {
        if let data = text.data(using: String.Encoding.utf8) {
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject]
                return json
            } catch {
                UIAlertController.showAlert(withTitle: APP_NAME, alertMessage:"Something went wrong", buttonArray: ["OK"], completion: { (index:Int) in
                    
                })
            }
        }
        return nil
    }
}

extension UIAlertController {
    
    class func showAlert(withTitle alertTitle: String, alertMessage: String, buttonArray: [String], completion: @escaping(_ buttonIndex : Int) -> Void)
    {
        
        let alertController = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .alert)
        
        for i in 0..<buttonArray.count {
            let alertButton = UIAlertAction(title: (buttonArray[i]), style: .default, handler: { UIAlertAction in
                
                completion(i)
                
                alertController.dismiss(animated: true, completion: {
                    
                })
            })
            
            alertController.addAction(alertButton)
        }
        
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            
            // present the view controller
            topController.present(alertController, animated: true, completion: nil)
            
            
        }
    }
    
}
