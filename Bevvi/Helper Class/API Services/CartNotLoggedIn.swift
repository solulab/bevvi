//
//  CartNotLoggedIn.swift
//  Bevvi
//
//  Created by Hetal Govani on 01/01/18.
//  Copyright © 2018 solulab. All rights reserved.
//

import UIKit
import CoreData

class CartNotLogIn {
   
   
   let managedContext = appDel.managedObjectContext

   public class var sharedInstance: CartNotLogIn
   {
      struct Singleton
      {
         static let instance: CartNotLogIn = CartNotLogIn()
      }
      return Singleton.instance
   }
   
   init() {}
   
   func getAllCartProducts() -> [cartDetail] {
      
      var cartObj = [cartDetail]()
      
      // UIApplication.shared.isNetworkActivityIndicatorVisible = true
      
      let fetchRequest =
         NSFetchRequest<NSManagedObject>(entityName: "CartNotLoggedIn")
      
      //3
      do {
         let products = try managedContext.fetch(fetchRequest)
         
         if products.count > 0 {
            for object in products
            {
               let cart = cartDetail()
               
               if let dict = object.value(forKey: "cart") as? JSONDictionary {
                  cart.toPopulateWithJSON(dict: dict)
                  cart.isFromServer = "1"
                  cartObj.append(cart)
               }
            }
            try managedContext.save()
            // UIApplication.shared.isNetworkActivityIndicatorVisible = false
            
         }
      } catch let error as NSError {
         print("Could not fetch. \(error), \(error.userInfo)")
      }
      
      return cartObj
   }
   func getAllCartProductsNotLoggedIn(completion: @escaping (_ cart: [cartDetail]) -> ())
   {
      var cartObj = [cartDetail]()
      
      // UIApplication.shared.isNetworkActivityIndicatorVisible = true
      let appDelegate =
         UIApplication.shared.delegate as! AppDelegate
      // 1
      let managedContext =
         appDelegate.managedObjectContext
      
      //2
      let fetchRequest =
         NSFetchRequest<NSManagedObject>(entityName: "CartNotLoggedIn")
      
      //3
      do {
         let products = try managedContext.fetch(fetchRequest)
         
         if products.count > 0 {
            for object in products {
               
               let cart = cartDetail()
               
               if let dict = object.value(forKey: "cart") as? JSONDictionary {
                  cart.toPopulateWithJSON(dict: dict)
                  cart.isFromServer = "0"
                  cartObj.append(cart)
               }
            }
            try managedContext.save()
            completion(cartObj)
            // UIApplication.shared.isNetworkActivityIndicatorVisible = false
            
         }
         else
         {
            completion(cartObj)
         }
      } catch let error as NSError {
         print("Could not fetch. \(error), \(error.userInfo)")
      }
   }
   
   func saveProduct(forEntityName:String, object: Any)
   {
      guard let appDelegate =
         UIApplication.shared.delegate as? AppDelegate else
      {
            return
      }
      
      // 1
      let managedContext = appDelegate.managedObjectContext
      
      // 2
      let entity = NSEntityDescription.entity(forEntityName: forEntityName, in: managedContext)!
      
      let person = NSManagedObject(entity: entity,
                                   insertInto: managedContext)
      
      // 3
      person.setValue(object, forKeyPath: "cart")
      
      // 4
      do {
         try managedContext.save()
      } catch let error as NSError {
         print("Could not save. \(error), \(error.userInfo)")
      }
   }
   
   func sendProduct() {
      
      // UIApplication.shared.isNetworkActivityIndicatorVisible = true
      guard let appDelegate =
         UIApplication.shared.delegate as? AppDelegate else {
            return
      }
      // 1
      let managedContext =
         appDelegate.managedObjectContext
      
      //2
      let fetchRequest =
         NSFetchRequest<NSManagedObject>(entityName: "CartNotLoggedIn")
      
      //3
      do {
         let games = try managedContext.fetch(fetchRequest)
         if games.count > 0 {
            for object in games{
               if let dict = object.value(forKey: "cart") as? JSONDictionary
               {
                  print(dict)
               }
            }
            // UIApplication.shared.isNetworkActivityIndicatorVisible = false
            
         }
      } catch let error as NSError {
         print("Could not fetch. \(error), \(error.userInfo)")
      }
   }
   
   func updateGameDataObjects(quantity: Int, object: JSONDictionary)
   {
      let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "CartNotLoggedIn")
      //3
      do
      {
         let games = try managedContext.fetch(fetchRequest)
         
         if games.count > 0
         {
            if let tempProduct = object as? JSONDictionary
            {
               let product = cartDetail()
               product.toPopulateWithJSON(dict: tempProduct)
               print(product.toJsonDict())
               for tempObject in games
               {
                  if var dictObj = tempObject.value(forKey: "cart") as? JSONDictionary
                  {
                     if var tempKeeperObj = dictObj as? JSONDictionary
                     {
                        let producttemp = cartDetail()
                        producttemp.toPopulateWithJSON(dict: tempKeeperObj)
                        if  product.uniqueId != nil
                        {
                           if producttemp.uniqueId == product.uniqueId
                           {
                              producttemp.quantity = quantity
                              producttemp.isFromServer = "1"
                              tempKeeperObj = producttemp.toJsonDict()
                              print(dictObj["quantity"]!)
                              dictObj["quantity"] = Int("\(tempKeeperObj["quantity"]!)") as AnyObject
                              print(dictObj["quantity"]!)
                              managedContext.delete(tempObject)
                              try managedContext.save()
                              self.saveProduct(forEntityName: "CartNotLoggedIn", object: dictObj)
                           }
                        }
                     }
                  }
               }
            }
         }
      }
      catch {}
      
   }
   func deleteAllRecords(completion: @escaping () -> ()) {
    
      // UIApplication.shared.isNetworkActivityIndicatorVisible = true
      let appDelegate =
         UIApplication.shared.delegate as! AppDelegate
      // 1
      let managedContext = appDelegate.managedObjectContext
      
      //2
      let fetchRequest =
         NSFetchRequest<NSManagedObject>(entityName: "CartNotLoggedIn")
      
      //3
      do {
         
         let cart = try managedContext.fetch(fetchRequest)
         
         if cart.count > 0
         {
            for product in cart
            {
               managedContext.delete(product)
            }
            try managedContext.save()
            completion()
         }
      } catch let error as NSError {
         print("Could not fetch. \(error), \(error.userInfo)")
      }
   }
   func deleteProduct(product: cartDetail,completion: @escaping () -> ())
   {
      product.toPopulateWithJSON(dict: product.toJsonDict())
      print("\(product.toJsonDict())")
      
      // UIApplication.shared.isNetworkActivityIndicatorVisible = true
      let appDelegate =
         UIApplication.shared.delegate as! AppDelegate
      // 1
      let managedContext = appDelegate.managedObjectContext
      
      //2
      let fetchRequest =
         NSFetchRequest<NSManagedObject>(entityName: "CartNotLoggedIn")
      
      //3
      do {
         let cart = try managedContext.fetch(fetchRequest)
         
         if cart.count > 0
         {
            for object in cart
            {
               let dictObj = object.value(forKey: "cart") as? JSONDictionary
               let producttemp = cartDetail()
               producttemp.toPopulateWithJSON(dict: dictObj!)
               if producttemp.productId == product.productId
               {
                  managedContext.delete(object)
               }
            }
            try managedContext.save()
            completion()
         }
      } catch let error as NSError {
         print("Could not fetch. \(error), \(error.userInfo)")
      }
      
   }
   func getLastObjectUniqueID() -> String?
   {
      
      // UIApplication.shared.isNetworkActivityIndicatorVisible = true
      
      if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
         
         let managedContext =
            appDel.managedObjectContext
         //2
         let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: "CartNotLoggedIn")
         
         //3
         do {
            let games = try managedContext.fetch(fetchRequest)
            if games.count > 0 {
               let object = games.last!
               if let dict = object.value(forKey: "cart") as? JSONDictionary {
                  // UIApplication.shared.isNetworkActivityIndicatorVisible = false
                  
                  return "\(Int(dict["uniqueId"]! as! String)!+1)"
               }
            } else {
               // UIApplication.shared.isNetworkActivityIndicatorVisible = false
               
               return "1"
            }
            // UIApplication.shared.isNetworkActivityIndicatorVisible = false
            
            return ""
         } catch let error as NSError
         {
            // UIApplication.shared.isNetworkActivityIndicatorVisible = false
            
            print("Could not fetch. \(error), \(error.userInfo)")
            return ""
         }
      } else {
         // UIApplication.shared.isNetworkActivityIndicatorVisible = false
         
         return ""
      }
   }
}
