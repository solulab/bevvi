//
//  PaymentViewController.swift
//  Bevvi
//
//  Created by Hetal Govani on 09/11/17.
//  Copyright © 2017 solulab. All rights reserved.
//

import UIKit
import Stripe

@objc protocol STSelectedCardDelegate {
   func setCard()
}

class PaymentViewController: UIViewController,UITableViewDelegate,UITableViewDataSource, ReloadCardDelegate {
   
   @IBOutlet var tblObj : UITableView!
   @IBOutlet var tblHeight : NSLayoutConstraint!
   @IBOutlet var viewHeight : NSLayoutConstraint!
//   var cards = [STCard]()
   var delegate: STSelectedCardDelegate?
   
   var isFromSetting: Bool = false
   
   override func viewDidLoad() {
      super.viewDidLoad()
      self.title = "Payment"
      viewHeight.constant = self.view.frame.height - 114
      
      let button1 = UIButton(type: UIButtonType.custom)
      button1.setImage(#imageLiteral(resourceName: "Back"), for: UIControlState.normal)
      button1.addTarget(self, action:#selector(btnBackPress(sender:)), for: UIControlEvents.touchUpInside)
      button1.frame=CGRect.init(x: 0, y: 0, width: 30, height: 30)
      let barButton1 = UIBarButtonItem(customView: button1)
      self.navigationItem.leftBarButtonItem = barButton1
      
      
      self.reloadTableData()
      //        self.tblObj.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
      
      //
      tblObj.tableFooterView = UIView()
      //        tblObj.layer.borderColor = UIColor.gray.cgColor
      //        tblObj.layer.borderWidth = 0.5
      self.reloadCards()
   }
   
   func reloadTableData() {
      DispatchQueue.main.async {
         self.tblObj.reloadData()
         self.tblObj.layoutIfNeeded()
         self.tblHeight.constant = self.tblObj.contentSize.height
      }
      
   }
   
   func reloadCards() {
      appDel.showHUD()
      
      STPCustomerClass.shared.getCustomer { (error) in
      
         self.reloadTableData()
         appDel.hideHUD()
      }
      
//      STPCardApiManager.shared.getAllCardDetails(parameter: ["customerId": MyAPIClient.sharedClient.customerID as AnyObject]) { (response: CustomerData?, error:Error?) in
//         if error == nil {
//            if let customerDataObjArr = response?.sources?.data {
//               if customerDataObjArr.count > 0 {
//                  self.cards = customerDataObjArr
//               }
//            }
//         }
//         self.reloadTableData()
//         appDel.hideHUD()
//      }
   }
   
   override func viewWillAppear(_ animated: Bool) {
      super.viewWillAppear(true)
      
   }
   
   override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
      tblObj.layer.removeAllAnimations()
      tblHeight.constant = tblObj.contentSize.height
      UIView.animate(withDuration: 0.5) {
         self.tblObj.layoutIfNeeded()
         self.updateViewConstraints()
         
      }
   }
   
   //    override func updateViewConstraints() {
   //        super.updateViewConstraints()
   //        self.viewHeight.constant = self.tblObj.frame.origin.y + self.tblHeight.constant
   //    }
   
   @IBAction func btnBackPress(sender:UIButton)
   {
      self.navigationController?.popViewController(animated: true)
   }
   
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      if STPCustomerClass.shared.cardSources.count == 0 {
         return 1
      } else {
         return STPCustomerClass.shared.cardSources.count + 1
      }
      
   }
   func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
      return 60
   }
   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      
      if STPCustomerClass.shared.cardSources.count == 0 || indexPath.row == STPCustomerClass.shared.cardSources.count  {
         let cell = tableView.dequeueReusableCell(withIdentifier: "cellCard", for: indexPath) as! AddCardCell
         cell.selectionStyle = .none
         return cell
      } else {
         
         let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! paymentCell
         cell.selectionStyle = .none
         
         let cardObj = STPCustomerClass.shared.cardSources[indexPath.row].cardDetails!
         
//         let card = self.cards[indexPath.row]
//         if let digit = card.last4 {
            cell.lblCard.text = cardObj.last4!
//         }

         cell.imgView.image = STPImageLibrary.brandImage(for: cardObj.brand)
         
//         if let brand = card.brand {
//            cell.imgView.image = UIImage.init(named: "stp_card_\(brand.lowercased())")
//         }else {
//            cell.imgView.image = #imageLiteral(resourceName: "stp_card_unknown")
//         }
//
         if indexPath.row == 0 {
            cell.lblDefault.isHidden = false
         } else {
            cell.lblDefault.isHidden = true
         }
         
         return cell
      }
   }
   
   func tableView(_ tableView: UITableView, editActionsForRowAt: IndexPath) -> [UITableViewRowAction]?
   {
      if editActionsForRowAt.row != STPCustomerClass.shared.cardSources.count
      {
         let Delete = UITableViewRowAction(style: .normal, title: "Delete") { action, index in
            tableView.setEditing(false, animated: true)
            appDel.hideHUD()
            appDel.customerContext.detachSource(fromCustomer: STPCustomerClass.shared.cardSources[index.row], completion: { (error) in
               if error == nil {
                  STPCustomerClass.shared.getCustomer(completion: { (error) in
                     if error == nil {
                        self.reloadTableData()
                     }
                     appDel.hideHUD()
                  })
               } else {
                  appDel.hideHUD()
               }
            })

            //            var parameter = JSONDictionary()
//            parameter["customerId"] = MyAPIClient.sharedClient.customerID as AnyObject
//            parameter["cardId"] = self.cards[index.row].id! as AnyObject
//            appDel.showHUD()
//            print(parameter)
//            STPCardApiManager.shared.deleteCard(parameter: parameter, completion: { (response, error) in
//               if error == nil {
//                  self.cards.remove(at: index.row)
//
//               }
//               self.reloadTableData()
//               appDel.hideHUD()
//            })
            print("delete button tapped")
         }
         Delete.backgroundColor = redColor
         
         let defaultObj = UITableViewRowAction(style: .normal, title: "Default") { action, index in
            tableView.setEditing(false, animated: true)
            appDel.showHUD()
            appDel.customerContext.selectDefaultCustomerSource(STPCustomerClass.shared.cardSources[index.row], completion: { (error) in
               
               if error == nil {
                  STPCustomerClass.shared.getCustomer(completion: { (error) in
                     if error == nil {
                        self.reloadTableData()
                        appDel.hideHUD()
                     } else {
                        appDel.hideHUD()
                     }
                  })
               } else {
                  appDel.hideHUD()
               }
            })
            
            //            var parameter = JSONDictionary()
            //            parameter["customerId"] = MyAPIClient.sharedClient.customerID as AnyObject
            //            parameter["cardId"] = self.cards[index.row].id! as AnyObject
            //            appDel.showHUD()
            //            print(parameter)
            //            STPCardApiManager.shared.deleteCard(parameter: parameter, completion: { (response, error) in
            //               if error == nil {
            //                  self.cards.remove(at: index.row)
            //
            //               }
            //               self.reloadTableData()
            //               appDel.hideHUD()
            //            })
            print("delete button tapped")
         }
         defaultObj.backgroundColor = redColor
         
         return [defaultObj,Delete]
      }
      return nil
   }
   
   func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
      if indexPath.row != STPCustomerClass.shared.cardSources.count
      {
         return true
      }
      return false
   }
   
   func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath)
   {
      if indexPath.row == STPCustomerClass.shared.cardSources.count
      {
         if (editingStyle == UITableViewCellEditingStyle.delete) {
            // handle delete (by removing the data from your array and updating the tableview)
         }
         if (editingStyle == UITableViewCellEditingStyle.delete) {
            // handle delete (by removing the data from your array and updating the tableview)
         }
      } else {
         
         
//         var parameter = JSONDictionary()
//         parameter["customerId"] = MyAPIClient.sharedClient.customerID as AnyObject
//         parameter["sourceId"] = self.cards[indexPath.row].id! as AnyObject
//         appDel.showHUD()
//         STPCardApiManager.shared.deleteCard(parameter: parameter, completion: { (response, error) in
//            if error == nil {
//               if let customerDataObjArr = response?.sources?.data {
//                  if customerDataObjArr.count > 0 {
//                     self.cards = customerDataObjArr
//                  }
//               }
//            }
//            self.reloadTableData()
//            appDel.hideHUD()
//         })

      }
   }
   
   func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
   {
      if indexPath.row == STPCustomerClass.shared.cardSources.count
      {
         let viewObj = self.storyboard?.instantiateViewController(withIdentifier: "AddCardViewController") as! AddCardViewController
         viewObj.delegate = self
         self.navigationController?.pushViewController(viewObj, animated: true)
      } else {
         if isFromSetting == true {
            appDel.showHUD()
            appDel.customerContext.selectDefaultCustomerSource(STPCustomerClass.shared.cardSources[indexPath.row], completion: { (error) in
               
               if error == nil {
                  STPCustomerClass.shared.getCustomer(completion: { (error) in
                     if error == nil {
                        self.reloadTableData()
                        appDel.hideHUD()
//                        STPCustomerClass.shared.defaultSource = STPCustomerClass.shared.cardSources[indexPath.row]
                        self.delegate!.setCard()
                        self.navigationController?.popViewController(animated: true)
                     } else {
                        appDel.hideHUD()
                     }
                  })
               } else {
                  appDel.hideHUD()
               }
            })
         } else {
            STPCustomerClass.shared.selectedSource = STPCustomerClass.shared.cardSources[indexPath.row]
            self.delegate!.setCard()
            self.navigationController?.popViewController(animated: true)
         }
         
       
      }
   }
   
//         self.delegate.setCard(card: self.cards[indexPath.row])
//
//         var parameter = JSONDictionary()
//         parameter["customerId"] = MyAPIClient.sharedClient.customerID as AnyObject
//         parameter["sourceId"] = self.cards[indexPath.row].id! as AnyObject
//         appDel.showHUD()
//         STPCardApiManager.shared.setDefaultCard(parameter: parameter, completion: { (response, error) in
//            if error == nil {
//               if let customerDataObjArr = response?.sources?.data {
//                  if customerDataObjArr.count > 0 {
//                     self.cards = customerDataObjArr
//                  }
//               }
//            }
//            self.reloadTableData()
//            appDel.hideHUD()
//         })
   
   override func didReceiveMemoryWarning()
   {
      super.didReceiveMemoryWarning()
   }
}

class paymentCell : UITableViewCell
{
   @IBOutlet var imgView: UIImageView!
   @IBOutlet var lblCard: UILabel!
   @IBOutlet var lblDefault: UILabel!
}
class AddCardCell: UITableViewCell {
   
   
}
