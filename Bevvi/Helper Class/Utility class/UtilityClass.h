//
//  UtilityClass.h
//  bidBrokers
//
//  Created by Ankit on 09/04/15.
//  Copyright (c) 2015 Pooja. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
//#import <AFNetworking/UIImageView+AFNetworking.h>
//#import <AFNetworking/AFImageDownloader.h>
//#import <AFNetworking/AFAutoPurgingImageCache.h>
@class mailConformViewController;

@interface UtilityClass : NSObject <UITextFieldDelegate>

// set RGB Value

+ (UIColor *) colorWithHexString:(float)r andGreen : (float)g andBlue:(float)b;

// Email Address Validation
+ (void)clearImageCacheForURL:(NSURL *)url imageView:(UIImageView *)imgView;
//+ (void)clearCached:(AFAutoPurgingImageCache *)imageCache Request:(NSURLRequest *)request;
+(BOOL)Emailvalidate:(NSString *)tempMail;
+(BOOL)removeSpace:(NSString *)str;
+(BOOL)numericValueForTextBox:(NSString *)string;
+(BOOL)alphabaticValueForTextBox:(NSString *)string;
+(BOOL)textFieldTextwithLimit:(UITextField *)textField andRange:(NSRange)range andString:(NSString *)string withCharacterLimit:(int)limit;

// this is for Display AlertView with Message
+(void)showAlert:(NSString*)message;

// this method for set scrollview ins 4s
+(void)setMyScrollView:(UIScrollView *)scrollObj viewController:(UIViewController *)view;

// this is method for  set Already Button in 4s
+(void)setMyAlreadyBtn:(UIButton *)btn;

// this is method for in category view to set button Border ex. 10k 5k
+(void)myBtnSet:(UIView *)myView;
+(void)setBorderAndPlaceHolder:(UITextField *)txtfld;
+(void)setBorderAndPlaceHolderForBlue:(UITextField *)txtfld;
+(void)setMyCustomAnimation:(UIView *)myView rect:(CGRect)rect;
+(void)setMyViewBorder:(UIView *)myView withBorder:(float)width radius:(float)radius;
+(void)setMyViewBorder:(UIView *)myView withBorder:(float)width radius:(float)radius color:(UIColor *)color;
+(void)setShedowInView:(UIView *)view;
+(void)setMyRotationhide:(UIView *)myView;
+(void)setMyRotationshow:(UIView *)myView;
+(void)setBtnBackGround:(UIButton *)btn;
+(void)setOtherBtnBackGround:(UIButton *)btn;

+(NSString *)getUTCtoSystem:(NSString *)string;

//+(void)postDataWithUrl:(NSString *)url image:(NSData *)image1 image2Data:(NSData *)image2 image3Data:(NSData *)image3 withDict:(NSDictionary *)dict sucessBlock:(void (^)(id responseObject))sucess failureBlock:(void (^)(NSError *error))fail;

+(void)postDataWithUrl:(NSString *)url image:(NSData *)image1 uolpadFstImg:(BOOL)img image2Data:(NSData *)image2 uolpadscndImg:(BOOL)img image3Data:(NSData *)image3 uolpadThrdImg:(BOOL)img withDict:(NSDictionary *)dict sucessBlock:(void (^)(id responseObject))sucess failureBlock:(void (^)(NSError *error))fail;

+ (void)textField:(UITextField *)textField1;
//+(UIImage *)setGifImage;
+(void)removelbl:(UIView *)view;
+(void)removeTextfield:(UIView *)view;

+(void)showHud:(UIView *)view;
+(void)hideHud:(UIView *)view;

+(NSString *)encodeToBase64String:(UIImage *)image;

+(CGRect)getFrectX:(CGFloat)x y:(CGFloat)y h:(CGFloat)h w:(CGFloat)w i:(int)i;

+(void)setNsUserDefaultValue:(id)value key:(NSString *)str;
+(NSString *)getNsUserDefault:(NSString *)str;

+(void)registerPushNotification:(UIApplication *)application;
+(void)setTint;
+ (int)lineCountForLabel:(UILabel *)label;
+(NSString *)get12TimeFrom24Time:(NSString *)str;
+(NSString *)get12TimeFrom24TimeActivityFeed:(NSString *)str;
+(NSString *)get12TimeFrom24TimeHostActivity:(NSString *)str;
+(NSString *)get12TimeFrom24TimeActivityFeedDateTime:(NSString *)str;

+(NSDate *)getDate:(NSString *)str;
+(NSString *)getCurrentDate;
+(NSString *)getCurrentTime;
+(NSString *)getLowerValue:(float)slider;
+(NSString *)getTimeTo24Hour:(float)slider;
+(NSString *)getUpperValue:(float)slider;

//signup

+(NSString *)getBirthDate:(NSDate *)date;

+(float)getSliderValue:(NSString *)timeStr;

+(NSString *)getCurrentSelectStartTime;

+(NSString *)getUTcTime;
+(NSString *)getSystemTime :(NSString *)date;

+(NSString *)getTimeFromDate:(NSDate *)date;


+(NSMutableArray *)getDay;
+(NSMutableArray *)getDayDate;

+(void)setMyContentOffset:(UIScrollView *)scrollObj point:(CGPoint)point;
+ (UIImage *)imageWithColor:(UIColor *)color;

+(float)getFloatValue:(float)value;

+ (UIImage *)imageByCroppingImage:(UIImage *)image toSize:(CGSize)size;

// Keyboard Show And Hide

+(void)isIQKeyboardShow:(BOOL)value;

//+(void)setUpTextFieldforIphone:(UIView *)containerView andtextView:(HPGrowingTextView *)textViewObj andViewController:(UIViewController *)viewController;
//+(void)setHeightForTextview:(UIView *)containerView andtextview:(HPGrowingTextView *)growingTextView andheight:(float)height;
+(void)keyboardShow:(NSNotification *)notification and:(UIViewController *)viewController and:(UIView *)uiview;


+(NSString *)saveSenderPic:(UIImage *)image;
+(NSString *)saveReceivePic:(UIImage *)image name:(NSString *)string;



+(CGRect)getrectFromText:(NSString *)string andVc:(UIViewController *)controller;
+(BOOL) CompareDateTime : (NSString *)date : (NSString *)CurrentDate;
+(void)registerPushNotification;

+(void)removeBadgeIcon:(UITabBar *)tabbar;

+(int)getAgeFromBdate:(NSDate *)date;

+(void)setStatusbar:(int)statusbar;

@end
