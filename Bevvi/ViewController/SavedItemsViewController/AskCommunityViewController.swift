//
//  AskCommunityViewController.swift
//  Bevvi
//
//  Created by Hetal Govani on 26/10/17.
//  Copyright © 2017 solulab. All rights reserved.
//

import UIKit

class AskCommunityViewController: UIViewController {

    @IBOutlet var viewHeight : NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewHeight.constant = self.view.frame.height - 180
        self.title = "Oyster Bay"
        
        let button1 = UIButton(type: UIButtonType.custom)
        button1.setImage(#imageLiteral(resourceName: "close"), for: UIControlState.normal)
        button1.addTarget(self, action:#selector(btnBackPress(sender:)), for: UIControlEvents.touchUpInside)
        button1.frame=CGRect.init(x: 0, y: 0, width: 30, height: 30)
        let barButton1 = UIBarButtonItem(customView: button1)
        self.navigationItem.leftBarButtonItem = barButton1
    }
    @IBAction func btnPostPress(sender:UIButton)
    {
        self.view.endEditing(true)
    }
    @IBAction func btnBackPress(sender:UIButton)
    {
        let transition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionMoveIn
        transition.subtype = kCATransitionFromBottom
        self.navigationController?.view.layer.add(transition, forKey: nil)
        self.navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
