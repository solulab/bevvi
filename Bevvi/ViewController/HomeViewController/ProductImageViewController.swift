//
//  ProductImageViewController.swift
//  Bevvi
//
//  Created by Hetal Govani on 13/11/17.
//  Copyright © 2017 solulab. All rights reserved.
//

import UIKit
protocol ProductImageDelegate
{
    func DismissButtonClicked(_ secondDetailViewController: ProductImageViewController)
}
class ProductImageViewController: UIViewController,UIScrollViewDelegate
{
    var delegate: ProductImageDelegate?
    @IBOutlet var scrView : UIScrollView!
    @IBOutlet var pageControll : UIPageControl!
    var arrImage : Array<UIImage>! = Array()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        arrImage = [#imageLiteral(resourceName: "bottle"),#imageLiteral(resourceName: "bottle"),#imageLiteral(resourceName: "bottle")]
        scrView.delegate=self
        scrView.isPagingEnabled = true
        scrView.showsHorizontalScrollIndicator = false
        scrView.showsVerticalScrollIndicator = false
        scrView.contentSize = CGSize.init(width: (UIScreen.main.bounds.width - 20) * 3, height: UIScreen.main.bounds.height-50)
        for i in 0 ..< arrImage.count
        {
            let imgView = UIImageView.init(frame: CGRect.init(x: CGFloat(i)*(UIScreen.main.bounds.width-10), y: 50, width: UIScreen.main.bounds.width-20, height: UIScreen.main.bounds.height - 100))
            imgView.image = #imageLiteral(resourceName: "bottle")
            imgView.contentMode = UIViewContentMode.scaleAspectFit
            scrView.addSubview(imgView)
        }
        
        pageControll.numberOfPages=3
        pageControll.currentPage=0
    }
    
    // MARK : TO CHANGE WHILE CLICKING ON PAGE CONTROL
    @IBAction func changePage(sender : UIPageControl)
    {
        let x = CGFloat(pageControll.currentPage) * scrView.frame.size.width
        scrView.setContentOffset(CGPoint.init(x: x, y: 0), animated: true)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        pageControll.currentPage = Int(pageNumber)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        if scrollView.contentOffset.y>0
        {
            scrollView.contentOffset.y = 0
        }
    }
    
    @IBAction func DismissButtonClicked(sender:UIButton)
    {
        delegate?.DismissButtonClicked(self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
