//
//  ProductDetailViewController.swift
//  Bevvi
//
//  Created by Hetal Govani on 25/10/17.
//  Copyright © 2017 solulab. All rights reserved.
//

import UIKit

class ProductDetailViewController: UIViewController,ProductImageDelegate,customAlertDelegate,customTwoButtonAlertDelegate
{
   @IBOutlet var lblName : UILabel!
   @IBOutlet var lblType : UILabel!
   @IBOutlet var lblBottleLeft : UILabel!
   @IBOutlet var lblOfferExpires : UILabel!
   @IBOutlet var lblOriginalPrice : UILabel!
   @IBOutlet var lblOfferPrice : UILabel!
   @IBOutlet var lblSize : UILabel!
   @IBOutlet var lblALCContent : UILabel!
   @IBOutlet var lblRegion : UILabel!
   @IBOutlet var lblCountry : UILabel!
   @IBOutlet var lblYear : UILabel!
   @IBOutlet var lblVerietal : UILabel!
   @IBOutlet var lblDescription : UILabel!
   @IBOutlet var btnAddToCart : UIButton!
   @IBOutlet var lblQuantity : UILabel!
   var qty : Int!
   @IBOutlet var viewHeight : NSLayoutConstraint!
   @IBOutlet var viewAddToCart : UIView!
   var dictDetail : Dictionary<String,AnyObject>!
   @IBOutlet var rateView : FloatRatingView!
   var address : String!
   var shopName : String!
   lazy var errorView: ErrorView = {
      let Errview = UINib(nibName: "ErrorView", bundle: nil).instantiate(withOwner: self, options: nil)[0] as! ErrorView
      Errview.translatesAutoresizingMaskIntoConstraints = false
      Errview.retryButton.addTarget(self, action: #selector(btnRetryPress(sender:)), for: .touchUpInside)
      return Errview
   }()
   func presentError(_ error: customError) {
      self.errorView.error = error
      
      DispatchQueue.main.async {
         self.view.addSubview(self.errorView)
         self.installErrorViewConstraints()
      }
   }
   
   func installErrorViewConstraints() {
      self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[view]|", options: [], metrics: nil, views: [ "view": self.errorView ]))
      self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[view]|", options: [], metrics: nil, views: [ "view": self.errorView ]))
   }
   @IBAction func btnRetryPress(sender:UIButton)
   {
      errorView.removeFromSuperview()
      if appDel.reachability.connection != .none
      {
         addToCartAPICall()
      }
      else
      {
         self.presentError(.NetworkError)
      }
   }
    override func viewDidLoad()
    {
        super.viewDidLoad()
      
      print("\(dictDetail!)")
      let productDict = dictDetail["product"] as! Dictionary<String,AnyObject>
      
      self.title = "\(productDict["name"]!)"
      
      let button1 = UIButton(type: UIButtonType.custom)
      button1.setImage(#imageLiteral(resourceName: "Back"), for: UIControlState.normal)
      button1.addTarget(self, action:#selector(btnBackPress(sender:)), for: UIControlEvents.touchUpInside)
      button1.frame=CGRect.init(x: 0, y: 0, width: 30, height: 30)
      let barButton1 = UIBarButtonItem(customView: button1)
      self.navigationItem.leftBarButtonItem = barButton1
      
      print(dictDetail!)
      
      let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: "$\(dictDetail["originalPrice"]!)")
      attributeString.addAttribute(NSAttributedStringKey.strikethroughStyle, value: 1, range: NSMakeRange(0, attributeString.length))
      attributeString.addAttribute(NSAttributedStringKey.strikethroughColor, value: redColor, range: NSMakeRange(0, attributeString.length))
      
      lblName.text = "\(productDict["name"]!)"
      lblType.text = "\(productDict["category"]!)"
      lblOfferPrice.text = "$\(dictDetail["salePrice"]!)"
      lblOriginalPrice.attributedText = attributeString
      rateView.rating = Float("\(productDict["rating"]!)")!
      lblBottleLeft.text = "\(dictDetail["remQty"]!) out \(dictDetail["totalQty"]!) bottles left"
      
      if let val = productDict["size"] as? String
      {
         lblSize.text = "\(val)"
      }
      if let val = productDict["alcContent"] as? String
      {
         lblALCContent.text = "\(val)"
      }
      if let val = productDict["region"] as? String
      {
         lblRegion.text = "\(val)"
      }
      if let val = productDict["country"] as? String
      {
         lblCountry.text = "\(val)"
      }
      if let val = productDict["year"] as? String
      {
         lblYear.text = "\(val)"
      }
      if let val = productDict["varietal"] as? String
      {
         lblVerietal.text = "\(val)"
      }
      if let val = productDict["description"] as? String
      {
         lblDescription.text = "\(val)"
      }
      
      viewHeight.constant = estimatedHeightOfLabel(view: self.view, text: lblDescription.text!) + lblDescription.frame.origin.y + 50

      qty = 1
      lblQuantity.text = "\(qty!)"
    }
    
    
   
   @IBAction func btnPlusPress(sender:UIButton)
   {
      qty = qty + 1
      if qty > Int("\(dictDetail["remQty"]!)")!{
         qty = Int("\(dictDetail["remQty"]!)")!
         
         let ratingVC = CustomAlertViewController(nibName: "CustomAlertViewController", bundle: nil)
         
         ratingVC.delegate=self
         ratingVC.strMessage = "It exceeded the limit."
         self.presentPopupViewController(ratingVC, animationType: MJPopupViewAnimationFade)
      }
      lblQuantity.text = "\(qty!)"
   }
   
   @IBAction func btnMinusPress(sender:UIButton)
   {
      qty = qty - 1
      if qty <= 1
      {
         qty = 1
         lblQuantity.text = "\(qty!)"
      }
      else
      {
         lblQuantity.text = "\(qty!)"
      }
   }
   func btnSubmitPress(viewController: CustomTwoButtonAlertViewController) {
      self.dismissPopupViewControllerWithanimationType(MJPopupViewAnimationFade)
      EmptyCart()
      
   }
   func EmptyCart() {
      if (UserDefaults.standard.object(forKey: "accountId") == nil)
      {
         CartNotLogIn.sharedInstance.deleteAllRecords {

            UserDefaults.standard.removeObject(forKey:"establishmentId")
            UserDefaults.standard.synchronize()
            appDel.hideHUD()
            self.addToCartAPICall()
         }
      }
      else
      {
         let para:NSMutableDictionary = NSMutableDictionary()
         para.setObject(4, forKey: "state" as NSCopying)
         if let theJSONData = try?  JSONSerialization.data(
            withJSONObject: para,
            options: .prettyPrinted
            ),
            let theJSONText = String(data: theJSONData,
                                     encoding: String.Encoding.ascii) {
            print("JSON string = \n\(theJSONText)")
            
            let storeListing = StoreListing()
            storeListing.jsonArray = theJSONText
            appDel.showHUD()
            ApiManager.shared.emptyCart(accountId: "\(UserDefaults.standard.value(forKey: "accountId")!)", listing: storeListing, view: self.view, completion: { (dict:JSONDictionary) in
               print(dict)
               UserDefaults.standard.removeObject(forKey:"establishmentId")
               UserDefaults.standard.synchronize()
               appDel.hideHUD()
               self.addToCartAPICall()
            })
         }
      }
   }
   func btnDismissPress(viewController: CustomTwoButtonAlertViewController) {
      self.dismissPopupViewControllerWithanimationType(MJPopupViewAnimationFade)

   }
    @IBAction func btnAddToCartPress(sender:UIButton)
    {
      if (UserDefaults.standard.object(forKey: "establishmentId") != nil)
      {
         if "\(self.dictDetail["establishmentId"]!)" != "\(UserDefaults.standard.value(forKey: "establishmentId")!)"
         {
            let ratingVC = CustomTwoButtonAlertViewController(nibName: "CustomTwoButtonAlertViewController", bundle: nil)
            
            ratingVC.delegate=self
            ratingVC.strTitle = "Cart not empty."
            ratingVC.strMessage = "Cart contains items from a different liquor store. Would you like to empty this cart and add this item?."
            
            self.presentPopupViewController(ratingVC, animationType: MJPopupViewAnimationFade)
         }
         else
         {
            if appDel.reachability.connection != .none
            {
               addToCartAPICall()
            }
            else
            {
               self.presentError(.NetworkError)
            }
         }
      }
      else
      {
         if appDel.reachability.connection != .none
         {
            addToCartAPICall()
         }
         else
         {
            self.presentError(.NetworkError)
         }
      }
    }
   func addToCartAPICall()
   {
         let storeDetail = StoreListing()
         appDel.showHUD()

//      OfflineGameStore.sharedInstance.saveGoalKeeper(forEntityName: "OfflineGoalKeeper", object: goalkeeper.toJsonDict())
      
      if (UserDefaults.standard.object(forKey: "accountId") == nil)
      {
         if let uniqueId = CartNotLogIn.sharedInstance.getLastObjectUniqueID()
         {
            let cartObj = cartDetail()
            
            let productDict = dictDetail["product"] as! Dictionary<String,AnyObject>
            
            cartObj.uniqueId = uniqueId
            cartObj.isFromServer = "0"
            cartObj.productId = "\(dictDetail["productId"]!)"
            cartObj.quantity = Int("\(lblQuantity.text!)")
            cartObj.id = "\(dictDetail["id"]!)"
            cartObj.name = "\(productDict["name"]!)"
            cartObj.varietal = "\(productDict["varietal"]!)"
            cartObj.salePrice = "\(dictDetail["salePrice"]!)"
            cartObj.originalPrice = "\(dictDetail["originalPrice"]!)"
            cartObj.dict = dictDetail
            cartObj.storeName = shopName
            cartObj.address = address

            CartNotLogIn.sharedInstance.getAllCartProductsNotLoggedIn(completion: { (cart:[cartDetail]) in
               var isProductExist : Bool! = false
               for product in cart
               {
                  if product.productId == cartObj.productId
                  {
                     let quantity = Int("\(product.quantity!)")! + Int("\(cartObj.quantity!)")!
                     isProductExist = true
                     CartNotLogIn.sharedInstance.updateGameDataObjects(quantity: quantity, object: product.toJsonDict())
                  }
               }
               if !isProductExist
               {
                  CartNotLogIn.sharedInstance.saveProduct(forEntityName: "CartNotLoggedIn", object: cartObj.toJsonDict())
               }
               UserDefaults.standard.set("\(self.dictDetail["establishmentId"]!)", forKey: "establishmentId")
               UserDefaults.standard.synchronize()
               self.btnAddToCart.setTitle("ADDED", for: .normal)
               self.viewAddToCart.backgroundColor = MaroonColor
               self.tabBarController?.addBadge(index: 1, value: 1, color: redColor, font: UIFont.init(name: "Avenir Medium", size: 13)!)
               appDel.hideHUD()
            })
         }
      }
      else
      {
         ApiManager.shared.addToCart(accountId: "\(UserDefaults.standard.value(forKey: "accountId")!)", productId: "\(dictDetail["productId"]!)", offerId: "\(dictDetail["id"]!)", quantity: Int("\(lblQuantity.text!)")!, listing: storeDetail, view: self.view, completion: { (arr:JSONDictionary) in
            
            print(arr)
            appDel.hideHUD()
            UserDefaults.standard.set("\(self.dictDetail["establishmentId"]!)", forKey: "establishmentId")
            UserDefaults.standard.synchronize()
            UIView.transition(with: self.btnAddToCart, duration: 0.5, options: .transitionFlipFromBottom, animations: {
               self.btnAddToCart.setTitle("ADDED", for: .normal)
               self.viewAddToCart.backgroundColor = MaroonColor

               self.tabBarController?.addBadge(index: 1, value: 1, color: redColor, font: UIFont.init(name: "Avenir Medium", size: 13)!)
            }, completion: nil)
         })
      }
   }
   
    func btnSubmitPress(viewController: CustomAlertViewController)
    {
        self.dismissPopupViewControllerWithanimationType(MJPopupViewAnimationFade)
    }
    @IBAction func btnOpenImages(sender:UIButton)
    {
        let ratingVC = ProductImageViewController(nibName: "ProductImageViewController", bundle: nil)
        ratingVC.view.frame = CGRect.init(x: 10, y: UIScreen.main.bounds.height - 20, width: UIScreen.main.bounds.width - 20, height: UIScreen.main.bounds.height - 40)
        ratingVC.delegate=self
        self.presentPopupViewController(ratingVC, animationType: MJPopupViewAnimationFade)
    }
    
    func DismissButtonClicked(_ secondDetailViewController: ProductImageViewController)
    {
        self.dismissPopupViewControllerWithanimationType(MJPopupViewAnimationFade)
    }

    @IBAction func btnBackPress(sender:UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}
