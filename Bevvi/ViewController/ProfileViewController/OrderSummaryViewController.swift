//
//  OrderSummaryViewController.swift
//  Bevvi
//
//  Created by Hetal Govani on 30/10/17.
//  Copyright © 2017 solulab. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

class OrderSummaryViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,RateUsDelegate,MKMapViewDelegate,CLLocationManagerDelegate
{
   @IBOutlet var tblObj : UITableView!
   @IBOutlet var tblHeight : NSLayoutConstraint!
   @IBOutlet var viewHeight : NSLayoutConstraint!
   @IBOutlet var pickupViewHeight : NSLayoutConstraint!
   @IBOutlet var btnRateProduct : UIButton!
   var dictDetail : Dictionary<String,AnyObject>! = Dictionary()
   var dictEstablishment : Dictionary<String,AnyObject>! = Dictionary()
   @IBOutlet var lblOrderNumber : UILabel!
   var arrOrderDetail : Array<Dictionary<String,AnyObject>>! = Array()
   var arrPrice : Array<CGFloat>! = Array()
   var arrQuantity : Array<Int>! = Array()
   @IBOutlet var inviteViewHeight : NSLayoutConstraint!
   @IBOutlet var PaymentViewHeight : NSLayoutConstraint!
   @IBOutlet var btnRateHeight : NSLayoutConstraint!
   
   @IBOutlet var txtInviteCode : UITextField!
   
   @IBOutlet var lblName : UILabel!
   @IBOutlet var lblAddress : UILabel!
   @IBOutlet var lblPickupDate : UILabel!
   @IBOutlet var lblPickupTime : UILabel!
   @IBOutlet weak var mapView: MKMapView!
   private var locationManager: CLLocationManager!
   
   var isPickup : Bool! = false
   var isFromCheckOut : Bool!
   
   override func viewDidLoad()
   {
      super.viewDidLoad()
      self.title = "Order Summary"
      viewHeight.constant = self.view.frame.height - 64
      print(dictDetail)
      lblOrderNumber.text = "Order number \(dictDetail["id"]!)"
      dictEstablishment = dictDetail["establishment"] as! [String:AnyObject]
      lblName.text = "\(dictEstablishment["name"]!)"
      lblAddress.text = "\(dictEstablishment["address"]!)"
      
      let RFC3339DateFormatter = DateFormatter()
      RFC3339DateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
      RFC3339DateFormatter.locale = Locale(identifier: "us")
      let dateFormatter = DateFormatter()
      dateFormatter.dateFormat = "EEEE MMM dd, yyyy"
      
      let date: Date? = RFC3339DateFormatter.date(from: "\(dictDetail["pickupTime"]!)")
      print(dateFormatter.string(from: date!))
      lblPickupDate.text = "\(dateFormatter.string(from:date!))"
      let dateFormatter1 = DateFormatter()
      dateFormatter1.dateFormat = "hh:mm a"
      lblPickupTime.text = dateFormatter1.string(from: date!)
      if isPickup == false
      {
         pickupViewHeight.constant = 0
         inviteViewHeight.constant = 0
      }
      else
      {
         btnRateProduct.isHidden = true
         btnRateHeight.constant = 0
      }
      if isFromCheckOut == false
      {
         inviteViewHeight.constant = 0
      }
      else
      {
         btnRateHeight.constant = 0
         inviteViewHeight.constant = 150
         PaymentViewHeight.constant = 0
      }
      let button = UIButton(type: UIButtonType.custom)
      button.setImage(#imageLiteral(resourceName: "settings"), for: UIControlState.normal)
      button.addTarget(self, action:#selector(btnSettingPress(sender:)), for: UIControlEvents.touchUpInside)
      button.frame=CGRect.init(x: 0, y: 0, width: 30, height: 30)
      let barButton = UIBarButtonItem(customView: button)
      self.navigationItem.rightBarButtonItem = barButton
      
      let button1 = UIButton(type: UIButtonType.custom)
      button1.setImage(#imageLiteral(resourceName: "Back"), for: UIControlState.normal)
      button1.addTarget(self, action:#selector(btnBackPress(sender:)), for: UIControlEvents.touchUpInside)
      button1.frame=CGRect.init(x: 0, y: 0, width: 30, height: 30)
      let barButton1 = UIBarButtonItem(customView: button1)
      self.navigationItem.leftBarButtonItem = barButton1
      mapView.delegate = self
      
      locationManager = CLLocationManager()
      locationManager.delegate = self
      locationManager.desiredAccuracy = kCLLocationAccuracyBest
      
      // Check for Location Services
      DispatchQueue.main.async {
         self.setMapView()
      }
      if let arr = dictDetail["orderdetails"] as? Array<Dictionary<String,AnyObject>>
      {
         arrOrderDetail = arr
      }
      
      tblObj.layer.borderColor = (UIColor.lightGray.withAlphaComponent(0.8)).cgColor
      tblObj.layer.borderWidth = 0.5
      
      for dict in self.arrOrderDetail
      {
         
         let salePrice = "\(dict["price"]!)"
         let quantity = "\(dict["quantity"]!)"
         
         self.arrPrice.append(CGFloat(NumberFormatter().number(from:salePrice)!))
         self.arrQuantity.append(Int(quantity)!)
      }
   }
   override func viewWillAppear(_ animated: Bool) {
      super.viewWillAppear(true)
      self.tblObj.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
      
   }
   override func updateViewConstraints()
   {
      super.updateViewConstraints()
      if isFromCheckOut == false
      {
         viewHeight.constant = tblObj.frame.origin.y + tblHeight.constant + 270
      }
      else
      {
         viewHeight.constant = tblObj.frame.origin.y + tblHeight.constant + 350
      }
   }
   
   func setMapView()
   {
      let arrgeoLocation = dictEstablishment["geoLocation"]! as! Dictionary<String,AnyObject>
      let arrCoord = arrgeoLocation["coordinates"] as! Array<NSNumber>
      
      let shopLatitude = (arrCoord[1] as NSNumber).doubleValue
      let shopLongitude = (arrCoord[0] as NSNumber).doubleValue
      
      let anno = Annonation.init(coordinate: CLLocationCoordinate2D(latitude: shopLatitude as CLLocationDegrees, longitude: shopLongitude as CLLocationDegrees))
      
      self.mapView.addAnnotation(anno)
      
      let location = CLLocation.init(latitude: shopLatitude as CLLocationDegrees, longitude: shopLongitude as CLLocationDegrees)
      
      let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
      let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate, 1000, 1000)
      mapView.setRegion(coordinateRegion, animated: true)
      print(shopLatitude)
      print(shopLongitude)
      self.lblAddress.text = "\(dictEstablishment["address"]!)"
      //      let arrDist = dictEstablishment["distinfo"] as! Array<Dictionary<String,AnyObject>>
      //      let dictDuration = arrDist[0]["duration"] as! Dictionary<String,AnyObject>
      //      if Int("\(UserDefaults.standard.value(forKey: "walk")!)") == 0 {
      //         lblDistance.text = "\(dictDuration["text"]!) walk"
      //      }
      //      else
      //      {
      //         lblDistance.text = "\(dictDuration["text"]!) drive"
      //      }
   }
   func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
      let identifier = "pin"
      
      if annotation.isKind(of: MKUserLocation.classForCoder()) {
         return nil
      }
      var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
      if  annotationView == nil {
         annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: "pin")
         annotationView!.canShowCallout = true
         annotationView!.image = #imageLiteral(resourceName: "location")
      }
      else {
         annotationView!.annotation = annotation
      }
      return annotationView
      
   }
   func openMapForPlace(location: CLLocation)
   {
      let latitude: CLLocationDegrees = location.coordinate.latitude
      let longitude: CLLocationDegrees = location.coordinate.longitude
      
      let regionDistance:CLLocationDistance = 10000
      let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
      let regionSpan = MKCoordinateRegionMakeWithDistance(coordinates, regionDistance, regionDistance)
      let options = [
         MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
         MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
      ]
      let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
      let mapItem = MKMapItem(placemark: placemark)
      mapItem.name = "Place Name"
      mapItem.openInMaps(launchOptions: options)
   }
   
   // MARK - CLLocationManagerDelegate
   
   func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
   {
      defer
      {
         currentLocation = locations.last
      }
      if currentLocation == nil
      {
         // Zoom to user location
         if let userLocation = locations.last
         {
            let viewRegion = MKCoordinateRegionMakeWithDistance(userLocation.coordinate, 2000, 2000)
            mapView.setRegion(viewRegion, animated: false)
         }
      }
   }
   @IBAction func btnCopyPress(sender:UIButton)
   {
      UIPasteboard.general.string = txtInviteCode.text!
      self.showToast(message: "Copied..!!")
   }
   @IBAction func btnInvitePress(sender:UIButton)
   {
      // text to share
      //      Make your hour hap÷py with Bevvi!!!! You friend <name> has given you $5 off your first Bevvi order with promo code <code>. Order now: <link>
      let text = "Make your hour happy with Bevvi!!!! Your friend has given you $5 off on your first Bevvi order with promo code \(txtInviteCode.text!). Order now: https://itunes.apple.com/us/app/bevvi/id1309066556?ls=1&mt=8"
      
      let textToShares = [text]
      
      // set up activity view controller
      let activityViewController = UIActivityViewController(activityItems: textToShares, applicationActivities: nil)
      activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
      
      // exclude some activity types from the list (optional)
      activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
      
      // present the view controller
      self.present(activityViewController, animated: true, completion: nil)
   }
   @IBAction func btnClosePress(sender:UIButton)
   {
      inviteViewHeight.constant = 0
   }
   @IBAction func btnMapPress(sender:UIButton)
   {
      let alert = UIAlertController.init(title: APP_NAME, message: "Open Map?", preferredStyle: .alert)
      alert.addAction(UIAlertAction.init(title: "Open", style: UIAlertActionStyle.default, handler: { (action) in
         
         //         let shopLatitude = (self.arrCoord[1] as NSNumber).doubleValue
         //         let shopLongitude = (self.arrCoord[0] as NSNumber).doubleValue
         //         let homeLocation = CLLocation(latitude: shopLatitude, longitude: shopLongitude)
         //         self.mapView.showsUserLocation = true
         //         self.openMapForPlace(location: homeLocation)
      }))
      alert.addAction(UIAlertAction.init(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
      self.present(alert, animated: true, completion: nil)
   }
   
   override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?)
   {
      tblObj.layer.removeAllAnimations()
      tblHeight.constant = tblObj.contentSize.height
      UIView.animate(withDuration: 0.5)
      {
         self.tblObj.layoutIfNeeded()
         self.updateViewConstraints()
      }
   }
   
   
   func numberOfSections(in tableView: UITableView) -> Int
   {
      return 2
   }
   
   func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
      if section == 0
      {
         return 50
      }
      return UITableViewAutomaticDimension
   }
   
   func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
      if section == 0
      {
         return "ITEMS"
      }
      return nil
   }
   
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      if section == 0
      {
         return arrOrderDetail.count
      }
      else
      {
         return 3
      }
   }
   
   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      if indexPath.section == 0 {
         let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! itemCell
         cell.selectionStyle = .none
         let dictProduct = arrOrderDetail[indexPath.row] as JSONDictionary
         let dictProductDetail = dictProduct["product"] as! JSONDictionary
         
         cell.lblTitle.text = "\(dictProductDetail["name"]!)"
         cell.lblDesc.text = "\(dictProductDetail["varietal"]!)"
         cell.lblQuantity.text = "\(arrOrderDetail[indexPath.row]["quantity"]!)"
         
         let price = CGFloat(NumberFormatter().number(from: "\(arrOrderDetail[indexPath.row]["price"]!)")!) * CGFloat(NumberFormatter().number(from: "\(arrOrderDetail[indexPath.row]["quantity"]!)")!)
         
         cell.lblPrice.text = "$\(price)"
         
         return cell
      }
      else
      {
         let cell = tableView.dequeueReusableCell(withIdentifier: "cellFinal", for: indexPath) as! finalInfoCell
         if indexPath.row == 0
         {
            cell.lblTitle.text = "Tax"
            
            cell.lblPrice.textColor = UIColor.init(red: 141/255, green: 141/255, blue: 141/255, alpha: 1)
            cell.lblPrice.text = "5%"
         }
            
         else if indexPath.row == 1
         {
            cell.lblTitle.text = "TOTAL"
            cell.lblTitle.textColor = UIColor.black
            var totalPrice : CGFloat = 0
            for i in 0 ..< arrPrice.count
            {
               totalPrice = totalPrice + (arrPrice[i] * CGFloat(arrQuantity[i]))
            }
            totalPrice = totalPrice + (totalPrice * 5) / 100
            
            cell.lblPrice.textColor = UIColor.black
            cell.lblPrice.text = "$\(totalPrice)"
            
         }
         else if indexPath.row == 2
         {
            cell.lblTitle.text = "Points earned"
            cell.lblTitle.font = UIFont.init(name: "Avenir-Roman", size: 13)
            cell.lblTitle.textColor = redColor
            
            cell.lblPrice.font = UIFont.init(name: "Avenir-Roman", size: 13)
            cell.lblPrice.textColor = redColor
            cell.lblPrice.text = "0 pts"
         }
         return cell
      }
   }
   @IBAction func btnSettingPress(sender:UIButton)
   {
      let viewObj = self.storyboard?.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
      self.navigationController?.pushViewController(viewObj, animated: true)
   }
   
   @IBAction func btnRateProductPress(sender:UIButton)
   {
      let ratingVC = RateUsViewController(nibName: "RateUsViewController", bundle: nil)
      ratingVC.delegate=self
      ratingVC.dictDetail = dictDetail
      self.presentPopupViewController(ratingVC, animationType: MJPopupViewAnimationFade)
   }
   func SubmitButtonClicked(_ secondDetailViewController: RateUsViewController) {
      self.dismissPopupViewControllerWithanimationType(MJPopupViewAnimationFade)
   }
   
   func DismissButtonClicked(_ secondDetailViewController: RateUsViewController) {
      self.dismissPopupViewControllerWithanimationType(MJPopupViewAnimationFade)
   }
   override func viewWillDisappear(_ animated: Bool)
   {
      super.viewWillDisappear(true)
      self.tblObj.removeObserver(self, forKeyPath: "contentSize", context: nil)
   }
   @IBAction func btnBackPress(sender:UIButton)
   {
      self.navigationController?.popViewController(animated: true)
   }
   override func didReceiveMemoryWarning() {
      super.didReceiveMemoryWarning()
   }
}
class itemCell: UITableViewCell
{
   @IBOutlet var lblTitle : UILabel!
   @IBOutlet var lblDesc : UILabel!
   @IBOutlet var lblPrice : UILabel!
   @IBOutlet var lblQuantity : UILabel!
}
class finalInfoCell: UITableViewCell {
    @IBOutlet var lblTitle : UILabel!
    @IBOutlet var lblPrice : UILabel!
}
