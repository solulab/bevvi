//
//  SettingsViewController.swift
//  Bevvi
//
//  Created by Hetal Govani on 01/11/17.
//  Copyright © 2017 solulab. All rights reserved.
//

import UIKit
import Stripe

class SettingsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource, STSelectedCardDelegate
{
   
   
   @IBOutlet var tblObj : UITableView!
   @IBOutlet var cell1 : UITableViewCell!
   @IBOutlet var cell2 : UITableViewCell!
   @IBOutlet var cell3 : UITableViewCell!
   @IBOutlet var cell4 : UITableViewCell!
   
   @IBOutlet var lblCardName : UILabel!
   @IBOutlet var cardImg : UIImageView!
   @IBOutlet var cardNumber : UILabel!
   
   @IBOutlet var btnWalk : UIButton!
   @IBOutlet var btnDrive : UIButton!
   var transportType : Int! = 0
   @IBOutlet var lblAddress : UILabel!
   @IBOutlet var lblEmail : UILabel!
   @IBOutlet var txtPhoneNumber : UITextField!

   @IBOutlet var progressview : RangeSeekSlider!
   @IBOutlet var switchNotification : UISwitch!
   
   
   override func viewDidLoad()
   {
      super.viewDidLoad()
      
      self.title = "Settings"
      let button1 = UIButton(type: UIButtonType.custom)
      button1.setImage(#imageLiteral(resourceName: "Back"), for: UIControlState.normal)
      button1.addTarget(self, action:#selector(btnBackPress(sender:)), for: UIControlEvents.touchUpInside)
      button1.frame=CGRect.init(x: 0, y: 0, width: 30, height: 30)
      let barButton1 = UIBarButtonItem(customView: button1)
      self.navigationItem.leftBarButtonItem = barButton1
      
      tblObj.layer.borderColor = UIColor.lightGray.withAlphaComponent(0.8).cgColor
      tblObj.layer.borderWidth = 0.5
      
      btnWalkingPress(sender: UIButton())
   }
   override func viewWillAppear(_ animated: Bool) {
      super.viewWillAppear(true)
      
      var address : String! = "Set Location"
      if let str = UserDefaults.standard.value(forKey: "address") as? String
      {
         let arr = "\(str)".components(separatedBy: ",")
         address = "\(arr[0])"
      }
      lblAddress.text = address
      getUserData()
      self.tblObj.reloadData()
   }
   @IBAction func btnDonePress(senedr:UIButton)
   {
      postUserData()
   }
   func getUserData() {
      let para:NSMutableDictionary = NSMutableDictionary()
      
      let geoLocation:NSDictionary = NSDictionary(object: "\(UserDefaults.standard.value(forKey: "accountId")!)", forKey: "accountId" as NSCopying)
      let includeDict:NSMutableArray = ["account"]
      para.setObject(geoLocation, forKey: "where" as NSCopying)
      para.setObject(includeDict, forKey: "include" as NSCopying)
      if let theJSONData = try?  JSONSerialization.data(
         withJSONObject: para,
         options: .prettyPrinted
         ),
         let theJSONText = String(data: theJSONData,
                                  encoding: String.Encoding.ascii)
      {
         print("JSON string = \n\(theJSONText)")
         
         let storeDetail = StoreListing()
         storeDetail.jsonArray = theJSONText
         appDel.showHUD()
         ApiManager.shared.getSetting(listing: storeDetail, completion: { (arr:[JSONDictionary]) in
            print(arr)
            if arr.count > 0
            {
               let dict = arr.last!
               let dictAccount = dict["account"] as! JSONDictionary
               if let phoneNum = dictAccount["phoneNumber"] as? String
               {
                  self.txtPhoneNumber.text = "\(phoneNum)"
               }
               self.lblEmail.text = "\(dictAccount["email"]!)"
               self.transportType = Int("\(dict["transport"]!)")
               if self.transportType == 0
               {
                  UserDefaults.standard.set(0, forKey: "walk")
                  UserDefaults.standard.synchronize()
                  self.btnWalkingPress(sender: UIButton())
               }
               else
               {
                  self.btnDrivingPress(sender: UIButton())
                  UserDefaults.standard.set(1, forKey: "walk")
                  UserDefaults.standard.synchronize()
               }
               self.progressview.selectedMinValue = 0
               if let n = NumberFormatter().number(from: "\(dict["radius"]!)") {
                  UserDefaults.standard.set(Double.init("\(dict["radius"]!)")! * 1609.34, forKey: "maxRadious")
//                  UserDefaults.standard.set(n * (NSNumber.init(value: 1609.34)), forKey: "maxRadious")
                  UserDefaults.standard.synchronize()
                  self.progressview.selectedMaxValue = CGFloat(truncating: n)
                  self.progressview.setNeedsLayout()
               }
               if Int("\(dict["notifications"]!)") == 1
               {
                  self.switchNotification.setOn(true, animated: true)
                  
               }
               else
               {
                  self.switchNotification.setOn(false, animated: true)
               }
            }
            else
            {
               self.getUserProfile()
            }
            appDel.hideHUD()
         })
      }
   }
   func getUserProfile()
   {
      let para:NSMutableDictionary = NSMutableDictionary()
      
      let geoLocation:NSDictionary = NSDictionary(object: "\(UserDefaults.standard.value(forKey: "accountId")!)", forKey: "accountId" as NSCopying)
      let includeDict:NSMutableArray = ["account"]
      para.setObject(geoLocation, forKey: "where" as NSCopying)
      para.setObject(includeDict, forKey: "include" as NSCopying)
      if let theJSONData = try?  JSONSerialization.data(
         withJSONObject: para,
         options: .prettyPrinted
         ),
         let theJSONText = String(data: theJSONData,
                                  encoding: String.Encoding.ascii)
      {
         print("JSON string = \n\(theJSONText)")
         
         let storeDetail = StoreListing()
         storeDetail.jsonArray = theJSONText
         ApiManager.shared.getUserDataAfterlogin(accountId: "\(UserDefaults.standard.value(forKey: "accountId")!)", listing: storeDetail, view: self.view, completion: { (arr:JSONDictionary) in
            appDel.hideHUD()
            
            print(arr)
            if let phoneNum = arr["phoneNumber"] as? String
            {
               self.txtPhoneNumber.text = "\(phoneNum)"
            }
            self.lblEmail.text = "\(arr["email"]!)"
         })
      }
   }
   func postUserData()
   {
      let para:NSMutableDictionary = NSMutableDictionary()
      para.setObject("\(UserDefaults.standard.value(forKey: "accountId")!)", forKey: "accountId" as NSCopying)
      para.setObject(progressview.selectedMaxValue, forKey: "radius" as NSCopying)
      para.setObject(transportType, forKey: "transport" as NSCopying)
      if switchNotification.isOn
      {
         para.setObject(true, forKey: "notifications" as NSCopying)
      }
      else
      {
         para.setObject(false, forKey: "notifications" as NSCopying)
      }

      if let theJSONData = try?  JSONSerialization.data(
         withJSONObject: para,
         options: .prettyPrinted
         ),
         let theJSONText = String(data: theJSONData,
                                  encoding: String.Encoding.ascii)
      {
         print("JSON string = \n\(theJSONText)")
         
         let storeDetail = StoreListing()
         storeDetail.jsonArray = theJSONText
         appDel.showHUD()
         ApiManager.shared.postSetting(listing: storeDetail, completion: { (dict:JSONDictionary) in
            print(dict)
            
            let para:NSMutableDictionary = NSMutableDictionary()
            para.setObject(self.txtPhoneNumber.text!, forKey: "phoneNumber" as NSCopying)
            
            if let theJSONData = try?  JSONSerialization.data(
               withJSONObject: para,
               options: .prettyPrinted
               ),
               let theJSONText = String(data: theJSONData,encoding: String.Encoding.ascii)
            {
               print("JSON string = \n\(theJSONText)")
               
               let storeDetail = StoreListing()
               storeDetail.jsonArray = theJSONText
               ApiManager.shared.sendPhoneNumber(accountId: "\(UserDefaults.standard.value(forKey: "accountId")!)", listing: storeDetail, view: self.view, completion: { (dict:JSONDictionary) in
                  
                  print(dict)
                  appDel.hideHUD()
               })
            }
         })
      }
   }
   
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return 4
   }
   func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
   {
      if indexPath.row == 0
      {
         return 180
      }
      else if indexPath.row == 1 {
         return 128
      }
      else if indexPath.row == 2 {
         return 200
      }
         
      else
      {
         return 164
      }
   }
   
  
   func setCardDetail(name: String, image: UIImage?, number: String) {
      self.lblCardName.text = name
      self.cardImg.image = image
      self.cardNumber.text = number
   }
   
   func setCard() {
      if STPCustomerClass.shared.cardSources.count > 0 {
         if let stpSource = STPCustomerClass.shared.defaultSource {
            let stpBrand = stpSource.cardDetails!.brand
            setCardDetail(name: STPCard.string(from: stpBrand), image: STPImageLibrary.brandImage(for: stpBrand), number: stpSource.cardDetails!.last4!)
         }
      } else {
         self.setCardDetail(name: "Add card", image: nil, number: "")
      }
      setDefaultCard()
   }
   
   func setDefaultCard() {
      self.tblObj.reloadData()
   }
   
   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
   {
      if indexPath.row == 0
      {
         cell1.selectionStyle = .none
         if STPCustomerClass.shared.cardSources.count > 0 {
            if let stpSource = STPCustomerClass.shared.defaultSource {
               let stpBrand = stpSource.cardDetails!.brand
               setCardDetail(name: STPCard.string(from: stpBrand), image: STPImageLibrary.brandImage(for: stpBrand), number: stpSource.cardDetails!.last4!)
            }
         } else {
            self.setCardDetail(name: "Add card", image: nil, number: "")
         }
         return cell1
      }
      else if indexPath.row == 1 {
         cell2.selectionStyle = .none
         
         return cell2
      }
      else if indexPath.row == 2 {
         cell3.selectionStyle = .none
         return cell3
      }
      else
      {
         cell4.selectionStyle = .none
         return cell4
      }
   }
   func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
   }
   @IBAction func btnWalkingPress(sender:UIButton)
   {
      btnWalk.backgroundColor = redColor
      btnWalk.setTitleColor(UIColor.white, for: .normal)
      btnWalk.setImage(#imageLiteral(resourceName: "Walk_white"), for: .normal)
      
      btnDrive.backgroundColor = UIColor.white
      btnDrive.setTitleColor(UIColor.black, for: .normal)
      btnDrive.setImage(#imageLiteral(resourceName: "car_black"), for: .normal)
      transportType = 0
   }
   @IBAction func btnDrivingPress(sender:UIButton)
   {
      btnDrive.backgroundColor = redColor
      btnDrive.setTitleColor(UIColor.white, for: .normal)
      btnDrive.setImage(#imageLiteral(resourceName: "Car_white"), for: .normal)
      
      btnWalk.backgroundColor = UIColor.white
      btnWalk.setTitleColor(UIColor.black, for: .normal)
      btnWalk.setImage(#imageLiteral(resourceName: "walk_black"), for: .normal)
      transportType = 1
   }
   @IBAction func btnLocationPress(sender:UIButton)
   {
      let transition = CATransition()
      transition.duration = 0.5
      transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
      transition.type = kCATransitionMoveIn
      transition.subtype = kCATransitionFromTop
      self.navigationController?.view.layer.add(transition, forKey: nil)
      let selectLocationObj = self.storyboard?.instantiateViewController(withIdentifier: "SelectLocationViewController") as! SelectLocationViewController
      self.navigationController?.pushViewController(selectLocationObj, animated: false)
   }
   @IBAction func btnPaymentPress(sender:UIButton)
   {
      let viewObj = self.storyboard?.instantiateViewController(withIdentifier: "PaymentViewController") as! PaymentViewController
      viewObj.delegate = self
      viewObj.isFromSetting = true
      self.navigationController?.pushViewController(viewObj, animated: true)
   }
   @IBAction func btnLogoutPress(sender:UIButton)
   {
      let alert = UIAlertController.init(title: APP_NAME, message: "Are you sure you want to logout?", preferredStyle: .alert)
      alert.addAction(UIAlertAction.init(title: "Yes", style: UIAlertActionStyle.default, handler: { (action) in
         
         self.logout()
      }))
      alert.addAction(UIAlertAction.init(title: "No", style: .cancel, handler: nil))
      self.present(alert, animated: true, completion: nil)
   }
   
   func logout()
   {
      appDel.showHUD()
      ApiManager.shared.logout(accessToken: "\(UserDefaults.standard.value(forKey: "access_token")!)")
      {
         let domain = Bundle.main.bundleIdentifier!
         UserDefaults.standard.removePersistentDomain(forName: domain)
         UserDefaults.standard.synchronize()
         print(Array(UserDefaults.standard.dictionaryRepresentation().keys).count)
         
         UserDefaults.standard.set(false, forKey: "isLogin")
         UserDefaults.standard.set(true, forKey: "isFirstTime")
         UserDefaults.standard.set(1, forKey: "maxRadious")
         
         UserDefaults.standard.synchronize()
         appDel.hideHUD()

         NotificationCenter.default.post(name: NSNotification.Name(rawValue: "MoveToFirstView"), object: nil)
         self.navigationController?.popToRootViewController(animated: false)
      }
   }
   
   @IBAction func btnBackPress(sender:UIButton)
   {
      self.navigationController?.popViewController(animated: true)
   }
   override func didReceiveMemoryWarning() {
      super.didReceiveMemoryWarning()
   }
}
extension UIViewController {
   
   func showToast(message : String) {
      
      let toastLabel = UILabel(frame: CGRect(x: 20, y: self.view.frame.size.height-100, width: self.view.frame.size.width - 40, height: 35))
      toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
      toastLabel.textColor = UIColor.white
      toastLabel.textAlignment = .center;
      toastLabel.font = UIFont(name: "Avenir Medium", size: 14.0)
      toastLabel.text = message
      toastLabel.alpha = 1.0
      toastLabel.layer.cornerRadius = 10;
      toastLabel.clipsToBounds  =  true
      self.view.addSubview(toastLabel)
      UIView.animate(withDuration: 4.0, delay: 0.3, options: .curveEaseOut, animations: {
         toastLabel.alpha = 0.0
      }, completion: {(isCompleted) in
         toastLabel.removeFromSuperview()
      })
   } }
