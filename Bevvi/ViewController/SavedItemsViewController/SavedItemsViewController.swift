//
//  SavedItemsViewController.swift
//  Bevvi
//
//  Created by Hetal Govani on 24/10/17.
//  Copyright © 2017 solulab. All rights reserved.
//

import UIKit

class SavedItemsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource
{
    @IBOutlet var tblObj : UITableView!

    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.title = "Saved Items"
        self.tabBarItem.title = ""
        tblObj.tableFooterView = UIView()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : savedItemCell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! savedItemCell
        cell.selectionStyle = .none
        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: cell.lblOriginalPrice.text!)
        attributeString.addAttribute(NSAttributedStringKey.strikethroughStyle, value: 1, range: NSMakeRange(0, attributeString.length))
        attributeString.addAttribute(NSAttributedStringKey.strikethroughColor, value: redColor, range: NSMakeRange(0, attributeString.length))
        cell.btnAskCommunity.tag = indexPath.row
        cell.btnAskCommunity.addTarget(self, action: #selector(btnAskCommunity(sender:)), for: .touchUpInside)
        cell.lblOriginalPrice.attributedText = attributeString
        return cell
    }
    @IBAction func btnAskCommunity(sender:UIButton)
    {
        let transition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionMoveIn
        transition.subtype = kCATransitionFromTop
        self.navigationController?.view.layer.add(transition, forKey: nil)
        let viewObj = self.storyboard?.instantiateViewController(withIdentifier: "AskCommunityViewController") as! AskCommunityViewController
        self.navigationController?.pushViewController(viewObj, animated: false)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
class savedItemCell: UITableViewCell {
    @IBOutlet var imgView : UIImageView!
    @IBOutlet var lblStoreName : UILabel!
    @IBOutlet var lblOriginalPrice : UILabel!
    @IBOutlet var btnAskCommunity : UIButton!
}
