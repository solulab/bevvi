struct customError {
    static let HTTPNotFoundError = customError(title: "Page Not Found", message: "There doesn’t seem to be anything here.")
    static let NetworkError = customError(title: "Can’t Connect", message: "Bevvi can’t connect to the server.Please check your internet connection")
    static let UnknownError = customError(title: "Unknown Error", message: "An unknown error occurred.")
    
    let title: String
    let message: String
    
    init(title: String, message: String) {
        self.title = title
        self.message = message
    }
    
    init(HTTPStatusCode: Int) {
        self.title = "Server Error"
        self.message = "The server returned an HTTP \(HTTPStatusCode) response."
    }
}
