//
//  RateUsViewController.swift
//  Bevvi
//
//  Created by Hetal Govani on 02/11/17.
//  Copyright © 2017 solulab. All rights reserved.
//

import UIKit
protocol RateUsDelegate
{
    func SubmitButtonClicked(_ secondDetailViewController: RateUsViewController)
    func DismissButtonClicked(_ secondDetailViewController: RateUsViewController)
}
class RateUsViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, FloatRatingViewDelegate
{
   var delegate: RateUsDelegate?
   @IBOutlet var collectionView : UICollectionView!
   @IBOutlet var btnSubmit  : UIButton!
   @IBOutlet var btnDismiss  : UIButton!
   var dictDetail : JSONDictionary!
   var arrOrderDetail : [JSONDictionary]!

   override func viewDidLoad() {
      super.viewDidLoad()
      print(dictDetail)
      if let arr = dictDetail["orderdetails"] as? Array<Dictionary<String,AnyObject>>
      {
         arrOrderDetail = arr
      }
      self.collectionView.register(rateCell.self, forCellWithReuseIdentifier: "cell")
      self.collectionView.register(UINib(nibName: "rateCell", bundle: nil), forCellWithReuseIdentifier: "cell")
      let rectShape = CAShapeLayer()
      rectShape.bounds = self.btnSubmit.frame
      rectShape.position = self.btnSubmit.center
      rectShape.path = UIBezierPath(roundedRect: self.btnSubmit.bounds, byRoundingCorners: [.bottomRight], cornerRadii: CGSize(width: 10, height:10)).cgPath
      
      //Here I'm masking the textView's layer with rectShape layer
      self.btnSubmit.layer.mask = rectShape
      
      let rectShape1 = CAShapeLayer()
      rectShape1.bounds = self.btnDismiss.frame
      rectShape1.position = self.btnDismiss.center
      rectShape1.path = UIBezierPath(roundedRect: self.btnDismiss.bounds, byRoundingCorners: [.bottomLeft], cornerRadii: CGSize(width: 10, height:10)).cgPath
      
      //Here I'm masking the textView's layer with rectShape layer
      self.btnDismiss.layer.mask = rectShape1
      setupLayout()
   }
   
   fileprivate var pageSize: CGSize
   {
      let layout = self.collectionView.collectionViewLayout as! UPCarouselFlowLayout
      var pageSize = layout.itemSize
      if layout.scrollDirection == .horizontal
      {
         pageSize.width += layout.minimumLineSpacing
      }
      else
      {
         pageSize.height += layout.minimumLineSpacing
      }
      return pageSize
   }
   fileprivate var orientation: UIDeviceOrientation
   {
      return UIDevice.current.orientation
   }
   
   func setupLayout()
   {
      let layout = UPCarouselFlowLayout()
      
      layout.itemSize = CGSize.init(width: 190, height: 250)
      
      layout.scrollDirection = .horizontal
      self.collectionView.collectionViewLayout = layout
      
      layout.spacingMode = UPCarouselFlowLayoutSpacingMode.overlap(visibleOffset: 30)
   }
   
   func collectionView(_ collectionView: UICollectionView,viewForSupplementaryElementOfKind kind: String,at indexPath: IndexPath) -> UICollectionReusableView
   {
      let headerView: CarouselCollectionViewCell  = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier:"cell", for: indexPath) as! CarouselCollectionViewCell
      return headerView
   }
   func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
   {
      return arrOrderDetail.count
   }
   
   func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
      let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! rateCell
      let dictProduct = arrOrderDetail[indexPath.row] as JSONDictionary
      let dictProductDetail = dictProduct["product"] as! JSONDictionary
      
      cell.lblName.text = "\(dictProductDetail["name"]!)"
      cell.lblCategory.text = "\(dictProductDetail["category"]!)"
      
      cell.rateView.delegate = self
      cell.rateView.tag = indexPath.row
      return cell
   }
   func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
   {
      print(indexPath.row)
   }

   func floatRatingView(_ ratingView: FloatRatingView, didUpdate rating: Float)
   {
      print("rating updated call..\(ratingView.tag)")
      let dictProduct = arrOrderDetail[ratingView.tag] as JSONDictionary
      
        let dictMain = ["productId":"\(dictProduct["id"]!)","accountId":"\(UserDefaults.standard.value(forKey: "accountId")!)","rating":ratingView.rating] as Dictionary<String,AnyObject>
      if let theJSONData = try?  JSONSerialization.data(withJSONObject: dictMain,options: .prettyPrinted),
         let theJSONText = String(data: theJSONData,encoding: String.Encoding.ascii)
      {
         print("JSON string = \n\(theJSONText)")
         appDel.showHUD()
         let listing = StoreListing()
         listing.jsonArray = theJSONText
         ApiManager.shared.postRating(listing: listing) { (dict:JSONDictionary) in
            
            print(dict)
            appDel.hideHUD()
         }
      }
      
   }
   func postRating()
   {
      
   }
   @IBAction func btnSubmitPress(sender:UIButton)
   {
      delegate?.SubmitButtonClicked(self)
   }
   @IBAction func btnDismissPress(sender:UIButton)
   {
      delegate?.DismissButtonClicked(self)
   }
   override func didReceiveMemoryWarning() {
      super.didReceiveMemoryWarning()
   }
}
class rateCell: UICollectionViewCell {
    @IBOutlet var imgView : UIImageView!
    @IBOutlet var rateView : FloatRatingView!
   @IBOutlet var lblName : UILabel!
   @IBOutlet var lblCategory : UILabel!

}
