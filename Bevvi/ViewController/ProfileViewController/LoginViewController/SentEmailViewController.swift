//
//  SentEmailViewController.swift
//  Bevvi
//
//  Created by Hetal Govani on 08/11/17.
//  Copyright © 2017 solulab. All rights reserved.
//

import UIKit

class SentEmailViewController: UIViewController
{
   @IBOutlet var viewHeight : NSLayoutConstraint!

    override func viewDidLoad()
    {
        super.viewDidLoad()
        viewHeight.constant = self.view.frame.height - 114
    }
    
    @IBAction func btnLogInPress(sender:UIButton)
    {
        for controller in self.navigationController!.viewControllers as Array
        {
            if controller.isKind(of: LoginViewController.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
