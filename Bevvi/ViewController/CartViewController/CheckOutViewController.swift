//
//  CheckOutViewController.swift
//  Bevvi
//
//  Created by Hetal Govani on 31/10/17.
//  Copyright © 2017 solulab. All rights reserved.
//

import UIKit
import MapKit
import Stripe

class CheckOutViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,MKMapViewDelegate,CLLocationManagerDelegate,STSelectedCardDelegate {
   func setDefaultCard() {
      
   }
   
   @IBOutlet var tblObj : UITableView!
    @IBOutlet var tblHeight : NSLayoutConstraint!
    @IBOutlet var datePickerHeight : NSLayoutConstraint!
    @IBOutlet weak var mapView: MKMapView!
   private var locationManager: CLLocationManager!

    @IBOutlet var viewHeight : NSLayoutConstraint!
    @IBOutlet var datePicker : UIDatePicker!
   var cards = [STCard]()
   var selectedSource: STPSource?
   var arrOriginalPrice = [CGFloat]()
   var arrsalePrice = [CGFloat]()
   var arrQuantity = [Int]()
   @IBOutlet var lblCardName: UILabel!
   @IBOutlet var imgCard: UIImageView!
   @IBOutlet var lblCardNumber: UILabel!
   
   @IBOutlet var lblStoreName : UILabel!
   @IBOutlet var lblAddress : UILabel!
   @IBOutlet var lblDistance : UILabel!
   @IBOutlet var btnPurchase : UIButton!

   var totalPrice : CGFloat! = 0
  var arrList : Array<Dictionary<String,AnyObject>>! = Array()
   lazy var errorView: ErrorView = {
      let Errview = UINib(nibName: "ErrorView", bundle: nil).instantiate(withOwner: self, options: nil)[0] as! ErrorView
      Errview.translatesAutoresizingMaskIntoConstraints = false
      Errview.retryButton.addTarget(self, action: #selector(btnRetryPress(sender:)), for: .touchUpInside)
      return Errview
   }()
   func presentError(_ error: customError) {
      self.errorView.error = error
      
      DispatchQueue.main.async {
         self.view.addSubview(self.errorView)
         self.installErrorViewConstraints()
      }
   }
   
   func installErrorViewConstraints() {
      self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[view]|", options: [], metrics: nil, views: [ "view": self.errorView ]))
      self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[view]|", options: [], metrics: nil, views: [ "view": self.errorView ]))
   }
   @IBAction func btnRetryPress(sender:UIButton)
   {
      errorView.removeFromSuperview()
      if appDel.reachability.connection != .none
      {
         retriveCart()
      }
      else
      {
         self.presentError(.NetworkError)
      }
   }
    override func viewDidLoad()
    {
      super.viewDidLoad()
      
      self.title = "Check out"
      
      let button1 = UIButton(type: UIButtonType.custom)
      button1.setImage(#imageLiteral(resourceName: "Back"), for: UIControlState.normal)
      button1.addTarget(self, action:#selector(btnBackPress(sender:)), for: UIControlEvents.touchUpInside)
      button1.frame=CGRect.init(x: 0, y: 0, width: 30, height: 30)
      let barButton1 = UIBarButtonItem(customView: button1)
      self.navigationItem.leftBarButtonItem = barButton1
      
      tblObj.layer.borderWidth = 0.5
      tblObj.layer.borderColor = UIColor.gray.withAlphaComponent(0.8).cgColor
      
      datePicker.layer.borderWidth = 0.5
      datePicker.layer.borderColor = UIColor.gray.withAlphaComponent(0.8).cgColor
      datePicker.backgroundColor = UIColor.white
      
      mapView.delegate = self
      
      locationManager = CLLocationManager()
      locationManager.delegate = self
      locationManager.desiredAccuracy = kCLLocationAccuracyBest
      self.getDefaultCard()
    }
   
   func getDefaultCard() {
      
      if STPCustomerClass.shared.cardSources.count > 0 {
         if let stpSource = STPCustomerClass.shared.defaultSource {
            let stpBrand = stpSource.cardDetails!.brand
            setCardDetail(name: STPCard.string(from: stpBrand), image: STPImageLibrary.brandImage(for: stpBrand), number: stpSource.cardDetails!.last4!)
            self.selectedSource = stpSource
         }
         
      } else {
         self.setCardDetail(name: "Add card", image: nil, number: "")
      }
      
      
//      appDel.showHUD()
//      STPCardApiManager.shared.getAllCardDetails(parameter: ["customerId": MyAPIClient.sharedClient.customerID as AnyObject]) { (response: CustomerData?, error:Error?) in
//         if error == nil {
//            if let customerDataObjArr = response?.sources?.data {
//               if customerDataObjArr.count > 0 {
//                  self.cards = customerDataObjArr
//                  self.selectedCard = self.cards[0]
//                  self.setCardDetail(name: self.selectedCard!.brand!, image: UIImage.init(named: "stp_card_\(self.selectedCard!.brand!.lowercased())"), number: self.selectedCard!.last4!)
//               } else {
//                  self.setCardDetail(name: "Add card", image: nil, number: "")
//               }
//            }
//         }
//         appDel.hideHUD()
//      }
   }
   
   func setCardDetail(name: String, image: UIImage?, number: String) {
      
      self.lblCardName.text = name
      self.imgCard.image = image
      self.lblCardNumber.text = number
      
   }
      
   func setCard() {
//      self.selectedCard = card
//      setCardDetail(name: card.brand!, image: UIImage.init(named: "stp_card_\(self.selectedCard!.brand!.lowercased())"), number: card.last4!)
      if STPCustomerClass.shared.cardSources.count > 0 {
         
         if let stpSource = STPCustomerClass.shared.selectedSource {
            let stpBrand = stpSource.cardDetails!.brand
            setCardDetail(name: STPCard.string(from: stpBrand), image: STPImageLibrary.brandImage(for: stpBrand), number: stpSource.cardDetails!.last4!)
            self.selectedSource = STPCustomerClass.shared.selectedSource!
         } else {
            self.setCardDetail(name: "Add card", image: nil, number: "")
         }
         
      }
   }
   
   
   func setMapView(lat:NSNumber,long:NSNumber)
   {
         let shopLatitude = (lat).doubleValue
         let shopLongitude = (long).doubleValue
      
//      let shopLatitude = 23.0225
//      let shopLongitude = 72.5714
      
      let anno = Annonation.init(coordinate: CLLocationCoordinate2D(latitude: shopLatitude as CLLocationDegrees, longitude: shopLongitude as CLLocationDegrees))
      
      self.mapView.addAnnotation(anno)
      
      let location = CLLocation.init(latitude: shopLatitude as CLLocationDegrees, longitude: shopLongitude as CLLocationDegrees)
      
      let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
      let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate, 1000, 1000)
      mapView.setRegion(coordinateRegion, animated: true)
      print(shopLatitude)
      print(shopLongitude)
      
//      CommonMethods.shared.getAdress(lat: arrCoord[0] as NSNumber, long: arrCoord[1] as NSNumber) { (str:String, err:Error?) in
//         if err != nil
//         {
//            self.lblAddress.text = str
//         }
//      }
   }
   func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
      let identifier = "pin"
      
      if annotation.isKind(of: MKUserLocation.classForCoder()) {
         return nil
      }
      var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
      if  annotationView == nil {
         annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: "pin")
         annotationView!.canShowCallout = true
         annotationView!.image = #imageLiteral(resourceName: "location")
      }
      else {
         annotationView!.annotation = annotation
      }
      return annotationView
      
   }
    override func viewWillAppear(_ animated: Bool)
    {
      super.viewWillAppear(true)
      if appDel.reachability.connection != .none
      {
         retriveCart()
      }
      else
      {
         self.presentError(.NetworkError)
      }
    }
   
   func changeTableSize()
   {
      tblObj.layer.removeAllAnimations()
      tblHeight.constant = CGFloat(((arrList.count + 4) * 40) + 120)
      UIView.animate(withDuration: 0.5)
      {
         self.tblObj.layoutIfNeeded()
         self.updateViewConstraints()
      }
   }
   
   override func updateViewConstraints()
   {
      super.updateViewConstraints()
      self.viewHeight.constant = self.tblHeight.constant + self.datePickerHeight.constant + 400
   }
   func retriveCart()
   {
      totalPrice = 0
      arrOriginalPrice = Array()
      arrsalePrice = Array()
      //http://18.221.89.220:3000/api/offers?filter={"where":{"establishmentId":"5a24a29410785a5c1aa56cde"}, "include":["product"]}
      let para:NSMutableDictionary = NSMutableDictionary()
      
      let geoLocation:NSDictionary = NSDictionary(object: "\(UserDefaults.standard.value(forKey: "accountId")!)", forKey: "accountId" as NSCopying)
      
      //      ["product",{"offer":["establishment"]}]
      let muray_latlong : Array<String> = ["establishment"]
      let dictOffer:NSMutableDictionary = NSMutableDictionary()
      dictOffer.setObject(muray_latlong, forKey: "offer" as NSCopying)
      
      let includeDict : Array<AnyObject> = ["product" as AnyObject,dictOffer as AnyObject]
      //      let includeDict:NSMutableArray = ["product","offer"]
      para.setObject(geoLocation, forKey: "where" as NSCopying)
      para.setObject(includeDict, forKey: "include" as NSCopying)
      
      if let theJSONData = try?  JSONSerialization.data(
         withJSONObject: para,
         options: .prettyPrinted
         ),
         let theJSONText = String(data: theJSONData,
                                  encoding: String.Encoding.ascii)
      {
         print("JSON string = \n\(theJSONText)")
         
         let storeDetail = StoreDetails()
         storeDetail.jsonArray = theJSONText
         appDel.showHUD()
         ApiManager.shared.retriveCart(listing: storeDetail, view: self.view, completion: { (arr:[JSONDictionary]) in
            
            print(arr)
            self.arrList = arr
            for dict in self.arrList
            {
               let dictOffer = dict["offer"] as! Dictionary<String,AnyObject>
               let salePrice = "\(dictOffer["salePrice"]!)"
               let originalPrice = "\(dictOffer["originalPrice"]!)"
               let quantity = "\(dict["quantity"]!)"
               
               self.arrsalePrice.append(CGFloat(NumberFormatter().number(from:salePrice)!))
               self.arrOriginalPrice.append(CGFloat(NumberFormatter().number(from:originalPrice)!))
               self.arrQuantity.append(Int(quantity)!)
            }
           
            let dict = self.arrList[0]["offer"] as! Dictionary<String,AnyObject>
            let dictEstablishment = dict["establishment"] as! Dictionary<String,AnyObject>
            self.lblStoreName.text = "\(dictEstablishment["name"]!)"
            self.lblAddress.text = "\(dictEstablishment["address"]!)"
            let dictLocation = dictEstablishment["geoLocation"]! as! Dictionary<String,AnyObject>
            let arrCoord = dictLocation["coordinates"] as! Array<NSNumber>
            
            self.setMapView(lat: arrCoord[1], long: arrCoord[0])
         
            self.tblObj.reloadData()
            appDel.hideHUD()
            
            DispatchQueue.main.async
               {
                  self.changeTableSize()
                  self.btnPurchase.setTitle(String(format: "%.2f PURCHASE", self.totalPrice!), for: .normal)
            }
         })
      }
   }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        if section == 0
        {
            return 40
        }
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?
    {
        if section == 0
        {
            return "ITEMS"
        }
        return nil
    }
    
   func numberOfSections(in tableView: UITableView) -> Int
   {
      return 2
   }
   
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
   {
      if section == 0
      {
         return arrList.count
      }
      else
      {
         return 4
      }
   }
   
   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      if indexPath.section == 0 {
         let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! itemCell
         cell.selectionStyle = .none
         let dictProduct = arrList[indexPath.row]["product"] as! Dictionary<String,AnyObject>
         cell.lblTitle.text = "\(dictProduct["name"]!)"
         cell.lblDesc.text = "\(dictProduct["varietal"]!)"
         cell.lblQuantity.text = "\(arrList[indexPath.row]["quantity"]!)"
         
         let dictOffer = arrList[indexPath.row]["offer"] as! Dictionary<String,AnyObject>
         
         let price = CGFloat(truncating: NumberFormatter().number(from: "\(dictOffer["salePrice"]!)")!) * CGFloat(NumberFormatter().number(from: "\(arrList[indexPath.row]["quantity"]!)")!)
         
         cell.lblPrice.text = "$\(price)"
         
         return cell
      }
      else
      {
         let cell = tableView.dequeueReusableCell(withIdentifier: "cellFinal", for: indexPath) as! finalInfoCell
         if indexPath.row == 0
         {
            cell.lblTitle.text = "Tax"
            
            cell.lblPrice.textColor = UIColor.init(red: 141/255, green: 141/255, blue: 141/255, alpha: 1)
            cell.lblPrice.text = "5%"
         }
         else if indexPath.row == 1
         {
            cell.lblTitle.text = "Rewards"
            var savedPrice : CGFloat = 0
            for i in 0 ..< arrOriginalPrice.count
            {
               let diff = (arrOriginalPrice[i] - arrsalePrice[i]) * CGFloat(arrQuantity[i])
               savedPrice = savedPrice + diff
            }
            cell.lblPrice.textColor = UIColor.init(red: 141/255, green: 141/255, blue: 141/255, alpha: 1)
            cell.lblPrice.text = "-$\(savedPrice)"
         }
         else if indexPath.row == 2
         {
            cell.lblTitle.text = "TOTAL"
            cell.lblTitle.textColor = UIColor.black
//            var totalPrice : CGFloat = 0
            for i in 0 ..< arrOriginalPrice.count
            {
               totalPrice = totalPrice! + (arrsalePrice[i] * CGFloat(arrQuantity[i]))
            }
            totalPrice = totalPrice! + (totalPrice! * 5) / 100

            cell.lblPrice.textColor = UIColor.black
            cell.lblPrice.text = "$\(totalPrice!)"
            
         }
         else if indexPath.row == 3
         {
            cell.lblTitle.text = "Points earned"
            cell.lblTitle.font = UIFont.init(name: "Avenir-Roman", size: 13)
            cell.lblTitle.textColor = redColor
            
            cell.lblPrice.font = UIFont.init(name: "Avenir-Roman", size: 13)
            cell.lblPrice.textColor = redColor
            cell.lblPrice.text = "0 pts"
         }
         cell.selectionStyle = .none
         return cell
      }
   }
 
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(true)
    }
    @IBAction func btnPurchasePress(sender:UIButton)
    {
      
      if let stpSource = self.selectedSource {
         var paymentDict = JSONDictionary()
         let newString = String(format: "%.2f", self.totalPrice!).replacingOccurrences(of: ".", with: "", options: .literal, range: nil)
         paymentDict["amount"] = newString as AnyObject
         paymentDict["customerId"] = MyAPIClient.sharedClient.customerID as AnyObject
         paymentDict["source"] = stpSource.stripeID as AnyObject
         paymentDict["destination_account"] = "acct_1Bj4CtD0iaqbIDoG" as AnyObject
         paymentDict["customerId"] = "\(UserDefaults.standard.value(forKey: "payment_token")!)" as AnyObject
         paymentDict["pickupDateTime"] = "2018-02-04 15:24:00" as AnyObject
         paymentDict["accountId"] = "\(UserDefaults.standard.value(forKey: "accountId")!)" as AnyObject
         paymentDict["accessToken"] = "\(UserDefaults.standard.value(forKey: "access_token")!)" as AnyObject
         
         let dict = self.arrList[0]["offer"] as! Dictionary<String,AnyObject>
         let dictEstablishment = dict["establishment"] as! Dictionary<String,AnyObject>
         
         paymentDict["establishmentId"] = "\(dictEstablishment["id"]!)" as AnyObject

         print(paymentDict)
         appDel.showHUD()
         STPCardApiManager.shared.makePayment(parameter: paymentDict) { (paymentResponse, error) in
            
            if error == nil
            {
               self.EmptyCart(dictDetail: paymentResponse![0])
            }
            appDel.hideHUD()
         }
      } else {
         UtilityClass.showAlert("Please add or select card")
         appDel.hideHUD()

      }
   }
   func EmptyCart(dictDetail : JSONDictionary) {
      let para:NSMutableDictionary = NSMutableDictionary()
      para.setObject(1, forKey: "state" as NSCopying)
      if let theJSONData = try?  JSONSerialization.data(
         withJSONObject: para,
         options: .prettyPrinted
         ),
         let theJSONText = String(data: theJSONData,
                                  encoding: String.Encoding.ascii) {
         print("JSON string = \n\(theJSONText)")
         
         let storeListing = StoreListing()
         storeListing.jsonArray = theJSONText
         appDel.showHUD()
         ApiManager.shared.emptyCart(accountId: "\(UserDefaults.standard.value(forKey: "accountId")!)", listing: storeListing, view: self.view, completion: { (dict:JSONDictionary) in
            print(dict)
            let orderView = self.storyboard?.instantiateViewController(withIdentifier: "OrderSummaryViewController") as! OrderSummaryViewController
            orderView.isPickup = true
            orderView.isFromCheckOut = true
            orderView.dictDetail = dictDetail
            self.navigationController?.pushViewController(orderView, animated: true)
            appDel.hideHUD()
         })
      }
   }
   @IBAction func btnPaymentPress(sender:UIButton)
   {
      let paymentView = self.storyboard?.instantiateViewController(withIdentifier: "PaymentViewController") as! PaymentViewController
      paymentView.delegate = self
      self.navigationController?.pushViewController(paymentView, animated: true)
   }
    @IBAction func btnBackPress(sender:UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
