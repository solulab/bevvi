//
//  STPCardApiManager.swift
//  Bevvi
//
//  Created by Ankit on 1/10/18.
//  Copyright © 2018 solulab. All rights reserved.
//

import Foundation
import Stripe
import Alamofire

public class STPCardApiManager
{
   
   static let shared: STPCardApiManager = STPCardApiManager()
   
   struct BaseURL {
      static let baseurl = "http://18.221.89.220:3038/"
      static let createCustomer = "\(baseurl)create-customer"
      static let listcustomer = "\(baseurl)list-customer"
      static let creteCharge = "\(baseurl)create_charge"
      static let updateCustomer = "\(baseurl)update_customer"
      static let retrieveCustomer = "\(baseurl)retrieve_customer"
      static let deleteCard = "\(baseurl)delete_card"
   }
   
   func addAuth() -> [String: String] {
      return ["Authorization":"Bearer sk_test_m836fOTfRKOxMXlQ4MafZ0pa"]
   }
   
   func addCardtoCustomer(parameter: JSONDictionary,completion:@escaping(_ dict:JSONDictionary?,_ error:Error?) -> ()) {
      //\(baseurl)/customers/cus_C6qVhSyvI54T81/sources
      Alamofire.request("", method: .post, parameters: parameter, encoding: JSONEncoding.default, headers: addAuth()).responseJSON { (responseJSON) in
         
         switch responseJSON.result {
         case .success(let json):
            completion(json as? JSONDictionary, nil)
         case .failure(let error):
            completion(nil, error)
         }
      }
   }
   
   func createCustomer(parameter: JSONDictionary, completion:@escaping(_ dict:JSONDictionary?,_ error:Error?) -> ()) {
      Alamofire.request(BaseURL.createCustomer, method: .post, parameters: parameter).responseJSON { (responseJSON) in
         
         switch responseJSON.result {
         case .success(let json):
            print("Customer Detail - \(json as? JSONDictionary)")
            completion(json as? JSONDictionary, nil)
         case .failure(let error):
            completion(nil, error)
         }
      }
   }
   
   func getAllCardDetails(parameter: JSONDictionary, completion:@escaping(_ customer:CustomerData?,_ error:Error?) -> ()) {
      
      Alamofire.request(BaseURL.retrieveCustomer, method: .post, parameters: parameter).responseJSON { (responseJSON) in
         switch responseJSON.result {
         case .success(let json):
            if let jsonObj = json as? JSONDictionary {
               let customerObj = CustomerData()
               customerObj.toParseDict(dict: jsonObj)
               completion(customerObj, nil)
            }
         case .failure(let error):
            completion(nil, error)
         }
      }
   }
   
   func makePayment(parameter: JSONDictionary, completion:@escaping(_ customer:[JSONDictionary]?,_ error:Error?) -> ()) {
      Alamofire.request(BaseURL.creteCharge, method: .post, parameters: parameter).responseJSON { (responseJSON) in
         switch responseJSON.result {
         case .success(let json):
            print(json)

            if let jsonObj = json as? [JSONDictionary] {
               completion(jsonObj, nil)
            }
         case .failure(let error):
            print(error.localizedDescription)
            completion(nil, error)
         }
      }
   }
   
   func setDefaultCard(parameter: JSONDictionary, completion:@escaping(_ customer:CustomerData?,_ error:Error?) -> ()) {
      Alamofire.request(BaseURL.updateCustomer, method: .post, parameters: parameter).responseJSON { (responseJSON) in
         switch responseJSON.result {
         case .success(let json):
            if let jsonObj = json as? JSONDictionary {
               let customerObj = CustomerData()
               customerObj.toParseDict(dict: jsonObj)
               completion(customerObj, nil)
            }
         case .failure(let error):
            completion(nil, error)
         }
      }
   }
   
   func deleteCard(parameter: JSONDictionary, completion:@escaping(_ customer:CustomerData?,_ error:Error?) -> ()) {
      Alamofire.request(BaseURL.deleteCard, method: .post, parameters: parameter).responseJSON { (responseJSON) in
         switch responseJSON.result {
            
         case .success(let json):
            
            print(json)
            if let jsonObj = json as? JSONDictionary {
               
               if let delete = jsonObj["deleted"] as? Bool
               {
                  if delete == true
                  {
                     completion(nil, nil)
                  }
               }
               else
               {
                  appDel.hideHUD()
               }
            }
         case .failure(let error):
            completion(nil, error)
         }
      }
   }
   
}

class MyAPIClient: NSObject, STPEphemeralKeyProvider {
   
   static let sharedClient = MyAPIClient()
   var baseURLString: String? = "http://18.221.89.220:3038"
   var customerID = "\(UserDefaults.standard.value(forKey: "payment_token")!)"
   var baseURL: URL {
      if let urlString = self.baseURLString, let url = URL(string: urlString) {
         return url
      } else {
         fatalError()
      }
   }
   
   func completeCharge(_ result: STPPaymentResult,
                       amount: Int,
                       shippingAddress: STPAddress?,
                       shippingMethod: PKShippingMethod?,
                       completion: @escaping STPErrorBlock) {
      let url = self.baseURL.appendingPathComponent("charge")
      var params: [String: Any] = [
         "source": result.source.stripeID,
         "amount": amount
      ]
      params["shipping"] = STPAddress.shippingInfoForCharge(with: shippingAddress, shippingMethod: shippingMethod)
      Alamofire.request(url, method: .post, parameters: params)
         .validate(statusCode: 200..<300)
         .responseString { response in
            switch response.result {
            case .success:
               completion(nil)
            case .failure(let error):
               completion(error)
            }
      }
   }
   
   func createCustomerKey(withAPIVersion apiVersion: String, completion: @escaping STPJSONResponseCompletionBlock) {
      let url = self.baseURL.appendingPathComponent("ephemeral_keys")
      Alamofire.request(url, method: .post, parameters: [
         "api_version": apiVersion,
         "customerId": self.customerID
         ])
         .validate(statusCode: 200..<300)
         .responseJSON { responseJSON in
            switch responseJSON.result {
            case .success(let json):
               STPCustomerClass.shared.getCustomer(completion: { (error) in
               })
               completion(json as? [String: AnyObject], nil)
            case .failure(let error):
               completion(nil, error)
            }
      }
   }
   
   
   
}

