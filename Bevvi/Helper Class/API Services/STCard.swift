//
//  STCard.swift
//  Bevvi
//
//  Created by Ankit on 1/11/18.
//  Copyright © 2018 solulab. All rights reserved.
//

import Foundation

class STCard {
   
   var id: String?
   var object: String?
   var address_city: String?
   var address_country: String?
   var address_line1: String?
   var address_line1_check: String?
   var address_line2: String?
   var address_state: String?
   var address_zip: String?
   var address_zip_check: String?
   var brand: String?
   var country: String?
   var customer: String?
   var cvc_check: String?
   var dynamic_last4: String?
   var exp_month: NSNumber?
   var exp_year: NSNumber?
   var fingerprint: String?
   var funding: String?
   var last4: String?
   var metadata: JSONDictionary?
   var name: String?
   var tokenization_method: String?
   
   
   func toParseDict(dict: JSONDictionary) {
      
      print(dict)
      
      if let object = dict["object"] as? String {
         
         if object == "source" {
            
         } else {
            
         }
         
         self.object = object
      }
      
      if let id = dict["id"] as? String {
         self.id = id
      }
      
      if let address_city = dict["address_city"] as? String {
         self.address_city = address_city
      }
      
      if let address_country = dict["address_country"] as? String {
         self.address_country = address_country
      }
      
      if let address_line1 = dict["address_line1"] as? String {
         self.address_line1 = address_line1
      }
      
      if let address_line1_check = dict["address_line1_check"] as? String {
         self.address_line1_check = address_line1_check
      }
      
      if let address_line2 = dict["address_line2"] as? String {
         self.address_line2 = address_line2
      }
      
      if let address_state = dict["address_state"] as? String {
         self.address_state = address_state
      }
      
      if let address_zip = dict["address_zip"] as? String {
         self.address_zip = address_zip
      }
      
      if let address_zip_check = dict["address_zip_check"] as? String {
         self.address_zip_check = address_zip_check
      }
      
      if let brand = dict["brand"] as? String {
         self.brand = brand
      }
      
      if let country = dict["country"] as? String {
         self.country = country
      }
      
      if let customer = dict["customer"] as? String {
         self.customer = customer
      }
      
      if let cvc_check = dict["cvc_check"] as? String {
         self.cvc_check = cvc_check
      }
      
      if let dynamic_last4 = dict["dynamic_last4"] as? String {
         self.dynamic_last4 = dynamic_last4
      }
      
      if let dynamic_last4 = dict["dynamic_last4"] as? String {
         self.dynamic_last4 = dynamic_last4
      }
      
      if let exp_month = dict["exp_month"] as? NSNumber {
         self.exp_month = exp_month
      }
      
      if let exp_year = dict["exp_year"] as? NSNumber {
         self.exp_year = exp_year
      }
      
      if let fingerprint = dict["fingerprint"] as? String {
         self.fingerprint = fingerprint
      }
      
      if let funding = dict["funding"] as? String {
         self.funding = funding
      }
      
      if let last4 = dict["last4"] as? String {
         self.last4 = last4
      }
      
      if let metadata = dict["metadata"] as? JSONDictionary {
         self.metadata = metadata
      }
      
      if let name = dict["name"] as? String {
         self.name = name
      }
      
      if let tokenization_method = dict["tokenization_method"] as? String {
         self.tokenization_method = tokenization_method
      }

   }
}
