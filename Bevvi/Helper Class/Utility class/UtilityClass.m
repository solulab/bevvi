//
//  UtilityClass.m
//  bidBrokers
//
//  Created by Ankit on 09/04/15.
//  Copyright (c) 2015 Pooja. All rights reserved.
//

#import "UtilityClass.h"
#import "checkDevice.h"
#import "Constant.h"
//#import "SocialLoginClass.h"

@implementation UtilityClass


+ (UIColor *) colorWithHexString:(float)r andGreen : (float)g andBlue:(float)b
{
    return [UIColor colorWithRed:((float) r / 255.0f)
                       green:((float) g / 255.0f)
                        blue:((float) b / 255.0f)
                       alpha:1.0f];
}

+(BOOL)Emailvalidate:(NSString *)tempMail;
{
    BOOL stricterFilter = YES;
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:tempMail];
}

// this is implemented For remove space in String

+(BOOL)removeSpace:(NSString *)str {

    if ([str isEqualToString:@" "]) {
        return NO;
    } else {
        return YES;
    }
}

+(BOOL)numericValueForTextBox:(NSString *)string
{
    NSCharacterSet *numbersOnly = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
    NSCharacterSet *characterSetFromTextField = [NSCharacterSet characterSetWithCharactersInString:string];
    
    BOOL stringIsValid = [numbersOnly isSupersetOfSet:characterSetFromTextField];
    return stringIsValid;
}
//ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz

+(BOOL)alphabaticValueForTextBox:(NSString *)string
{
    NSCharacterSet *alphabatsOnly = [NSCharacterSet characterSetWithCharactersInString:@"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz "];
    NSCharacterSet *characterSetFromTextField = [NSCharacterSet characterSetWithCharactersInString:string];
    
    BOOL stringIsValid = [alphabatsOnly isSupersetOfSet:characterSetFromTextField];
    return stringIsValid;
}

+(BOOL)textFieldTextwithLimit:(UITextField *)textField andRange:(NSRange)range andString:(NSString *)string withCharacterLimit:(int)limit {
    
    if(range.length + range.location > textField.text.length)
    {
        return NO;
    }
    
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    return newLength <= limit;
    
}

+(void)showAlert:(NSString*)message
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@APP_NAME message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    
    [[[[UIApplication sharedApplication] keyWindow] rootViewController] presentViewController:alertController animated:YES completion:^{
    }];
//    [[[UIAlertView alloc]initWithTitle:@"MedsyncRX" message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil]show];
}

+(void)setMyScrollView:(UIScrollView *)scrollObj viewController:(UIViewController *)view
{
    if(IS_IPHONE_4)
    {
        scrollObj.frame  = CGRectMake(0, 0, 320, 480);
        scrollObj.contentSize = CGSizeMake(320, 568);
    }
    else {
     view.automaticallyAdjustsScrollViewInsets = false;   
    }
}

+(void)setMyAlreadyBtn:(UIButton *)btn
{
    if(IS_IPHONE_4)
    {
        btn.frame = CGRectMake(96, 480-30-6, 135, 30);
    }
}
// set button color and border color

+(void)myBtnSet:(UIView *)myView
{
    for (UIButton *btn in myView.subviews) {
        
        if ([btn isKindOfClass:[UIButton class]])
        {
            [btn setBackgroundColor:[UIColor clearColor]];
            //btn.layer.borderColor = [[self colorWithHexString:89 andGreen:167 andBlue:242]CGColor];
            btn.layer.borderColor = [[UIColor blackColor]CGColor];
            btn.layer.borderWidth = 1;
            btn.clipsToBounds = YES;
        }
    }
}
// Place Holder and change border color
//gray

+(void)setBorderAndPlaceHolder:(UITextField *)txtfld
{
   // txtfld.layer.borderColor = [[self colorWithHexString:228 andGreen:226 andBlue:234]CGColor];
    
//    txtfld.borderStyle = UITextBorderStyleNone;
//    txtfld.layer.borderWidth = 0;
//    txtfld.layer.cornerRadius = 5;
    [txtfld setValue:[self colorWithHexString:51 andGreen:51 andBlue:51]
                    forKeyPath:@"_placeholderLabel.textColor"];


}

// Place Holder and change border color
//Blue

+(void)setBorderAndPlaceHolderForBlue:(UITextField *)txtfld
{
   // txtfld.layer.borderColor = [[self colorWithHexString:89 andGreen:167 andBlue:242]CGColor];
   // txtfld.layer.borderWidth = 0;

   txtfld.borderStyle = UITextBorderStyleNone;
   txtfld.layer.cornerRadius = 0;
   [txtfld setValue:[self colorWithHexString:166 andGreen:163 andBlue:164]
          forKeyPath:@"_placeholderLabel.textColor"];
}

+(void)setMyCustomAnimation:(UIView *)myView rect:(CGRect)rect
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.2];
    myView.frame = rect;
    [UIView commitAnimations];
}

+(void)setMyViewBorder:(UIView *)myView withBorder:(float)width radius:(float)radius
{
    myView.layer.borderColor = [[self colorWithHexString:89 andGreen:167 andBlue:242]CGColor];
    myView.layer.borderWidth = width;
    myView.layer.cornerRadius = radius;
    myView.clipsToBounds = YES;
}

+(void)setMyViewBorder:(UIView *)myView withBorder:(float)width radius:(float)radius color:(UIColor *)color
{
    myView.layer.borderColor = [color CGColor];
    myView.layer.borderWidth = width;
    myView.layer.cornerRadius = radius;
    myView.layer.shadowOffset = CGSizeZero;
    myView.layer.shadowRadius = 2.0f;
    myView.layer.shadowOpacity = 0.35f;
    
    myView.clipsToBounds = NO;
}
+(void)setShedowInView:(UIView *)myView
{
    myView.layer.masksToBounds = NO;
    myView.layer.shadowOffset = CGSizeMake(0, 0);
    myView.layer.shadowRadius = 2;
    myView.layer.shadowOpacity = 0.5;
}

+(void)setMyRotationhide:(UIView *)myView
{
    [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionAllowUserInteraction
                     animations:^{
                         myView.transform =
                         CGAffineTransformScale(myView.transform, 0.01, 0.01);
                     }
                     completion:^(BOOL finished) {
                         myView.hidden = YES;
                     }];
}

+(void)setMyRotationshow:(UIView *)myView
{
    myView.hidden = NO;
    [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionAllowUserInteraction
                     animations:^{
                         myView.transform = CGAffineTransformIdentity;
                         
                     }
                     completion:^(BOOL finished) {
                       // myView.hidden = YES;
                     }];
}

//87 160 238

+(void)setBtnBackGround:(UIButton *)btn
{
    [btn setBackgroundColor:[UIColor colorWithRed:87.0/255.0 green:160.0/255.0 blue:238.0/255.0 alpha:1]];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
}
+(void)setOtherBtnBackGround:(UIButton *)btn
{
    [btn setBackgroundColor:[UIColor whiteColor]];
    [btn setTitleColor:[UIColor colorWithRed:87.0/255.0 green:160.0/255.0 blue:238.0/255.0 alpha:1] forState:UIControlStateNormal];
}

/*
+(void)postDataWithUrl:(NSString *)url image:(NSData *)image1 image2Data:(NSData *)image2 image3Data:(NSData *)image3 withDict:(NSDictionary *)dict sucessBlock:(void (^)(id responseObject))sucess failureBlock:(void (^)(NSError *error))fail
{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
   
    [manager POST:url parameters:dict constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
        [formData appendPartWithFileData:image1
                                    name:@"image1"
                                fileName:@"image1.jpg"
                                mimeType:@"image/jpeg"];
        
        [formData appendPartWithFileData:image2
                                    name:@"image2"
                                fileName:@"image2.jpg"
                                mimeType:@"image/jpeg"];
        
        [formData appendPartWithFileData:image3
                                    name:@"image3"
                                fileName:@"image3.jpg"
                                mimeType:@"image/jpeg"];
        
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        sucess(responseObject);
        
        NSLog(@"Success: %@", responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        fail(error);
    }];
}
*/

+(void)postDataWithUrl:(NSString *)url
                 image:(NSData *)image1
          uolpadFstImg:(BOOL)img
            image2Data:(NSData *)image2
         uolpadscndImg:(BOOL)img2
            image3Data:(NSData *)image3
         uolpadThrdImg:(BOOL)img3
              withDict:(NSDictionary *)dict
           sucessBlock:(void (^)(id responseObject))sucess
          failureBlock:(void (^)(NSError *error))fail{
    
//    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
//    NSLog(@"%@",dict);
//    [manager POST:url parameters:dict constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
//        
//        if (img == YES) {
//            [formData appendPartWithFileData:image1
//                                        name:@"image1"
//                                    fileName:@"image1.jpg"
//                                    mimeType:@"image/jpeg"];
//        }
//        
//        if (img2 == YES) {
//            [formData appendPartWithFileData:image2
//                                        name:@"image2"
//                                    fileName:@"image2.jpg"
//                                    mimeType:@"image/jpeg"];
//        }
//        
//        if (img3 == YES) {
//            [formData appendPartWithFileData:image3
//                                        name:@"image3"
//                                    fileName:@"image3.jpg"
//                                    mimeType:@"image/jpeg"];
//        }
//        
//    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        NSLog(@"Success: %@", responseObject);
//        sucess(responseObject);
//        
//        
//    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        NSLog(@"Error: %@", error);
//        fail(error);
//    }];
    
}

+ (void)textField:(UITextField *)textField1
{
    
    NSString *cc_num_string = textField1.text;
    UITextField *textField = textField1;
    textField.rightViewMode = UITextFieldViewModeAlways;
    if ([cc_num_string length] > 1 && [cc_num_string characterAtIndex:0] == '4')
    {textField.rightView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"visa.png"]];}
    
    else if ([cc_num_string length] > 1 && [cc_num_string characterAtIndex:0] == '5' && ([cc_num_string characterAtIndex:1] == '1' || [cc_num_string characterAtIndex:1] == '5'))
    {textField.rightView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"mastercard.png"]];}
    
    else if ([cc_num_string length] > 1 && [cc_num_string characterAtIndex:0] == '6' && ([cc_num_string characterAtIndex:1] == '5' || ([cc_num_string length] > 2 &&[cc_num_string length] > 1 && [cc_num_string characterAtIndex:1] == '4' && [cc_num_string characterAtIndex:2] == '4') || ([cc_num_string length] > 3 && [cc_num_string characterAtIndex:1] == '0' && [cc_num_string characterAtIndex:2] == '1' && [cc_num_string characterAtIndex:3] == '1' )))
    { textField.rightView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"discover.png"]];}
    
    else if ([cc_num_string length] > 1 && [cc_num_string characterAtIndex:0] == '3' && ([cc_num_string characterAtIndex:1] == '4' || [cc_num_string characterAtIndex:1] == '7'))
    { textField.rightView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"americanexpress.png"]];}
    
    else{textField.rightView = nil;}
}

//+(UIImage *)setGifImage{
//    NSURL *url = [[NSBundle mainBundle] URLForResource:@"loading" withExtension:@"gif"];
//    return [UIImage animatedImageWithAnimatedGIFURL:url];
//}


+(void)removeTextfield:(UIView *)view {
    for (UITextField *txt in view.superview.subviews) {
        if ([txt isKindOfClass:[UITextField class]]) {
            [txt resignFirstResponder];
        }
    }
}
+(void)removelbl:(UIScrollView *)view {
    for (UIImageView *txt in view.superview.subviews) {
        if ([txt isKindOfClass:[UIImageView class]]) {
            
            if(txt.tag == 1001){
                [txt removeFromSuperview];
            }
            
        }
    }
}

+(void)showHud:(UIView *)view {
//    [MBProgressHUD showHUDAddedTo:view animated:YES];
    
}

+(void)hideHud:(UIView *)view {
//    [MBProgressHUD hideHUDForView:view animated:YES];
//    [MBProgressHUD hideAllHUDsForView:view animated:YES];
}

+(NSString *)encodeToBase64String:(UIImage *)image {
    return [UIImagePNGRepresentation(image) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
}


+(void)setNsUserDefaultValue:(id)value key:(NSString *)str{
    [[NSUserDefaults standardUserDefaults] setObject:value forKey:str];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
+(NSString *)getNsUserDefault:(NSString *)str{
    return [[NSUserDefaults standardUserDefaults] objectForKey:str];
}


+(void)registerPushNotification:(UIApplication *)application {
    
    if ([application respondsToSelector:@selector(isRegisteredForRemoteNotifications)])
    {
        // iOS 8 Notifications
        [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        
        [application registerForRemoteNotifications];
    }
    else
    {
        // iOS < 8 Notifications
//        [application registerForRemoteNotificationTypes:
//         (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound)];
    }
}
+(CGRect)getFrectX:(CGFloat)x y:(CGFloat)y h:(CGFloat)h w:(CGFloat)w i:(int)i
{
    return CGRectMake(x * i, y, w, y);
}

+(void)setTint {
    
}
+ (int)lineCountForLabel:(UILabel *)label {
    CGSize constrain = CGSizeMake(label.bounds.size.width, FLT_MAX);
    CGSize size = [label.text sizeWithFont:label.font constrainedToSize:constrain lineBreakMode:NSLineBreakByWordWrapping];
    
    return ceil(size.height / label.font.lineHeight);
}


+(NSString *)getLowerValue:(float)slider{
    
    NSUInteger numberOfSlots = 24*4 - 1; //total number of 15mins slots
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"h:mm a"];
    
    NSDate *zeroDate = [dateFormatter dateFromString:@"12:00 AM"];
    
    NSUInteger actualSlot = roundf(numberOfSlots*slider);
    NSTimeInterval slotInterval = actualSlot * 15 * 60;
    
    NSDate *slotDate = [NSDate dateWithTimeInterval:slotInterval sinceDate:zeroDate];
    
    [dateFormatter setDateFormat:@"h:mm a"];
    [dateFormatter setAMSymbol:@"AM"];
    [dateFormatter setPMSymbol:@"PM"];
    return [dateFormatter stringFromDate:slotDate];
}




+(NSString *)getUpperValue:(float)slider{
    NSUInteger numberOfSlots = 24*4; //total number of 15mins slots
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"h:mm a"];
    
    NSDate *zeroDate = [dateFormatter dateFromString:@"12:00 AM"];
    
    NSUInteger actualSlot = roundf(numberOfSlots*slider);
    NSTimeInterval slotInterval = actualSlot * 15 * 60;
    
    NSDate *slotDate = [NSDate dateWithTimeInterval:slotInterval sinceDate:zeroDate];
    [dateFormatter setDateFormat:@"h:mm a"];
    [dateFormatter setAMSymbol:@"AM"];
    [dateFormatter setPMSymbol:@"PM"];
    return [dateFormatter stringFromDate:slotDate];
    
}

+(NSString *)getCurrentDate {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    
    return [dateFormatter stringFromDate:[NSDate date]];
}

+(NSString *)getCurrentTime{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"HH:mm:ss";
    return [dateFormatter stringFromDate:[NSDate date]];
}

+(NSString *)getCurrentSelectStartTime {
    NSDate *date = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"hh:mm a";
    
    return [dateFormatter stringFromDate:date];
}

+(NSDate *)getDate:(NSString *)str{
    
    //2015-11-03
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"MM/dd/yyyy HH:mm:ss";
    NSDate *date = [dateFormatter dateFromString:str];
    
    return date;
    
}

+(NSString *)get12TimeFrom24Time:(NSString *)str{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"HH:mm:ss";
    
    NSDate *date = [dateFormatter dateFromString:str];
    
    dateFormatter.dateFormat = @"hh:mm a";
    //NSLog(@"%@",[dateFormatter stringFromDate:date]);
    [dateFormatter setAMSymbol:@"AM"];
    [dateFormatter setPMSymbol:@"PM"];
    return [dateFormatter stringFromDate:date];
}

+(NSString *)get12TimeFrom24TimeActivityFeed:(NSString *)str{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    
    NSDate *date = [dateFormatter dateFromString:str];
    
    dateFormatter.dateFormat = @"hh:mm a";
    [dateFormatter setAMSymbol:@"AM"];
    [dateFormatter setPMSymbol:@"PM"];
    return [dateFormatter stringFromDate:date];
}

+(NSString *)get12TimeFrom24TimeHostActivity:(NSString *)str{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"hh:mm a";
    
    NSDate *date = [dateFormatter dateFromString:str];
    
    dateFormatter.dateFormat = @"HH:mm:ss";
    return [dateFormatter stringFromDate:date];
}


+(NSString *)getBirthDate:(NSDate *)date{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"MM/dd/yyyy";
    
    return [dateFormatter stringFromDate:date];
}

//ashit
+(NSString *)get12TimeFrom24TimeActivityFeedDateTime:(NSString *)str{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    
    NSDate *date = [dateFormatter dateFromString:str];
    
    dateFormatter.dateFormat = @"EE, MMM dd  h:mm a";
    
    [dateFormatter setAMSymbol:@"AM"];
    [dateFormatter setPMSymbol:@"PM"];
    return [dateFormatter stringFromDate:date];
}
//

+(NSString *)getTimeTo24Hour:(float)slider{
    
    NSUInteger numberOfSlots = 24*4 - 1; //total number of 15mins slots
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm:ss"];
    
    NSDate *zeroDate = [dateFormatter dateFromString:@"00:00:00"];
    
    NSUInteger actualSlot = roundf(numberOfSlots*slider);
    NSTimeInterval slotInterval = actualSlot * 15 * 60;
    
    NSDate *slotDate = [NSDate dateWithTimeInterval:slotInterval sinceDate:zeroDate];
    
    [dateFormatter setDateFormat:@"HH:mm:ss"];
    [dateFormatter setAMSymbol:@"AM"];
    [dateFormatter setPMSymbol:@"PM"];
    return [dateFormatter stringFromDate:slotDate];
}

+(float)getSliderValue:(NSString *)timeStr{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm:ss"];
    
    NSDate *zeroDate = [dateFormatter dateFromString:@"00:00:00"];
    NSDate *currentTime = [dateFormatter dateFromString:timeStr];
    
    
    NSUInteger numberOfSlots = 24*4 - 1; //total number of 15mins slots
    
    NSTimeInterval nn = [currentTime timeIntervalSinceDate:zeroDate];
    
    
    return  nn / numberOfSlots / 15 / 60;

}

+(NSMutableArray *)getDay{
    
    NSMutableArray *arrDate = [[NSMutableArray alloc]init];
    
    for (int i = 0; i < 7; i++) {
        NSCalendar *calendar=[NSCalendar currentCalendar];
        NSDateComponents *components = [[NSDateComponents alloc]init];
        components.day = i;
        NSDate *fireDate =[calendar dateByAddingComponents:components toDate:[NSDate date] options:0];
        
        NSDateFormatter *df=[[NSDateFormatter alloc] init];
        [df setDateFormat:@"EEE\ndd\nMMM"];
        
        NSString *Str = [df stringFromDate:fireDate];
        [arrDate addObject:Str];
//        NSLog(@"Date: %@", Str);

    }
    
        return arrDate;
}

+(NSMutableArray *)getDayDate{
    NSMutableArray *arrDate = [[NSMutableArray alloc]init];
    
    for (int i = 0; i < 7; i++) {
        NSCalendar *calendar=[NSCalendar currentCalendar];
        NSDateComponents *components = [[NSDateComponents alloc]init];
        components.day = i;
        NSDate *fireDate =[calendar dateByAddingComponents:components toDate:[NSDate date] options:0];
        
        NSDateFormatter *df=[[NSDateFormatter alloc] init];
        [df setDateFormat:@"yyyy-MM-dd"];
        
        NSString *Str = [df stringFromDate:fireDate];
        [arrDate addObject:Str];
//        NSLog(@"Date: %@", Str);
        
    }
    return arrDate;
}


+(NSString *)getUTcTime{
    
    NSDate* datetime = [NSDate date];
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]]; // Prevent adjustment to user's local time zone.
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    return [dateFormatter stringFromDate:datetime];
}

+(NSString *)getSystemTime :(NSString *)date
{
    NSDate* datetime = [self getDate:date];
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]]; // Prevent adjustment to user's local time zone.
    [dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
    return [dateFormatter stringFromDate:datetime];
    
}
+(NSString *)getTimeFromDate:(NSDate *)date{
    
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setDateFormat:@"HH:mm:00"];
    
//   [dateFormatter stringFromDate:date];
//    [dateFormatter setAMSymbol:@"AM"];
//    [dateFormatter setAMSymbol:@"PM"];
    
    return [dateFormatter stringFromDate:date];
    
}

+(NSString *)getUTCtoSystem:(NSString *)string
{
    
    //yyyy-MM-dd HH:mm:ss ZZZ
//    2016-02-15 10:28:56 +0000
    
//    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
//    dateFormatter.timeZone = [NSTimeZone localTimeZone];
//    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZ"];
//
//    NSDate *S = [dateFormatter dateFromString:string];
//    
//    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
//    dateFormatter1.dateFormat = @"yyyy-MM-dd HH:mm:ss";
//    
//    NSLog(@"%@",[dateFormatter1 stringFromDate:S]);
//    return [dateFormatter1 stringFromDate:S];

//    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//    [formatter setDateFormat:@"HH:mm:ss"];
//    formatter.timeZone = [NSTimeZone localTimeZone];
//    NSDate *date = [formatter dateFromString:string];
//    [formatter setDateFormat:@"HH:mm:ss"];
//    return [formatter stringFromDate:date];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm:ss"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    NSDate *date = [dateFormatter dateFromString:string]; // create date from string
    
    // change to a readable time format and change to local time zone
    [dateFormatter setDateFormat:@"HH:mm:ss"];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    NSString *timestamp = [dateFormatter stringFromDate:date];
    
    return timestamp;
}

+(void)setMyContentOffset:(UIScrollView *)scrollObj point:(CGPoint)point {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.2];
    scrollObj.contentOffset = point;
    [UIView commitAnimations];
    
}

+ (UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

+(float)getFloatValue:(float)value{
    return value / 9;
}


//+(void)isIQKeyboardShow:(BOOL)value{
//    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:value];
//    [[IQKeyboardManager sharedManager] setEnable:value];
//}

//+(void)setHeightForTextview:(UIView *)containerView andtextview:(HPGrowingTextView *)growingTextView andheight:(float)height{
//    float diff = (growingTextView.frame.size.height - height);
//    CGRect r = containerView.frame;
//    r.size.height -= diff;
//    r.origin.y += diff;
//    containerView.frame = r;
//    
//}

+(void)keyboardShow:(NSNotification *)note and:(UIViewController *)viewController and:(UIView *)uiview{
    CGRect keyboardBounds = [note.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    [[note.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue: &keyboardBounds];
    NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    // Need to translate the bounds to account for rotation.
    keyboardBounds = [viewController.view convertRect:keyboardBounds toView:nil];
    // get a rect for the textView frame
    CGRect containerFrame = uiview.frame;
    containerFrame.origin.y = viewController.view.bounds.size.height - (keyboardBounds.size.height + containerFrame.size.height);
    
    //    CGRect tableviewframe=self.sphChatTable.frame;
    //    tableviewframe.size.height-=160;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:[duration doubleValue]];
    [UIView setAnimationCurve:[curve intValue]];
    uiview.frame = containerFrame;
    //    self.sphChatTable.frame=tableviewframe;
    
    [UIView commitAnimations];
}

+(NSString *)saveSenderPic:(UIImage *)image{
    //SenderPicture
    NSData *data = UIImagePNGRepresentation(image);
    NSString *strDocPAth = [UtilityClass createDocumentDirectory:@"SenderPicture"];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"ddMMyyyyHHmmss"];
    NSString *orignalImgName = [NSString stringWithFormat:@"%@versafit",[formatter stringFromDate:[NSDate date]]];
    NSString *savedImagePath = [strDocPAth stringByAppendingPathComponent:[NSString stringWithFormat:@"/SenderPicture/%@.png",orignalImgName]];
    
    [data writeToFile:savedImagePath atomically:NO];
    return savedImagePath;
}

+(NSString *)saveReceivePic:(UIImage *)image name:(NSString *)string{
    
    NSData *data = UIImagePNGRepresentation(image);
    NSString *strDocPAth = [UtilityClass createDocumentDirectory:@"ReceivePicture"];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"ddMMyyyyHHmmss"];
    NSString *orignalImgName = [NSString stringWithFormat:@"%@versafit",[formatter stringFromDate:[NSDate date]]];
    
    //NSString *digits = [string stringByTrimmingCharactersInSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]];
    NSString *savedImagePath = [strDocPAth stringByAppendingPathComponent:[NSString stringWithFormat:@"/ReceivePicture/%@.png",orignalImgName]];
    UIImage *img =[UIImage imageWithData:data];
    data = UIImagePNGRepresentation(img);
    [data writeToFile:savedImagePath atomically:NO];
    
    
    return savedImagePath;
}

+(NSString *)createDocumentDirectory:(NSString *)strFolderName {
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@",strFolderName]];
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error];
    return documentsDirectory;
}


+ (UIImage *)imageByCroppingImage:(UIImage *)image toSize:(CGSize)size
{
    double x = (image.size.width - size.width) / 2.0;
    double y = (image.size.height - size.height) / 2.0;
    
    CGRect cropRect = CGRectMake(x, y, size.height, size.width);
    CGImageRef imageRef = CGImageCreateWithImageInRect([image CGImage], cropRect);
    
    UIImage *cropped = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    
    return cropped;
}

+(CGRect)getrectFromText:(NSString *)string andVc:(UIViewController *)controller{
    
    
    NSString *srr = [string stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
    
    CGSize textSize = { controller.view.frame.size.width - 30, 10000.0 };
    
    
    return [srr  boundingRectWithSize:textSize
                              options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading)
                           attributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:15]} context:nil];
}

+(BOOL) CompareDateTime : (NSString *)date : (NSString *)CurrentDate
{
    NSString *time1 = date;
    NSString *time2 = CurrentDate;
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSDate *date1= [formatter dateFromString:time1];
    NSDate *date2 = [formatter dateFromString:time2];
    
    NSComparisonResult result = [date1 compare:date2];
    if(result == NSOrderedDescending)
    {
      //  NSLog(@"date1 is later than date2");
        
        return YES;
    }
    else if(result == NSOrderedAscending)
    {
      //  NSLog(@"date2 is later than date1");
        
        return NO;
    }
    else
    {
      //  NSLog(@"date1 is equal to date2");
        
        return NO;
    }
    
}
+(void)removeBadgeIcon:(UITabBar *)tabbar {
    [[tabbar.items objectAtIndex:1] setBadgeValue:nil];
}

+(int)getAgeFromBdate:(NSDate *)date{
    NSDate* now = [NSDate date];
    NSDateComponents* ageComponents = [[NSCalendar currentCalendar]
                                       components:NSCalendarUnitYear
                                       fromDate:date
                                       toDate:now
                                       options:0];
    return (int)[ageComponents year];
}

+(void)registerPushNotification {
    // Subscribe to push notifications
    
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 80000
    if ([[UIApplication sharedApplication] respondsToSelector:@selector(registerUserNotificationSettings:)]) {
        
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    else{
//        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound];
    }
#else
    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound];
#endif
}


+(void)setStatusbar:(int)statusbar{
    
    if (statusbar == 0) {
        [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    } else {
        [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    }
    
}


@end
