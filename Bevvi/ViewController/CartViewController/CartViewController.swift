//
//  CartViewController.swift
//  Bevvi
//
//  Created by Hetal Govani on 31/10/17.
//  Copyright © 2017 solulab. All rights reserved.
//

import UIKit

class CartViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UpdateCartItemDelegate,UITextFieldDelegate
{
   @IBOutlet var tblObj : UITableView!
   @IBOutlet var tblHeight : NSLayoutConstraint!
   @IBOutlet var viewHeight : NSLayoutConstraint!
   @IBOutlet var promoView : UIView!
   @IBOutlet var btnCheckOut : UIButton!
   @IBOutlet var btnApply : UIButton!
   @IBOutlet var btnSignUp : UIButton!
   @IBOutlet var btnLogin : UIButton!
   @IBOutlet var txtPromoCode : customTextField!
   @IBOutlet var cartView : UIView!
//   @IBOutlet var loginView : UIView!
   var arrOriginalPrice = [CGFloat]()
   var arrsalePrice = [CGFloat]()
   var arrQuantity = [Int]()
   @IBOutlet var lblStoreName : UILabel!
   @IBOutlet var lblAddress : UILabel!
   @IBOutlet var lblDistance : UILabel!
   @IBOutlet var scrView : UIScrollView!
   
   var arrList : Array<Dictionary<String,AnyObject>>! = Array()
   var arrcartDetail : [cartDetail] = Array()

   var cartObj: cartDetail?
   lazy var errorView: ErrorView = {
      let Errview = UINib(nibName: "ErrorView", bundle: nil).instantiate(withOwner: self, options: nil)[0] as! ErrorView
      Errview.translatesAutoresizingMaskIntoConstraints = false
      Errview.retryButton.addTarget(self, action: #selector(btnRetryPress(sender:)), for: .touchUpInside)
      return Errview
   }()
   func presentError(_ error: customError) {
      self.errorView.error = error
      
      DispatchQueue.main.async {
         self.view.addSubview(self.errorView)
         self.installErrorViewConstraints()
      }
   }
   
   func installErrorViewConstraints() {
      self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[view]|", options: [], metrics: nil, views: [ "view": self.errorView ]))
      self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[view]|", options: [], metrics: nil, views: [ "view": self.errorView ]))
   }
   @IBAction func btnRetryPress(sender:UIButton)
   {
      errorView.removeFromSuperview()
      if appDel.reachability.connection != .none
      {
         retriveCart()
      }
      else
      {
         self.presentError(.NetworkError)
      }
   }
    override func viewDidLoad()
    {
      super.viewDidLoad()
      self.title = "Cart"
      self.tabBarItem.title = ""
      
      viewHeight.constant = self.view.frame.height - 114
      
      promoView.layer.borderWidth = 1
      promoView.layer.borderColor = UIColor.lightGray.cgColor
      
      let left = UISwipeGestureRecognizer(target: self, action: #selector(swipeLeft))
      left.direction = .left
      self.view.addGestureRecognizer(left)
      
      let right = UISwipeGestureRecognizer(target: self, action: #selector(swipeRight))
      right.direction = .right
      self.view.addGestureRecognizer(right)
//      tblHeight.constant = 0
    }
   
    override func viewWillAppear(_ animated: Bool)
    {
      super.viewWillAppear(true)
      self.navigationController?.navigationBar.isHidden = false
      
      let isLogin = UserDefaults.standard.object(forKey: "isLogin") as! Bool
      
      if isLogin == true
      {
         btnSignUp.isHidden = true
         btnLogin.isHidden = true
         btnCheckOut.isHidden = false
         self.navigationItem.rightBarButtonItem?.isEnabled = true
      }
      else
      {
         btnSignUp.isHidden = false
         btnLogin.isHidden = false
         btnCheckOut.isHidden = true
         self.navigationItem.rightBarButtonItem?.isEnabled = false
      }
      
      self.tabBarController?.removeBadge()
      if appDel.reachability.connection != .none
      {
         retriveCart()
      }
      else
      {
         self.presentError(.NetworkError)
      }
    }
   
   func changeTableSize() {
      tblObj.layer.removeAllAnimations()
      tblHeight.constant = tblObj.contentSize.height
      UIView.animate(withDuration: 0.5)
      {
         self.tblObj.layoutIfNeeded()
         self.updateViewConstraints()
      }
   }
    
    override func updateViewConstraints()
    {
        super.updateViewConstraints()
        viewHeight.constant = tblObj.frame.origin.y + tblHeight.constant + 250
    }
   func retriveCart()
   {
      arrOriginalPrice = Array()
      arrQuantity = Array()
      arrsalePrice = Array()

      arrcartDetail = Array()

      //http://18.221.89.220:3000/api/offers?filter={"where":{"establishmentId":"5a24a29410785a5c1aa56cde"}, "include":["product"]}
      if (UserDefaults.standard.object(forKey: "accountId") == nil)
      {
         CartNotLogIn.sharedInstance.getAllCartProductsNotLoggedIn(completion: { (cart:[cartDetail]) in
            
            for product in cart
            {
               self.arrcartDetail.append(product as cartDetail)
               self.arrsalePrice.append(CGFloat(NumberFormatter().number(from: product.salePrice!)!))
               self.arrOriginalPrice.append(CGFloat(NumberFormatter().number(from: product.originalPrice!)!))
               self.arrQuantity.append(product.quantity!)
               print("\(product.productId!)")
            }
            //         self.arrcartDetail = cartProducts
            if self.arrcartDetail.count == 0
            {
               self.cartView.isHidden = false
            }
            else
            {
               self.lblAddress.text = cart[0].address
               self.lblStoreName.text = cart[0].storeName
               self.cartView.isHidden = true
               self.tblObj.isHidden = false
               self.tblObj.reloadData()
               DispatchQueue.main.async
                  {
                     self.changeTableSize()
                     //                  self.tblObj.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
               }
            }
         })
      }
      else
      {
         let para:NSMutableDictionary = NSMutableDictionary()

         let geoLocation:Dictionary = ["accountId":"\(UserDefaults.standard.value(forKey: "accountId")!)","state":0] as [String : Any]
         
         //      ["product",{"offer":["establishment"]}]
         let muray_latlong : Array<String> = ["establishment"]
         let dictOffer:NSMutableDictionary = NSMutableDictionary()
         dictOffer.setObject(muray_latlong, forKey: "offer" as NSCopying)
         
         let includeDict : Array<AnyObject> = ["product" as AnyObject,dictOffer as AnyObject]
         //      let includeDict:NSMutableArray = ["product","offer"]
         para.setObject(geoLocation, forKey: "where" as NSCopying)
         para.setObject(includeDict, forKey: "include" as NSCopying)
         
         if let theJSONData = try?  JSONSerialization.data(
            withJSONObject: para,
            options: .prettyPrinted
            ),
            let theJSONText = String(data: theJSONData,
                                     encoding: String.Encoding.ascii)
         {
            print("JSON string = \n\(theJSONText)")
            
            let storeDetail = StoreDetails()
            storeDetail.jsonArray = theJSONText
            appDel.showHUD()
            ApiManager.shared.retriveCart(listing: storeDetail, view: self.view, completion: { (arr:[JSONDictionary]) in
               
               print(arr)
               self.arrList = arr
               for dict in self.arrList
               {
                  let dictOffer = dict["offer"] as! Dictionary<String,AnyObject>
                  let salePrice = "\(dictOffer["salePrice"]!)"
                  let originalPrice = "\(dictOffer["originalPrice"]!)"
                  let quantity = "\(dict["quantity"]!)"
                  
                  self.arrsalePrice.append(CGFloat(NumberFormatter().number(from: salePrice)!))
                  self.arrOriginalPrice.append(CGFloat(NumberFormatter().number(from: originalPrice)!))
                  self.arrQuantity.append(Int(quantity)!)
               }
               if self.arrList.count == 0
               {
                  self.cartView.isHidden = false
               }
               else
               {
                  self.cartView.isHidden = true
                  let dict = self.arrList[0]["offer"] as! Dictionary<String,AnyObject>
                  let dictEstablishment = dict["establishment"] as! Dictionary<String,AnyObject>
                  self.lblStoreName.text = "\(dictEstablishment["name"]!)"
                  self.lblAddress.text = "\(dictEstablishment["address"]!)"
                  self.getDistance(id: "\(dictEstablishment["id"]!)")
               }
               self.tblObj.reloadData()
               appDel.hideHUD()
               
               DispatchQueue.main.async
                  {
                     self.changeTableSize()
                     //                  self.tblObj.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
               }
            })
         }
      }
   }
   func getDistance(id : String)
   {
      let dict = self.arrList[0]["offer"] as! Dictionary<String,AnyObject>
      let dictEstablishment = dict["establishment"] as! Dictionary<String,AnyObject>
      let geoDict = dictEstablishment["geoLocation"] as! [String:AnyObject]
      let arrCoord = geoDict["coordinates"] as! [NSNumber]
      ApiManager.shared.getEstDistance(latitude: "\(arrCoord[1])", longitude: "\(arrCoord[0])", establishmentId: id) { (arr:[JSONDictionary]) in
         print(arr)
         let dictDuration = arr[0]["duration"] as! Dictionary<String,AnyObject>
         if Int("\(UserDefaults.standard.value(forKey: "walk")!)") == 0
         {
            self.lblDistance.text = "\(dictDuration["text"]!) walk"
         }
         else
         {
            self.lblDistance.text = "\(dictDuration["text"]!) drive"
         }
      }
   }
   func textFieldDidEndEditing(_ textField: UITextField) {
      self.view.endEditing(true)
      self.scrView.setContentOffset(
         CGPoint(x: 0,y: -self.scrView.contentInset.top),
         animated: true)
   }
    @objc func swipeLeft() {
        let total = self.tabBarController!.viewControllers!.count - 1
        tabBarController!.selectedIndex = min(total, tabBarController!.selectedIndex + 1)
    }
    
    @objc func swipeRight() {
        tabBarController!.selectedIndex = max(0, tabBarController!.selectedIndex - 1)
    }
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 2
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?
    {
        return nil
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0
        {
         if (UserDefaults.standard.object(forKey: "accountId") == nil)
         {
            return arrcartDetail.count
         }
         else{
            return arrList.count
         }
        }
        else
        {
            return 4
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! itemCell
            cell.selectionStyle = .none
         if (UserDefaults.standard.object(forKey: "accountId") == nil)
         {
            let cartDetail = arrcartDetail[indexPath.row]
            cell.lblTitle.text = "\(cartDetail.name!)"
            cell.lblDesc.text = "\(cartDetail.varietal!)"
            cell.lblQuantity.text = "\(cartDetail.quantity!)"
            let price = CGFloat(NumberFormatter().number(from: "\(cartDetail.salePrice!)")!) * CGFloat(NumberFormatter().number(from: "\(cartDetail.quantity!)")!)
            cell.lblPrice.text = "$\(price)"
         }
         else
         {
            let dictProduct = arrList[indexPath.row]["product"] as! Dictionary<String,AnyObject>
            cell.lblTitle.text = "\(dictProduct["name"]!)"
            cell.lblDesc.text = "\(dictProduct["varietal"]!)"
            cell.lblQuantity.text = "\(arrList[indexPath.row]["quantity"]!)"
            
            let dictOffer = arrList[indexPath.row]["offer"] as! Dictionary<String,AnyObject>
            let price = CGFloat(NumberFormatter().number(from: "\(dictOffer["salePrice"]!)")!) * CGFloat(NumberFormatter().number(from: "\(arrList[indexPath.row]["quantity"]!)")!)

            cell.lblPrice.text = "$\(price)"
         }
         
         return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellFinal", for: indexPath) as! finalInfoCell
            if indexPath.row == 0
            {
               cell.lblTitle.text = "Tax"
               
               cell.lblPrice.textColor = UIColor.init(red: 141/255, green: 141/255, blue: 141/255, alpha: 1)
               cell.lblPrice.text = "5%"
            }
            else if indexPath.row == 1
            {
                cell.lblTitle.text = "Rewards"
               var savedPrice : CGFloat = 0
               for i in 0 ..< arrOriginalPrice.count
               {
                  let diff = (arrOriginalPrice[i] - arrsalePrice[i]) * CGFloat(arrQuantity[i])
                  savedPrice = savedPrice + diff
               }
               cell.lblPrice.textColor = UIColor.init(red: 141/255, green: 141/255, blue: 141/255, alpha: 1)
               cell.lblPrice.text = "-$\(savedPrice)"
            }
            else if indexPath.row == 2
            {
               cell.lblTitle.text = "TOTAL"
               cell.lblTitle.textColor = UIColor.black
               var totalPrice : CGFloat = 0
               for i in 0 ..< arrOriginalPrice.count
               {
                  totalPrice = totalPrice + (arrsalePrice[i] * CGFloat(arrQuantity[i]))
               }
               totalPrice = totalPrice + (totalPrice * 5) / 100
               cell.lblPrice.textColor = UIColor.black
               cell.lblPrice.text = "$\(totalPrice)"
            }
            else if indexPath.row == 3
            {
               cell.lblTitle.text = "Points earned"
               cell.lblTitle.font = UIFont.init(name: "Avenir-Roman", size: 13)
               cell.lblTitle.textColor = redColor
              
               cell.lblPrice.font = UIFont.init(name: "Avenir-Roman", size: 13)
               cell.lblPrice.textColor = redColor
               cell.lblPrice.text = "0 pts"
         }
            cell.selectionStyle = .none
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
    {
        if indexPath.section == 0
        {
            return true
        }
        return false
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt: IndexPath) -> [UITableViewRowAction]?
    {
         let Edit = UITableViewRowAction(style: .normal, title: "Edit")
         { action, index in
            
            tableView.setEditing(false, animated: true)
            let ratingVC = UpdateCartItemViewController(nibName: "UpdateCartItemViewController", bundle: nil)
            if (UserDefaults.standard.object(forKey: "accountId") == nil)
            {
               ratingVC.cartDetail = self.arrcartDetail[index.row]
            }
            else
            {
               ratingVC.dictDetail = self.arrList[index.row]
            }
            ratingVC.delegate=self
            self.presentPopupViewController(ratingVC, animationType: MJPopupViewAnimationFade)
            print("edit button tapped")
        }
        Edit.backgroundColor = YellowColor

        let Delete = UITableViewRowAction(style: .normal, title: "Delete")
        { action, index in
         
         tableView.setEditing(false, animated: true)
         self.deletePress(indexPath: index)
      }
        Delete.backgroundColor = redColor
        
        return [Delete,Edit]
    }
   
   func deletePress(indexPath:IndexPath)
   {
      let alert = UIAlertController.init(title: APP_NAME, message: "Are you sure you want to delete this item from cart?", preferredStyle: .alert)
      alert.addAction(UIAlertAction.init(title: "Yes", style: .default, handler: { (action) in
         if (UserDefaults.standard.object(forKey: "accountId") == nil)
         {
            CartNotLogIn.sharedInstance.deleteProduct(product: self.arrcartDetail[indexPath.row], completion: {
               if self.arrcartDetail.count == 1
               {
                  UserDefaults.standard.removeObject(forKey:"establishmentId")
                  UserDefaults.standard.synchronize()
               }
               if appDel.reachability.connection != .none
               {
                  self.retriveCart()
               }
               else
               {
                  self.presentError(.NetworkError)
               }
            })
         }
         else
         {
            appDel.showHUD()
            ApiManager.shared.deleteCart(id: "\(self.arrList[indexPath.row]["id"]!)", view: self.view, completion: { (dict:JSONDictionary) in
               
               appDel.hideHUD()
               print(dict)
               if Int("\(dict["count"]!)") == 1
               {
                  if self.arrList.count == 1
                  {
                     UserDefaults.standard.removeObject(forKey:"establishmentId")
                     UserDefaults.standard.synchronize()
                  }
                  if appDel.reachability.connection != .none
                  {
                     self.retriveCart()
                  }
                  else
                  {
                     self.presentError(.NetworkError)
                  }
               }
               else
               {
                  UtilityClass.showAlert("Something get wrong. Please try again later.")
               }
            })
            print("delete button tapped")
         }
         
      }))
      alert.addAction(UIAlertAction.init(title: "No", style: .cancel, handler: nil))
      self.present(alert, animated: true, completion: nil)
   }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath)
    {
        if indexPath.section == 0
        {
            if (editingStyle == UITableViewCellEditingStyle.delete) {
                // handle delete (by removing the data from your array and updating the tableview)
            }
            if (editingStyle == UITableViewCellEditingStyle.delete) {
                // handle delete (by removing the data from your array and updating the tableview)
            }
        }
    }
   func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      self.view.endEditing(true)
      self.scrView.setContentOffset(
         CGPoint(x: 0,y: -self.scrView.contentInset.top),
         animated: true)
   }
   
    func UpdateButtonClicked(_ secondDetailViewController: UpdateCartItemViewController)
    {
      if (UserDefaults.standard.object(forKey: "accountId") == nil)
      {
//         CartNotLogIn.sharedInstance.deleteProduct(product: self.arrcartDetail[indexPath.row])
         self.dismissPopupViewControllerWithanimationType(MJPopupViewAnimationFade)

         let cartItem = secondDetailViewController.cartDetail as! cartDetail
         CartNotLogIn.sharedInstance.updateGameDataObjects(quantity: secondDetailViewController.qty!, object: cartItem.toJsonDict())
         if appDel.reachability.connection != .none
         {
            self.retriveCart()
         }
         else
         {
            self.presentError(.NetworkError)

         }
      }
      else
      {
         let dict:Dictionary = ["accountId":"\(UserDefaults.standard.value(forKey: "accountId")!)","quantity":secondDetailViewController.lblQuantity.text,"productId":"\(secondDetailViewController.dictDetail["productId"]!)","offerId":"\(secondDetailViewController.dictDetail["offerId"]!)","id":"\(secondDetailViewController.dictDetail["id"]!)"]
         
         if let theJSONData = try?  JSONSerialization.data(
            withJSONObject: dict,
            options: .prettyPrinted
            ),
            let theJSONText = String(data: theJSONData,
                                     encoding: String.Encoding.ascii) {
            print("JSON string = \n\(theJSONText)")
            appDel.showHUD()
            let storeDetail = StoreDetails()
            storeDetail.jsonArray = theJSONText
            ApiManager.shared.updateCart(id: "\(secondDetailViewController.dictDetail["id"]!)", listing: storeDetail, view: self.view, completion: { (dict:JSONDictionary) in
               print(dict)
               appDel.hideHUD()
               self.dismissPopupViewControllerWithanimationType(MJPopupViewAnimationFade)
               if appDel.reachability.connection != .none
               {
                  self.retriveCart()
               }
               else
               {
                  self.presentError(.NetworkError)
               }
            })
         }
      }
    }
    
    func DismissButtonClicked(_ secondDetailViewController: UpdateCartItemViewController)
    {
        self.dismissPopupViewControllerWithanimationType(MJPopupViewAnimationFade)
    }
    
    @IBAction func btnLoginPress(sender:UIButton)
    {
        let viewObj = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.navigationController?.pushViewController(viewObj, animated: true)
    }
    
    @IBAction func btnSignupPress(sender:UIButton)
    {
        let viewObj = self.storyboard?.instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
        self.navigationController?.pushViewController(viewObj, animated: true)
    }
    
    @IBAction func btnApplyPress(sender:UIButton)
    {
        if txtPromoCode.text?.count != 0
        {
            btnApply.setImage(#imageLiteral(resourceName: "applied"), for: .normal)
            btnApply.setTitle("", for: .normal)
        }
        else
        {
            UtilityClass.showAlert("Please enter promo code.")
        }
    }
    
    @IBAction func btnCheckoutPress(sender:UIButton)
    {
        let viewObj = self.storyboard?.instantiateViewController(withIdentifier: "CheckOutViewController") as! CheckOutViewController
        self.navigationController?.pushViewController(viewObj, animated: true)
    }
    
    @IBAction func btnBackPress(sender:UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
//        self.tblObj.removeObserver(self, forKeyPath: "contentSize", context: nil)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}
