//
//  STPaymentResponse.swift
//  Bevvi
//
//  Created by Ankit on 1/11/18.
//  Copyright © 2018 solulab. All rights reserved.
//

import Foundation

class STPaymentResponse {
   
   var status: String?
   
   func toParseDict(dict: JSONDictionary) {
      
      if let status = dict["status"] as? String {
         self.status = status
      }
      
   }
}
